// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:
if (typeof vjs_edge == "function") {
    vjs = vjs_edge;
}

//vjs.createClass({
//    "extends"    : Base,
//    "abstract"   : true,
//    "final"      : true,
//    "implements" : []
//}, {
//
//});

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                           TODO APP EXAMPLE                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
var NotesApp = (function(undefined) {

    // Create a namespace to contain references
    var my = { classes : {} };
    
    /**
     * Class Event - the base class for custom events
     */
    my.classes.Event = vjs("class Event", {

        /**
         * The event type is public so anyone can read it but you don't have to
         * worry. It is a "const" (read only) property, so once initialized by
         * the constructor, no one can modify it.
         */
        "const type" : undefined,

        "const target" : undefined,

        /**
         * Private flag used internally to store the "PropagationStopped" state
         * of the event (only useful if the event can bubble up).
         */
        "private _isPropagationStopped" : false,

        /**
         * Private flag used internally to store the "defaultPrevented" state
         * of the event.
         */
        "private _isDefaultPrevented" : false,

        /**
         * Private flag used internally to store the "immediatePropagationStopped"
         * state of the event.
         */
        "private _isImmediatePropagationStopped" : false,

        /**
         * That is how the outer world can check if the event propagation has
         * been stopped
         */
        isPropagationStopped : function() {
            return this._isPropagationStopped;
        },

        /**
         * That is how the outer world can check if the event's immediate
         * propagation has been stopped
         */
        isImmediatePropagationStopped : function() {
            return this._isImmediatePropagationStopped;
        },

        /**
         * That is how the outer world can check if the event's default behavior
         * has been prevented
         */
        isDefaultPrevented : function() {
            return this._isDefaultPrevented;
        },

        /**
         * Just sets the private "_isPropagationStopped" flag to true. Call this
         * to prevent the event from bubbling up (in case such bubbling is
         * implemented at all)
         */
        stopPropagation : function() {
            this._isPropagationStopped = true;
        },

        /**
         * Just sets the private "_isImmediatePropagationStopped" flag to true.
         * Call this from within an event listener to make the observer skip any
         * further event handlers execution.
         */
        stopImmediatePropagation : function() {
            this._isImmediatePropagationStopped = true;
            this._isPropagationStopped = true;
        },

        /**
         * Call this to mark the event as one who's default handling has been
         * prevented. Within a dispatch loop multiple handlers of the same event
         * can be executed. If any of them calls this method of the event, that
         * will toggle the _defaultPrevented flag and eventually, the dispatch
         * method of the observer will return false;
         */
        preventDefault : function() {
            this._isDefaultPrevented = true;
        },

        /**
         * The constructor just sets the event type. Note that once the type
         * property is set (to something other than undefined), it gets locked
         * and nothing can change it and introduce weird bugs that are very
         * difficult to find...
         */
        init : function(type) {
            this.type = type;
        }
    });

    my.classes.Observable = vjs("class Observable", {

        /**
         * Contains all the listeners grouped by the eventType that they are
         * attached to. Note that the value is assigned later inside the 
         * constructor.
         * @type Object
         * @protected
         */
        "public listeners" : {},

        "public delegates" : [],

        init : function(listeners) {

            // It is VERY important to define this (and any other objects or 
            // arrays) inside the constructor. Otherwise all the instances of 
            // this class would share the same object.
            //this.listeners = {};
            //this.delegates = [];
        },

        /**
         * The method used to attach event listeners.
         * @param {String} eventType The type of the event to listen to.
         * @param {Function} callback The event handler function
         * @return {Function} Returns a reference to the callback function that 
         *  has been used as listener. This can be very useful sometimes (when 
         *  the @callback function is inline or something like Function.bind() 
         *  was used...).
         */
        bind : function( eventType, callback ) {
            var set = this.listeners[ eventType ];
            if ( !set ) {
                this.listeners[ eventType ] = [ callback ];
            } else {
                this.listeners[ eventType ].push( callback );
            }
            return callback;
        },

        listenTo : function(observable, event, callback) {
            this.delegates.push({
                observable : observable,
                event      : event,
                handler    : observable.bind(event, callback)
            });
        },

        stopListening : function(observable, event, callback) {
            for ( var o, i = 0, l = this.delegates.length; i < l; i++ ) {
                o = this.delegates[i];
                if ( o.observable === observable ) {
                    if ( !event || o.event == event ) {
                        if (!callback || callback === o.callback) {
                            delete o;
                        }
                    }
                }
            }
        },

        /**
         * Unbinds event listener(s).
         * If no arguments are provided, all of the event listeners are removed
         * from the target (the observable instance). If only the eventType is
         * provided, than all the listeners for that event are removed.
         * If both arguments are provided, than ONLY that listener for that
         * eventType will be removed.
         * @param {String} eventType The type of the event to listen to.
         * @param {Function} callback The event handler function
         */
        unbind : function( eventType, callback ) {
            if (!eventType) {
                this.listeners = {};
            } else if (!callback) {
                this.listeners[eventType] = [];
            } else {
                var set = this.listeners[ eventType ];
                if (set) {
                    for ( var i = 0, l = set.length; i < l; i++ ) {
                        if (set[i] === callback) {
                            set.splice(i--, 1);
                        }
                    }
                }
            }
            return this;
        },

        /**
         * Dispatches an event on the target.
         * @param {String} eventType The type of the event to be dispatched.
         * Note that the eventType argument will be used to create new Event
         * instance that will be passed to the listeners as their first argument.
         * @param {any} data Additional data passed to the event handlers. Can be
         * anything, including array or object that contains multiple stuff ...
         * @return {Boolean} True if the event has not been cancelled (i.e. the
         * stopPropagation method has not been called from any of the listeners),
         * false otherwise.
         */
        trigger : function( event, data ) {
            if ( !(event instanceof my.classes.Event) ) {
                event = new my.classes.Event(event);
            }

            if (event.target === undefined) {
                event.target = this;
            }

            var set = this.listeners[ event.type ];
            if ( set ) {
                for ( var i = 0, l = set.length; i < l; i++ ) {
                    set[i].call(this, event, data);
                    if (event.isImmediatePropagationStopped()) {
                        break;
                    }
                }
            }

            if (this.eventTargetParent && !event.isPropagationStopped()) {
                this.eventTargetParent.trigger(event, data);
            }

            return !event.isDefaultPrevented();
        }

    });

    my.classes.Model = vjs("class Model extends Observable", {

        "const id" : undefined,

        "const idAttribute" : "id",

        "protected defaults" : {},

        /**
         * The model attributes are being stored here. The property is protected,
         * so only the methods this and sub-classes can access it.
         * @type Object
         * @protected
         */
        "protected attributes" : {},

        init : function( props ) {
            props = props || {};
            for ( var x in this.defaults ) {
                this.set(x, props.hasOwnProperty(x) ? props[x] : this.defaults[x]);
            }
        },

        url : function() {
            return this.id || "nullmodel";
        },

        save : function() {
            var url = this.url();
            if (this.collection) {
                url = this.collection.url() + "/" + url;
            }
            localStorage[url] = this.toString();
        },

        toJSON : function() {
            return this.attributes;
        },

        toString : function() {
          return JSON.stringify(this.toJSON());
        },

        /**
         * Gets the value of some attribute identified by name. If it doesn't
         * exist the result will be undefined.
         * @param {String} name The name of the attribute
         * @return {any}
         */
        get : function( name ) {
            return this.attributes[name]; // might be undefined
        },

        /**
         * Sets the value of model's attribute. If the value is the same as the
         * current one - nothing happens. Otherwise, several events are
         * dispatched.
         * 1. "beforechange:{name}" and "beforechange" - those can be canceled
         * 2. "change:{name}" and "change"
         * This will proxy to the "add" method if the attribute does not exist
         * yet, so the events will be different in this case.
         * @param {String} name The name of the attribute to be changed
         * @param {any} value The new value to set
         * @return {Model} Returns the instance so that this method can be chained
         */
        set : function( name, value ) {

            if ( !this.defaults.hasOwnProperty(name) ) {
                throw new Error(
                    'The "' + name + '" property is not part of the model schema'
                );
            }

            var old = this.attributes[name], eventData;

            // Don't do anything if the value is not different than the current
            if (value !== old) {
                eventData = {
                    attrName : name,
                    oldValue : old,
                    newValue : value,
                    model    : this
                };

                // One can listen to beforechange event and cancel the
                // modification if the value doesn't meet some validation rules
                if (this.trigger("beforechange:" + name, eventData) &&
                    this.trigger("beforechange", eventData))
                {
                    if ( name === this.idAttribute ) {
                        this.id = value;
                    }
                    this.attributes[name] = value;
                    this.trigger("change:" + name, eventData);
                    this.trigger("change", eventData);
                }
            }
            return this;
        },

        /**
         * Check if the model currently has an attribute named @name.
         * @param {String} name The name of the searched attribute.
         * @return {Boolean}
         */
        has : function( name ) {
            return this.attributes.hasOwnProperty(name);
        }    

    });

    /**
     *
     */
    my.classes.ModelCollection = vjs("class ModelCollection extends Observable", {

        "protected models" : [],

        "protected name" : "",

        "protected __ai__" : 1,

        "protected __idmap__" : {},

        "protected modelClass" : null,

        save : function() {
            if (!this.loading) {
                localStorage[this.name] = JSON.stringify(this.models);
            }
        },

        add : function(props) {
            props.id = this.__ai__;
            
            var model = new this.modelClass(props),
                len   = this.models.push(model);
            
            this.__idmap__[this.__ai__] = len - 1;
            this.__ai__++;

            model.eventTargetParent = this;
            model.collection = this;
            this.trigger("add", model);
            return model;
        },

        remove : function(id) {
            var i = this.__idmap__[id];
            if (i > -1) { 
                this.models.splice(i, 1);
                this.__idmap__[id] = -1;
                this.save();
                this.trigger("remove", id);
            }
        },

        find : function(id) {
            return this.models[this.__idmap__[String(id)]];
        },

        load : function() {
            this.loading = true;
            var models = JSON.parse(localStorage[this.name] || "[]");
            
            this.models = [];
            this.__idmap__ = {};
            this.__ai__ = 0;

            for ( var i = 0, l = models.length; i < l; i++ ) {
                var model = new this.modelClass(models[i]),
                    len   = this.models.push(model);
                this.__idmap__[model.id] = len - 1;
                this.__ai__ = Math.max(this.__ai__, model.id);
                model.eventTargetParent = this;
                model.collection = this;
                this.trigger("add", model);
            }

            this.__ai__++;
            this.loading = false;
            this.trigger("load");
        },

        init : function() {
            this.__super__();
            this.bind("add", this.save.bind(this));
            this.bind("change", this.save.bind(this));
            if (this.name) {
                this.load();
            }
        }
    });

    my.classes.Note = vjs("class Note extends Model", {
        defaults : {
            txt   : "",
            id    : null,
            mtime : 0
        },

        init : function( props ) {
            props = props || {};
            
            if (props.mtime === 0) {
                props.mtime = Date.now();
            }
            
            this.__super__(props);
            
            this.bind("change", function(e, data) {
                if (data.attrName != "mtime") {
                    this.set("mtime", Date.now());
                }
                //this.save();
            });
            if (this.get("mtime") === 0) {
                this.set("mtime", Date.now());
                if (this.collection) {
                    this.collection.save();
                }
            }
        }
    });

    my.classes.NotesCollection = vjs("class NotesCollection extends ModelCollection", {
        modelClass : my.classes.Note,
        name : "notes"
    });

    /** 
     * Class ListView - renders all the notes as a scrollable list. This also 
     * manages the selection and search behaviors.
     */
    my.classes.ListView = vjs("class ListView extends Observable", {

        /**
         * Will contain the ID of the currently selected model (if any). 
         * This will initially be null but once some item is selected, there is 
         * no way to un-select it without selecting another one or deleting 
         * everything from the list. 
         * @type {Number|null}
         */
        "protected selectedID" : null,

        /**
         * This is going to contain  references to the nested views so that we
         * can find them later (by model.id).
         */
        "public childViews" : null,

        /**
         * This method will be called when new model is added to the collection.
         * It will create new ListItem view and render and append it to the list.
         */
        "protected onNoteAdd" : function(event, note) {
            var view = new my.classes.ListItem(note);
            view.render();
            view.$el.appendTo(this.el);
            this.childViews[note.id] = view;
        },

        /**
         * This method will be called when a model is deleted from the 
         * collection. It will find the corresponding ListItem view and destroy 
         * it and it's HTML representation.
         */
        "protected onNoteRemove" : function(event, id) {
            var view = this.childViews[id], nextToSelect;
            if (view) {
                nextToSelect = view.$el.next();
                if (!nextToSelect.length) {
                    nextToSelect = view.$el.prev();
                }
            }
            
            this.childViews[id].destroy();

            if (nextToSelect.length === 1) {
                nextToSelect.trigger("click");
            }
        },

        "protected onItemClick" : function(e) {
            var id = $(e.target).closest("a").attr("data-model-id");
            if (id) {
                this.selectID(id);
            }
        },

        "public selectID" : function(id) {
            var li, model;
            if (id > -1) {
                model = this.collection.find(id);

                if (model) {
                    li = this.el.find('[data-model-id="' + id + '"]');
                    if (li.length === 1) {
                        li.trigger("focus").addClass("active")
                            .siblings().removeClass("active");
                    }
                } else {
                    id = -1;
                }

                if (id !== this.selectedID) {
                    this.selectedID = id;
                    this.trigger("change:selection");
                }
            }
        },

        /**
         * Attaches the event listeners. 
         * The "this.__super__();" call is needed to create the listeners object
         * attached to this instance. By using this.listenTo(...) instead of 
         * this.collection.bind(...) the event handlers are stored in this 
         * instance's listeners and not in the collection's one. This means we 
         * don't need to unbind before destroying the ListView - that will 
         * happen automatically.  
         */
        init : function( collection ) {
            //this.__super__();
            this.childViews = {};
            this.collection = collection;
            this.el = $('<div class="list"/>');
            this.listenTo(this.collection, "add"   , this.onNoteAdd   .bind(this));
            this.listenTo(this.collection, "remove", this.onNoteRemove.bind(this));
            this.el.on("click", "a", this.onItemClick.bind(this));
        },

        render : function() {
            return this;
        },

        search : function(q) {
            if (!q) {
                $.each(this.childViews, function(id, view) {
                    view.render().$el.show();
                });
                return;
            }
            
            $.each(this.childViews, function(id, view) {
                var noteText = view.model.get("txt"),
                    index    = noteText.indexOf(q),
                    snippet,
                    endIndex,
                    start,
                    end;

                if (index > -1) {
                    endIndex = index + q.length;
                    start    = Math.max(0, index - 10);
                    end      = Math.min(start + 100, endIndex + 30);
                    snippet  = noteText.substring(start, index) +
                        '<span class="search-match">' + 
                        noteText.substr(index, q.length) +
                        '</span>' +
                        noteText.substr(index + q.length, 10);
                    view.$title.html(snippet);
                    view.$el.show();
                } else {
                    view.$el.hide();
                }
            });
        }
    });

    // ListItem
    my.classes.ListItem = vjs("class ListItem extends Observable", {
        
        "public onNoteTextChange" : function(event, data) {
            this.setTitle(data.newValue);
        },
        
        "public onNoteMdateChange" : function(event, data) {
            this.setFooter(data.newValue);
        },

        init : function( model ) {//console.log("ListItem.init(" + model.id + ")");
            this.__super__();
            
            this.model   = model;
            
            this.$el = $('<a class="note" tabindex="auto" />').attr({
                "href" : "#notes/" + model.id,
                "data-model-id" : model.id
            });

            this.el = this.$el[0];
            
            this.$title = $('<h4 />').appendTo(this.$el);
            
            this.$footer = $('<p class="list-group-item-text text-muted"/>')
                .appendTo(this.$el);
            
            this.listenTo(this.model, "change:txt"  , this.onNoteTextChange .bind(this)); 
            this.listenTo(this.model, "change:mtime", this.onNoteMdateChange.bind(this));

        },

        destroy : function() {
            this.$el.remove();
            return this;
        },

        render : function() {
            this.setTitle(this.model.get("txt"));
            this.setFooter(this.model.get("mtime"));
            return this;
        },

        setTitle : function(txt) {
            this.$title.text(txt.substr(0, 30) + "...");
            return this;
        },

        setFooter : function(x) {
            var mod         = new XDate(x),
                now         = new XDate(),
                txt         = "",
                diff        = mod.diffMilliseconds(now), 
                diffSeconds = Math.floor(mod.diffSeconds(now)),
                diffMinutes = Math.floor(mod.diffMinutes(now)),
                diffHours   = Math.floor(mod.diffHours(now));

            do {
                if (diffSeconds <= 1) {
                    txt = "now";
                    break;
                }

                if (diffSeconds <= 60) {
                    txt = diffSeconds + " seconds ago";
                    break;
                }

                if (diffMinutes <= 60) {
                    txt = diffMinutes + " minutes ago";
                    break;
                }

                if (diffHours < 24) {
                    diffMinutes -= diffHours * 60;
                    txt = diffHours + " hours and " + diffMinutes + 
                        " minutes ago";
                    break;
                }

                if (diffHours < 48) {
                    txt = "Yesterday at " + mod.toString("hh:mm tt");
                    break;
                }

                txt = mod.toString("MMMM dd yyyy hh:mm tt");

            } while(false);
            
            this.$footer.text(txt);

            return this;
        }
    });


    
    my.classes.App = vjs("class App extends Observable", {

        "notes" : null,

        "protected currentNote" : null,

        init : function( container ) {
            //this.__super__();
            
            this.container = $(container);

            this.notes = new my.classes.NotesCollection();

            // Create the ListView
            this.listView = new my.classes.ListView(this.notes);

            this.listView.render().el.appendTo(
                this.container.find(".notes-list")
            );

            
            this.attachEvents();
            this.notes.load();
        },

        setCurrentNote : function(note) {
            if ( note instanceof my.classes.Note ) {
                this.currentNote = note;
            }
        },

        getCurrentNote : function() {
            return this.currentNote;
        },

        "protected attachEvents" : function() {
            var app = this;

            //this.notes.bind("add", function(e, model) {
            //    var view = new my.classes.ListItem(model);
            //    view.render();
            //    view.$el.appendTo(".notes-list .list-group");
            //    view.$el.trigger("click");
            //});

            // On text change edit the current note
            $(this.container).on("input", "textarea", function() {
                var note = app.getCurrentNote();
                if (note) {
                    note.set("txt", this.value);
                }
            });

            $(this.container).on("input", 'input[type="search"]', function() {
                app.listView.search(this.value);
            });

            $(document).on("click", ".btn-add", function() {
                app.notes.add({ txt : "New note" });
            });

            $(document).on("click", ".notes-list [data-model-id]", function() {
                $(this).trigger("focus");
                app.showNote(this.getAttribute("data-model-id"));
            });

            //window.addEventListener("hashchange", function() {
            //    app.route();
            //}, false);
            
            this.listenTo(this.listView, "change:selection", function() {
                $(".btn-delete").removeAttr("disabled");
            });

            $(document).on("click", ".btn-delete", function(e) {
                var note = app.getCurrentNote();
                e.preventDefault();
                if (note) {
                    app.notes.remove(note.id);
                }
            });
        },

        route : function() {
            var hash = location.hash.replace(/^#/, ""), m;

            if ((m = hash.match(/^notes\/(\d+)$/))) {
                this.showNote(m[1]);
            }
            else if (/^notes\/delete$/.test(hash)) {
                //this.notes.remove();
            }
            else {
                this.showBlank()
            }
        },

        showNote : function(id) {
            var note = this.notes.find(id);
            if (note) {
                this.listView.selectID(id);
                //console.log("txt: ", note.get("txt"), id);
                $("textarea").val(note.get("txt"));
                this.setCurrentNote(note);
            }
        },

        showBlank : function() {}
        
    });

    return my.classes.App;

})();
