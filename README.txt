

To build:
    1. cd to the project root
    2. run "grunt"
    3. The generated afiles are at build/vjs-lib.js
       and build/vjs-lib.min.js.

To test:
    1. cd to the project directory
    2. run "grunt"
    3. run "npm test"

