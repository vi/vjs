
module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json'),
        srcFiles = [
            "src/constants.js",
            "src/utils.js",
            "src/vjs-core-2.js"
        ];

    // Project configuration.
    grunt.initConfig({
        pkg: pkg,

        // uglify -------------------------------------------------------------
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> ' +
                        ' <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src : 'build/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            }
        },

        // concat -------------------------------------------------------------
        concat: {
            options: {
                process: function(src, filepath) {
                    var out = '// Source: ' + filepath + '\n' + src;

                    // Remove "use strict"
                    if ( filepath != "src/header.js" ) {
                        out = out.replace(
                            /(^|\n)[ \t]*('use strict'|"use strict");?\s*/g,
                            '$1'
                        );
                    }

                    return out;
                }
            },
            dist: {
                src   : ["src/header.js"].concat(srcFiles)
                        .concat(["src/footer.js"]),
                dest  : 'build/<%= pkg.name %>.js',
                nonull: true
            },
            tests: {
                src : ["src/header.js"].concat(srcFiles)
                        .concat(["src/tests-footer.js", "src/footer.js"]),
                dest: 'build/<%= pkg.name %>.test.js',
                nonull : true
            }
        }
    });

    // Load the plugins -------------------------------------------------------
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    // tasks ------------------------------------------------------------------
    grunt.registerTask('default', ['concat:dist', 'concat:tests', 'uglify:build']);

};

