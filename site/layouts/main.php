<!DOCTYPE html>
    <html lang="en">
        <meta charset='utf-8' />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>VJS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/js/vendor/google-code-prettify/prettify.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="/js/vendor/jquery-1.10.2.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/vendor/json2.js"></script>
        <script src="/js/vendor/bootstrap.js"></script>
        <script src="/js/vendor/underscore.js"></script>
        <script src="/js/vendor/backbone.js"></script>
        <script src="/js/vendor/google-code-prettify/prettify.js"></script>
        <script src="/js/main.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <b style="color: crimson; font-family: cambria; vertical-align: middle; line-height: 0; position: relative; top: -4px; font-size: 38px;">VJS</b>
                        JavaScript OOP Library
                    </a>
                </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="#">Link</a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 sidebar">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="side-nav">
                                        <li>Documentation
                                            <ul>
                                                <li><a href="/docs/introduction.html">Introduction</a></li>
                                                <li>The Basics</li>
                                                <li><a href="/docs/namespaces.html">Namespaces</a></li>
                                                <li><a href="/docs/properties.html">Properties</a></li>
                                                <li><a href="/docs/constants.html">Constants</a></li>
                                                <li><a href="/docs/autoloading.html">Autoloading Classes</a></li>
                                                <li>Constructors and Destructors</li>
                                                <li><a href="/docs/visibility.html">Visibility</a></li>
                                                <li><a href="/docs/inheritance.html">Inheritance</a></li>
                                                <li>Scope Resolution</li>
                                                <li>Static Keyword</li>
                                                <li><a href="/docs/abstract.html">Class Abstraction</a></li>
                                                <li>Object Interfaces</li>
                                                <li>Traits</li>
                                                <li>Overloading</li>
                                                <li>Object Iteration</li>
                                                <li>Magic Methods</li>
                                                <li><a href="/docs/final.html">Final Keyword</a></li>
                                                <li>Object Cloning</li>
                                                <li>Comparing Objects</li>
                                                <li>Type Hinting</li>
                                                <li>Late Static Bindings</li>
                                                <li>Objects and references</li>
                                                <li>Object Serialization</li>
                                                <li>Changelog</li>
                                            </ul>
                                        </li>
                                        <li><a href="/tests/index.html">Unit Tests</a></li>
                                        <li><a href="/tests/speed-tests2.html">Speed Tests</a></li>
                                        <li><a href="#">Download</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 main">
                            <div class="errors"></div>
                            <div class="page-container">
                            <?php 
                            if (isset($url) && !empty($url) && $url != '/') {
                                include $url;
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <p class="text-muted text-center">&copy; Footer stuff</p>
        </div>
    </body>
</html>
