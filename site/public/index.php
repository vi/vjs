<?php


$url = isset($_SERVER['REDIRECT_URL']) && !empty($_SERVER['REDIRECT_URL'])
    ? $_SERVER['REDIRECT_URL']
    : '/index.html';

$url = 'pages' . $url;

include '../layouts/main.php';