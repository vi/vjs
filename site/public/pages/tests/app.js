(function() {
    var
    reSpaces       = /\s+/,
    reSpaceTrim    = /^\s*|\s*$/,
    rePrintfTokens = /%(\.(\d))?([a-zA-Z%])/g,
    slice          = Array.prototype.slice,
    toString       = Object.prototype.toString,
    //hasOwn         = Object.hasOwnProperty,
    reservedWords  = {
        "public"    : 1,
        "private"   : 1,
        "protected" : 1,
        "const"     : 1,
        "static"    : 1,
        "final"     : 1,
        "abstract"  : 1
    },
    errors = {
        // General message for unknown error
        0   : "Unknown VJS error",

        // 100-199 - Parse errors
        100 : "Invalid property declaration.",
        101 : "Invalid (empty) property declaration.",
        102 : "Invalid property name '%s' (reserved word).",
        103 : "Invalid property declaration. Unrecognised token '%s'.",
        104 : "Invalid property declaration. Missing name.",
        105 : "Unrecognised class modifier '%s'",
        106 : "Invalid class properties declaration",
        107 : "Invalid class declaration '%s'. Missing class name or path.",

        // 200-299 Runtime errors
        201 : "Cannot access the property '%s' from out of scope",
        202 : "No such class '%s'.",
        203 : "Cannot change the value of the constant '%s' property.",
        204 : "Cannot inherit from undefined class '%s'.",
        205 : "Cannot inherit from class '%s'",
        206 : "Cannot inherit from final class '%s'",
        207 : "Cannot create an instance of the abstract class '%s'.",
        208 : "The abstract property or method '%s' cannot be private",
        209 : "The abstract property or method '%s' cannot be final",
        210 : "The property '%s' is abstract, therefore the class '%s' must " +
              "also be defined as abstract",
        211 : "The final property or method '%s' cannot be overridden",
        212 : "The class '%s' must implement the '%s' property or method",
        213 : "The method '%s' of class '%s' does not have a super method"
    };

    /**
     * Trims the white space on the both sides of a string. Delegates
     * to the native trim if it exists.
     * @param {String} str The string to trim
     * @return String
     */
    function trim(str) {
        return str.trim ?
            str.trim() :
            String(str).replace(reSpaceTrim, "");
    }

    /**
     * JavaScriipt implementation of the most useful parts of the C printf
     * function
     */
    function printf() {
        var args = slice.call(arguments),
            len  = args.length,
            tpl;

        if ( !len ) {
            return "";
        }

        tpl = args.shift();

        return tpl.replace(
            rePrintfTokens,
            function(match, precision, precisionDigit, specifier/*, pos, input*/) {
                var replacement = args.shift();

                if (replacement === undefined) {
                    return "";
                }

                switch ( specifier ) {

                    // Signed decimal integer
                    case "d":
                    case "i":
                        return parseInt(replacement, 10);

                    // Unsigned decimal integer
                    case "u":
                        replacement = parseInt(replacement, 10);
                        if ( !isNaN(replacement) && isFinite(replacement) ) {
                            replacement = Math.abs(replacement);
                        }
                        return replacement;

                    // Unsigned hexadecimal integer 7fa
                    case "x":
                        replacement = parseInt(replacement, 10);
                        if ( !isNaN(replacement) && isFinite(replacement) ) {
                            replacement = Math.abs(replacement).toString(16);
                        }
                        return replacement;

                    // Unsigned hexadecimal integer (uppercase) 7FA
                    case "X":
                        replacement = parseInt(replacement, 10);
                        if ( !isNaN(replacement) && isFinite(replacement) ) {
                            replacement = Math.abs(replacement).toString(16).toUpperCase();
                        }
                        return replacement;

                    // Decimal floating point, lowercase 392.65
                    // TODO: This does not work as it should!!! (not used here)
                    //case "f":
                    //    return parseFloat(replacement).toFixed(
                    //        parseInt(precisionDigit, 10)
                    //    );

                    //Decimal floating point, uppercase 392.65
                    // TODO: This does not work as it should!!! (not used here)
                    //case "F":
                    //    return parseFloat(replacement).toFixed(
                    //        parseInt(precisionDigit, 10)
                    //    ).toUpperCase();

                    case "c":
                    case "s":
                        return String(replacement);

                    case "%":
                        return "%";

                    default:
                        return "%" + specifier;
                }
            }
        );
    }

    /**
     * Class VJSError extends Error. This is used to create Error object
     * by providing an error code to the constructor and other variables
     * that are substituted into the message using printf. This class
     * exists for several reasons:
     * 1. To make it possible to store all the error messages in one place
     * 2. To make it easier to implement some kind of message translations
     *    later
     * 3. To allow for test to compare errors by their code instead of by
     *    the message that can vary
     * @param {Number|String} code The error code as number or as numeric
     *    string
     * @param {*} any Zero or more additional arguments to be substituted into
     *    the message
     * @return {VJSError}
     */
    function VJSError( code ) {
        var params = slice.call(arguments, 1);
        this.code = code || 0;
        params.unshift(errors[this.code]);
        this.message = printf.apply({}, params);
    }

    function warn() {
        return console.warn.apply(console, arguments);
    }

    VJSError.prototype = Object.create(Error.prototype);


    /**
     * Creates a property object from the given declaration and value
     * @param {String} declaration The property declaration
     * @param {*} value The property value
     * @return {Object} The property object
     * @throws {Error} Upon validation failure
     */
    function Property(declaration, value, owner) {

        var arr  = trim(String(declaration || "")).split(reSpaces),
            len  = arr.length,
            name;

        name = arr[--len];

        // missing name
        if (!name) {
            throw new VJSError(104);
        }

        // name is a reserved word
        if (reservedWords.hasOwnProperty(name)) {
            throw new VJSError(102, name);
        }

        this.name = name;

        while ( len ) {
            switch (arr[--len]) {
                case "public":
                    this.isPrivate   = false;
                    this.isProtected = false;
                    this.isPublic    = true ;
                break;
                case "private":
                    this.isPrivate   = true ;
                    this.isProtected = false;
                    this.isPublic    = false;
                break;
                case "protected":
                    this.isPrivate   = false;
                    this.isProtected = true ;
                    this.isPublic    = false;
                break;
                case "const":
                    this.isConst = true;
                break;
                case "static":
                    this.isStatic = true;
                break;
                case "final":
                    this.isFinal = true;
                break;
                case "abstract":
                    this.isAbstract = true;
                break;
                default:
                    throw new VJSError(103, arr[len]);
            }
        }

        // Check for invalid combinations
        if ( this.isAbstract ) {
            if ( this.isPrivate ) { // Can't have abstract + private
                throw new VJSError(208, name);
            }
            if ( this.isFinal ) { // Can't have abstract + final
                throw new VJSError(209, name);
            }
        }

        this.owner = owner;
        this.value = value;

        //PropertyCache[declaration] = this;
    }

    Property.prototype = {
        isConst     : false,
        isStatic    : false,
        isFinal     : false,
        isAbstract  : false,
        value       : undefined,
        isPublic    : true,
        isProtected : false,
        isPrivate   : false,
        owner       : undefined,

        // make sure that the properties have their declaration in common format so
        // that they can be compared between each other. Although the modifiers
        // order doesn't matter, the declaration property will always be in the
        // format: "[abstract] [final] [const] access [static] name"
        getDeclaration : function() {
            var decl = [];

            if (this.isAbstract) {
                decl.push("abstract");
            }
            if (this.isFinal) {
                decl.push("final");
            }
            if (this.isConst) {
                decl.push("const");
            }

            if (this.isPublic) {
                decl.push("public");
            } else if (this.isPrivate) {
                decl.push("private");
            } else if (this.isProtected) {
                decl.push("protected");
            }

            if (this.isStatic) {
                decl.push("static");
            }

            decl.push(this.name);

            return decl.join(" ");
        },

        clone : function(newValue, newOwner) {
            return {
                isConst        : this.isConst,
                isStatic       : this.isStatic,
                isFinal        : this.isFinal,
                isAbstract     : this.isAbstract,
                value          : newValue,
                isPublic       : this.isPublic,
                isProtected    : this.isProtected,
                isPrivate      : this.isPrivate,
                owner          : newOwner,
                name           : this.name,
                getDeclaration : this.getDeclaration,
                clone          : this.clone
            };
        }
    };

    /**
     * This is used to create a pointer property on the given object. It should
     * "mirror" the public properties from the internal instance to the external
     * one.
     * @param {Object} obj The object to augment
     * @param {Object} store The object that contains the real property
     * @param {Property} prop The property object to proxy
     * @return void
     */
    function createProxy(obj, store, prop) {
        var name = prop.name;
        Object.defineProperty(obj, name, {
            enumerable  : true,
            configurable: false,
            get : function() {
                return store[name];
            },
            set : function(value) {
                if (prop.isConst && store[name] !== undefined) {
                    throw new VJSError(203, name);
                }
                store[name] = value;
            }
        });
    }

    /**
     * Creates a proxy property that will in fact reject access to itself. This
     * is used to fix the shadowing behavior in JavaScript which can cause a lot
     * of bugs. It is far more useful to just throw an error and warn the
     * developer early.
     * @param {Object} obj The object to augment
     * @param {String} propName The name of the property to reject
     * @return void
     */
    function createRejector(obj, propName) {
        Object.defineProperty(obj, propName, {
            enumerable  : false,
            configurable: false,
            get : function() {
                throw new VJSError(201, propName);
            },
            set : function() {
               throw new VJSError(201, propName);
            }
        });
    }

    function augment(obj, context, props) {
        var key, prop;
        for (key in props) {
            //if (key == "constructor") { continue; }
            prop = props[key];
            if (toString.call(prop.value) == "[object Function]") {
                context[prop.name] = prop.value.bind(context);
            } else {
                if (prop.isConst) {
                    (function() {
                        var _value = prop.value;
                        Object.defineProperty(context, prop.name, {
                            enumerable  : true,
                            configurable: false,
                            get : function() {
                                return _value;
                            },
                            set : function(value) {
                                if (_value !== undefined) {
                                    throw new VJSError(203, name);
                                }
                                _value = value;
                            }
                        });
                    })();
                } else {
                    context[prop.name] = prop.value;
                }
            }
            if (prop.isPublic) {
                createProxy(obj, context, prop);
            } else {
                createRejector(obj, prop.name);
            }
        }
    }

    function Class() {
        return {};
    }

    Class.extend = function extend(proto) {
        var //_extending = false,
            _props       = {},
            _statics     = {},
            _staticSelf  = {},
            _super       = this.__properies__       || {},
            _superStatic = this.__staticProperies__ || {},
            tmp, key, base;

        for (key in _superStatic) {
            tmp = _superStatic[key];
            if (!tmp.isPrivate) {
                _statics[key] = tmp;
            }
        }

        for (key in _super) {
            tmp = _super[key];
            if (!tmp.isPrivate) {
                _props[tmp.name] = tmp;
            }
        }

        for (key in proto) {
            tmp  = new Property(key, proto[key]);
            base = tmp.isStatic ? _statics : _props;

            if (tmp.isFinal && tmp.isPrivate) {
                warn(
                    'Using "private" and "final" together makes no sence: "' +
                    tmp.getDeclaration() + '"'
                );
            }

            // Check for "final" and prevent from overriding
            if (base[tmp.name] && base[tmp.name].isFinal) {
                throw new VJSError(211, tmp.name);
            }

            base[tmp.name] = tmp;
        }

        _statics.__staticProperies__ = new Property("__staticProperies__", _statics);
        _statics.__properies__ = new Property("__properies__", _props);

        // Create static properties
        augment(F, _staticSelf, _statics);

        /**
         * The internal constructor of any class
         */
        function F()
        {
            // "self" is the extended internal version of "this". "this" only
            // contains the public properties and methods, while "self" can also
            // access protected and private members. In order for this to work,
            // all the instance methods are bound to self instead of this. This
            // way, the code in the function bodies can acces the private and
            // protected members via "this".
            var self = {};

            // Apply properties
            augment(this, self, _props);

            // Call the user-defined constructor (if any)
            if (typeof self.init == "function") {
                self.init.apply(self, arguments);
            }

            // Return the public instance
            return this;
        }

        // If the _extending flag is set to true it means we are making the
        // dummy invocation which is only used to maintain (empty) prototype
        // chain so that the "instanceof" operator works properly
        F.prototype = Object.create(this.prototype);

        // We need to reset the "constructor" property of the prototype back to
        // it's original value. Doing so in the typical will make that property
        // enumerable so use the Object.defineProperty instead.
        // F.prototype.constructor = F;
        Object.defineProperty(F.prototype, "constructor", { value : F });

        // make the new class extendable the same way
        F.extend = extend;

        // Return the constructor
        return F;
    };

    window.Class = Class;
    ////////////////////////////////////////////////////////////////////////////
/*
    var tests = [
        {
            name: "final properties can be inherited",
            fn: function(err, ok) {
                var Base = Class.extend({ "final a" : 1 });
                var Sub = Base.extend({}), sub = new Sub();
                if (sub.a === 1) {
                    ok("Yes");
                } else {
                    err("No");
                }
            }
        },
        {
            name: "Trying to redefine final property throws an error",
            fn: function(err, ok) {
                var Base = Class.extend({ "final a" : 1 });
                try {
                    Base.extend({ "a" : 2 });
                    err("No");
                } catch (ex) {
                    ok("Yes: " + ex);
                }
            }
        }
    ];

    tests.forEach(function(entry) {
        entry.fn(function(err) {
            console.warn(entry.name + ": " + err);
        }, function(out) {
            console.info(entry.name + ": " + out);
        });
    });

    var Shape = Class.extend({
        "protected width" : 1,
        "protected height": 1,
        "public area" : function() {},
        "final public init" : function(w, h) {
            this.width  = w || 0;
            this.height = h || 0;
        },
        "public dump" : function() {
            var out = {};
            for (var k in this) {
                out[k] = this[k];
            }
            return out;
        }
    });

    var Rect = Shape.extend({
        "public area" : function() {
            return this.width * this.height;
        }
    });

    // var Circle = Shape.extend({
    //     "protected r" : null,
    //     "public init" : function(cx, cy, r) {
    //         this.cx = cx || 0;
    //         this.cy = cy || 0;
    //         this.r  = r  || 0;
    //     },
    //     "public area" : function() {
    //         return this.width * this.height;
    //     }
    // });

    var rect = new Rect(10, 20);
    //console.dir(rect);
    console.dir(rect);
    console.log("rect.dump: ", rect.dump());
    //console.log("rect.area: ", rect.area());
    //console.log("rect instanceof Rect: ", rect instanceof Rect);
    //console.log("rect instanceof Shape: ", rect instanceof Shape);
    window.rect = rect;


    //var circle = new Circle(15, 25, 345);
    //console.dir(rect);
    //console.log("circle: ", circle/*, circle.area()*//*);
*/
})();
