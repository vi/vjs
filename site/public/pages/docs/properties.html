<article>
    <h1>Properties</h1>

    <p>
        The class instances are objects containing zero or more properties.
        These properties are declared  in the second argument of the "vjs"
        function using the JavaScript object literal syntax.
    </p>
<pre class="prettyprint lang-js"><code>vjs("<b>class</b> Something", {
    a : 1,
    b : 2
});</code></pre>
    <p>
        In this simple example all the instances of class something will have 
        two properties: a and b.
    </p>
    <blockquote>
        <p>
        Note that the second (optional) argument to the vjs function is like a 
        prototype for the instances, but it IS NOT the prototype that you know 
        from regular JavaScript functions!
        </p>
    </blockquote>
    <br>
    <p>
        One of the major concepts behind VJS is that we can use the fact that 
        the property labels of the plain object are not restricted to being 
        valid variable names (only if they are quoted). Thus we can use the 
        label not only to specify the property name, but also include some 
        meta-data like the access restrictions for example.
    </p>
<pre class="prettyprint lang-js"><code>vjs("<b>class</b> Something", {
    "<b>protected</b> a" : 1, // protected property
    "<b>private</b> b"   : 2, // private property
    "<b>static</b> c"    : 3, // static property
    "<b>final</b> d"     : 4, // final property
    "<b>const</b> e"     : 5  // const property
});</code></pre>
    <br>
    <p>
        Off course combinations are also possible:
<pre class="prettyprint lang-js"><code>vjs("<b>class</b> Something", {
    "<b>protected static</b> a" : 1,
    "<b>private const</b> b"    : 2,
    "<b>final static</b> c"     : 3,
    "<b>final public</b> d"     : 4,
    "<b>static const</b> e"     : 5,
    // ...
});</code></pre>
    </p>
    <br>
    <p>
        The pattern for the property labels is <code><b>[keywords] name</b></code>, 
        where [keywords] is space-separated list of zero or more keywords which
        can be <code>final, static, const, [access]</code> and the [access] is 
        one of <code>public, private or protected</code>. The order of the 
        keywords doesn't matter but the name of the property must be the last
        token.
    </p>

    <h3>Methods</h3>
    <p>
        The methods are properties just like the others, but their value is a 
        function.
    </p>
    <blockquote>
        <p>
            The function you provide while creating the class will be executed 
            using "apply" when the method is invoked. Internally, it is not 
            attached to the instance directly, but will be wrapped in another 
            function.
        </p>
    </blockquote>

    <h3>Object and Array Properties</h3>
    <p>
        At first sight, object and array properties should be just like the 
        others, but that is not the case. As you know the objects (which 
        includes the arrays) are passed by reference in JavaScript. This is the 
        correct behavior in most cases and is generally very helpful for the 
        performance but it might lead to big problems in some cases.
    </p>
    <p>
        The problem is that sometimes you may need a copy of the object, 
        forgetting that what you get is actually  a reference. Here is an 
        example with classical JavaScrpt:
    </p>
<pre class="prettyprint lang-js"><code>function Person() {}

Person.prototype = {
    ownObjects : [],
    buy : function(item) {
        this.ownObjects.push(item);
    }
};</code></pre>
    <p>
        Certainly most of you have had this problem before. And some of you 
        might currently having it without event noticing it... Here is what 
        happens when the above code is used:
    </p>
<pre class="prettyprint lang-js"><code>var John = new Person();
var Bob  = new Person();

John.buy("House");
John.buy("Car");
Bob .buy("Boat");

alert(John.ownObjects); // alerts "House, Car, Boat"
alert(Bob.ownObjects);  // alerts "House, Car, Boat"
</code></pre>
    <p>
        If you only intend to use a single instance of such class this code will
        work well enough. Otherwise the problem is obvious - all the instances 
        share the same "ownObjects" object because they just have references to 
        the same object defined in the class prototype.
    </p>
    <p>
        Fortunately, the solution is pretty simple - just remove such objects 
        and arrays from the prototype and initialize them in the constructor 
        like so <code>this.ownObjects = [];</code>. That remains however very 
        easy to forget. You just need to build a habit to always keep an eye 
        over the prototypes and never place objects and arrays there, unless 
        off course that "sharing" is the desired effect.
    </p>

    <h4><b>The VJS Implementation</b></h4>
    <p>
        Even though the problem is solvable, it is really bad if we need to 
        "fix" something when what we actually expect is to have this handled 
        automatically. The practice shows that when one designs something that 
        is supposed to act as a template (the prototype in this case), sharing 
        the same properties is almost NEVER desired. That if why VJS has a 
        different approach.
    </p>
    <p>
        In VJS all the properties and methods are attached directly to the 
        instance (as own properties). The prototypes are kept empty and the 
        prototype chain is maintained only to enable some things like 
        "instanceof" to work correctly. Each instance will receive a copy of 
        the object properties instead of a reference if those are plain objects 
        or arrays. The same example from above translated to VJS can look 
        somewhat better:
    </p>
<pre class="prettyprint lang-js"><code>vjs("class Person", {
    ownObjects : [],
    buy : function(item) {
        this.ownObjects.push(item);
    }
});
var John = new Person();
var Bob  = new Person();

John.buy("House");
John.buy("Car");
Bob .buy("Boat");

alert(John.ownObjects); // alerts "House, Car"
alert(Bob.ownObjects);  // alerts "Boat"
</code></pre>
    <p>
        As you can see this is more intuitive. Actually, in real life it can 
        solve a lot more problem than what is seen here. If you have to 
        initialize your object properties in the constructors (or other methods),
        you might end up with additional complications. For example having
        a chain of few classes inheriting each other might require a carefully 
        managed set of super calls.
    </p>
    <p>
        To assign the object properties by value, VJS creates a deep copy of 
        them for each instance. This is not something that can be assumed to be 
        always possible so it is only done for arrays and plain objects (object 
        literals and those created with new Object()). Fortunately, this will 
        be the typical case. When people define a class template (or a prototype)
        and place an object property inside it, it either be an "inline" object 
        like "[...]" or "{...}" or a reference to something that already exists.
        If it's an inline object, than it's supposed to act as some kind of 
        internal storage or namespace and contain some stuff that the instance 
        will work with. VJS will create new copy of those object so that will 
        work correctly. 
    </p>
    <p>
        Having a custom objects (not plain objects or arrays) as properties 
        will result in different behavior. They will be assigned as a reference
        instead of being copied. Here are some example: 
    </p>
<pre class="prettyprint lang-js"><code>
/**
 * A person here is just somebody having a name that cannot change later
 */
vjs("class Person", {
    "const name" : undefined,
    "public init" : function(name) {
        this.name = String(name || "Anonimous");
    }
});

vjs("class BankAccount", {
    "const id" : undefined,
    "private money" : 0,
    "private owner" : null,
    "private bank"  : null,

    init : function(id, person, bank) {
        this.id = id;
        this.owner = person;
        this.bank = bank;
    }
});

vjs("class Bank", {
    "private accountCounter" : 0,
    "private accounts" : {},

    // Anyone can give money given the receiver's accountID and some amount
    "addMoney" : function(accountID, amount) {
        if (this.accounts.hasOwnProperty(accountID)) {
            this.accounts[accountID].money += amount;
            return true;
        }
        alert("No such bank account '" + accountID + "'.");
        return false;
    },

    // Only the account owner can withdraw money if he provides a valid 
    // accountID and if the amount to withdraw is less than or equal to the 
    // money available
    "withdraw" : function(accountOwner, accountID, amount) {
        var account = this.accounts[accountID];
        if (!account) {
            alert("No such bank account '" + accountID + "'.");
            return false;
        }
        if (account.owner !== accountOwner) {
            alert('The person "' + accountOwner + '" attempted to withdraw ' + 
            'money from another one's account!');
            return false;
        }
    },

    "createAccount" : function(person) {
        var id;
        if (person instanceof Person) {
            id = "account" + (++this.accountCounter);
            this.accounts[id] = new BankAccount(id, person, this);
            return id;
        }
        alert("'createAccount' expects a Person as argument.");
    }
});
</code></pre>
</article>