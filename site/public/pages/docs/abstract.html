<div>
<h1>Class Abstraction</h1>
<p>
    The abstract classes have two major goals - to act as base class for 
    something else and to force their children to provide certain 
    implementations. This might sound confusing at first , but it makes a 
    lot of sense and  is in fact one of the most often used OOP features.
</p>
<p>
    For example, consider what would you do if you have to create a set of 
    different widgets, each of which is represented by it’s own class. Most 
    likely those widgets will have at least some common functionality. To 
    reduce the code repetition, it would be wise to have a common base class 
    that defines the common parts and then the widget subclasses can just 
    inherit those common parts and use them:
</p>
<p>
<pre class="prettyprint lang-js">
<code>/**
 * Example base class for widgets. Provides the common 
 * method "disabled" that can be used to get or set the 
 * disabled state of the widget subclass instances.
 */
vjs("<b>class</b> WidgetBase", {

    /**
     * The protected property that is going to store the 
     * disabled state of the widget.
     */
    "<b>protected</b> isDisabled" : false,

    /**
     * Get or set the disabled state of the widget.
     * @param {Boolean} bDisabled The new state (will be 
     * converted to boolean)
     * @return {WidgetBase|Boolean} When called without 
     * arguments acts as a getter and returns the current 
     * disabled state. Otherwise sets the disabled state 
     * and returns the instance.
     */
    "<b>public</b> disabled" : function( bDisabled ) {
        if ( !arguments.length ) {
            return this.isDisabled;
        }
        this.isDisabled = !!bDisabled;
        return this;
    }
});

// Create two empty subclasses of WidgetBase
vjs("<b>class</b> NumberInput <b>extends</b> WidgetBase");
vjs("<b>class</b> EmailInput <b>extends</b> WidgetBase");

// Test it
var numberWidget = new NumberInput();
var emailWidget  = new EmailInput();

numberWidget.disabled(); // -> false
emailWidget .disabled(); // -> false

numberWidget.disabled(1); // Disable it

numberWidget.disabled(); // -> true
emailWidget .disabled(); // -> false
</code></pre>
</p>
<p>
    So far that is a typical inheritance solution that appears to solve all 
    our issues. However, in practice one application will contain a lot more 
    than these few classes and hierarchies deeper than just two levels like 
    in the above example. The applications are usually developed in multiple 
    stages. The first and most important one is where the basic application 
    architecture gets defined. At that point, the author roughly implements 
    his first ideas. To make the app scalable it is of critical significance 
    to consider some “extension points” in that stage. 
</p>
<p>
    To make this more visual, lets reconsider the widgets example from above. 
    It might look OK this way, but if we look deeper we should eventually 
    realise the bigger picture. The purpose of that hierarchy is not only to 
    prevent code repetition but also to decouple our code and give it some 
    additional flexibility. Anybody can create new types of widgets later and 
    all he needs to do is to inherit from the parent “WidgetBase”. That’s great, 
    but in reality you might not be able to remember the exact requirements for 
    the widget classes (because you wrote the first few widgets long time ago), 
    or maybe somebody else is trying to extend your set of widgets… 
</p>
<p>
    To make this process easier, you can actually turn your base class to 
    something more than just a base - something that can force it’s subclasses 
    to provide certain methods and/or properties. That is where the abstract 
    classes are really useful.
</p>
<p>
You can define an abstract class using the <code>abstract</code> keyword like so:
</p> 
<p>
<pre class="prettyprint lang-js">
<code>vjs("<b>abstract class</b> Base", {
    "<b>abstract</b> getType" : function() {}
});</code></pre>
</p>
<p>
There are a few important concepts to understand here:
</p>
    <ol>
        <li>
            The abstract classes are something like an incomplete templates. 
            They are meant to be extended, thus they cannot have instances. 
            Calling <code class="prettyprint lang-js">new Base()</code> after 
            the above code would throw an error like 
            <code>Cannot create an instance of the abstract class "Base"</code>.
        </li>
        <li>
            When some property or method is declared as “abstract”, the 
            subclasses are forced to redefine it (an error is thrown if 
            they don't).
        </li>
        <li>
        The abstract properties and methods must be redefined in the
            subclasses, thus their <b>original value doesn't really matter</b>.
            The methods values must be functions but those functions does
            not need to have a body, so an empty function will do just fine.
            For the properties you can use everything, but I find null and
            undefined to be the least confusing choices.
        </li>
        <li>
            Not all of the methods/properties of the abstract class need to
            be abstract. Usually only some of them are and the abstract
            classes are both classical base classes that provide common
            functionality for their children and “templates” that force their
            children to implement something. The other two cases are still
            acceptable though:
            <ul>
                <li>
                    If all the properties and methods of abstract class are 
                    defined as abstract, then that class “provides nothing” 
                    and “expects everything”. This makes it behave more like 
                    an interface.
                </li>
                <li>
                    If none the properties and methods of abstract class are 
                    defined as abstract, then it&apos;s like a normal base class 
                    with the only exception that we are still forced to extend 
                    it and we cannot create a direct instances.
                </li>
            </ul>
        </li>
        <li>
            If at least one property or method of a class is defined as abstract, 
            then <b>the class itself must also be defined as abstract</b>. 
            Otherwise an exception is thrown.
        </li>
        <li>
            The abstract properties and methods <b>cannot be final or private</b> 
            as that would prevent the subclasses from overriding them.
        </li>
    </ol>
</div>
