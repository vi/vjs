<section>
<h1>Introduction</h1>
<p>
    VJS is a small JavaScript library for object-oriented programming. 
    It adds a thin layer of utilities and syntax sugar to your scripts.
</p>
<p>
    Doing an object oriented programming using dynamic language like JavaScript 
    can be very difficult task. There are many difficulties due to the fact that 
    the classical OOP relies on the compile-time transformations and on strong 
    types. JavaScript however, is fully dynamic and loosely typed language, 
    which means we cannot expect to achieve the typical way of using the OOP as 
    we know it from other static languages, but we can at least get close to it.
</p>
<p>
    VJS has a few major goals:
</p>
<ol>
    <li>To be easy to use and learn.</li>
    <li>To work as fast as possible…</li>
    <li>Not to require additional compilation</li>
    <li>To be smart and helpful for the programmer, by providing a rich set of tests  and verbose error messages.</li>
</ol>
<p>Example:</p>
<pre class="prettyprint lang-js"><code>var Event = vjs("<b>class</b> Event", {

    /**
     * The event type is public so anyone can read it 
     * but you non't have to worry. It is a "const" 
     * (read only) property, so once initialized by 
     * the constructor, no one can modify it.
     */
    "<b>const</b> type" : undefined,

    /**
     * Private flag used internally to keep track of 
     * the "PropagationStopped" state of the event.
     */
    "<b>private</b> _isPropagationStopped" : false,

    /**
     * That is how the outer world can check if the 
     * event propagation has been stopped
     */
    "<b>public</b> isPropagationStopped" : function() {
        return this._isPropagationStopped;
    },

    /**
     * Just sets the private "_isPropagationStopped" flag 
     * to true.
     */
    "<b>public</b> stopPropagation" : function() {
        this._isPropagationStopped= true;
    },

    /**
     * The constructor just sets the event type. Note that 
     * once the type property is set (to other than undefined), 
     * it gets locked and nothing can change it and introduce 
     * weird bugs that are very difficult to find...
     */
    "init" : function(type) {
        this.type = type;
    }
});

// USAGE:
// ---------------------------------------------------------------
var myEvent = new Event("customevent");
alert( myEvent.type ); // alerts "customevent"

// throws an exception (cannot change const)
myEvent.type = "anotherevent"; 

myEvent.isPropagationStopped(); // -> false

// throws an exception (cannot access private property)
myEvent._isPropagationStopped = true; 
myEvent.stopPropagation();
myEvent.isPropagationStopped(); // -> true
</code></pre>
<br/>

<p>Table of contents:</p>
<ul>
    <li><a href="#">Property modifiers</a>
        <ul>
            <li><a href="#">static properties and methods</a></li>
            <li><a href="#">const properties and methods</a></li>
            <li><a href="#">final properties and methods</a></li>
        </ul>
    </li>
    <li><a href="#">Syntax requirements</a></li>
</ul>

<h1>Property modifiers</h1>
<p>
    When you declare your classes you can use the typical JSON-like structure - 
    an object literal that defines all the properties and methods using 
    key:value pairs. The name however, can be used to define more than just
    the property name using the format <code>[modifiers ]name</code>. 
    It will be split by white space into multiple tokens. The last token must 
    be the property name and the order of the rest of the tokens doesn't matter. 
    Here are some examples:
</p>

<h3><b>static</b> properties or methods</h3>
<pre class="prettyprint lang-js"><code>vjs("<b>class</b> A", {
    "<b>static</b> a" : 1
});</code></pre>

<p>
    The static properties and methods are attached to the class instead of on 
    the instance. In JavaScript that means that they are attached to the 
    constructor function of the class. For instance:
</p>

<pre class="prettyprint lang-js"><code>var MyClass = vjs("<b>class</b> MyClass", { "<b>static</b> a" : 1 });
var MyInstance = new MyClass();
MyClass.a; // 1
MyInstance.a; // undefined
MyInstance.constructor.a; // 1 because MyInstance.constructor === MyClass
</code></pre>

<h3><b>const</b> properties or methods</h3>
<pre class="prettyprint lang-js"><code>vjs("class A", {
    "<b>const</b> a" : 1
});</code></pre>


<p>
    The static properties and methods are basically read-only. This means that 
    once defined, they cannot be changed. They can be very useful when you need 
    something that should be public, but you also want to make sure that it 
    cannot be changed. There are a few interesting details to consider.
</p>
<ul>
    <li>
        In some languages there are some limitations to what you can assign 
        when defining those constants. That is often a limitation rather than 
        necessary restriction. For example the property might be defined before 
        the code is compiled and you cannot assign objects there because they 
        are only available at runtime. In VJS however, you can assign everything. 
        The const properties can be attached to the instance or the class 
        (can be static or not). Perhaps the best way to understand them is to 
        consider them read-only (which is what they actually are).
    </li>
    <li>
        It can be very useful if such a property can be declared as "const" 
        but initialized later, when we have enough data to decide what it's 
        value should be. This can be done if you declare the property as 
        "undefined" and later assign something else to it. Once it becomes 
        something other than undefined it gets "locked".
    </li>
</ul>

<h3><b>final</b> properties or methods</h3>
<pre class="prettyprint lang-js"><code>vjs("<b>class</b> A", {
    "<b>final</b> a" : 1
});</code>
</pre>

 
<p>
    The final properties and methods are those that cannot be overridden. 
    For example the following code should throw an exception:
</p>
<pre class="prettyprint lang-js"><code>var baseClass = vjs("<b>class</b> baseClass", { "<b>final</b> a" : 1 });
var subClass = vjs("<b>class</b> SubClass <b>extends</b> baseClass", { "a" : 2 });
</code></pre>


<h3><b>mixed</b> property or method declarations</h3>
<p>Off course all of the above can be mixed as you see fit. These are all valid examples:</p>
<pre class="prettyprint lang-js"><code>var C = vjs("<b>class</b> C", {
    "a" : 1,
    "<b>public</b> b" : 2,
    "<b>public static</b> c" : 3
    "<b>protected static final</b> d" : 4,
    "<b>private final const</b> e" : 5,
    // ...
});</code></pre> 


<h2>Syntax Requirements</h2>
<p>There are a few syntax requirements that you need to be aware of.</p>
<ul>
<li>The <strong>name must be the last token</strong> in the declaration (if there is only name, then it is the last anyways)</li>
<li>The <strong>name cannot contain white space</strong> because that is what is used to distinguish the tokens. Other than that, you are not actually limited to the valid variable name syntax for the name.</li>
<li>The visibility can be defined using the "public", "private" and "protected" keywords. If nothing is specified it defaults to "public". However, if you use multiple of those visibility keywords you will not get an exception. Instead VJS will read them from right to left, so <strong>the first one will be applied</strong> and the others will be ignored. For example <code>"public private a" -&gt; becomes "public"</code> and <code>"private public a" -&gt; becomes "private"</code>.</li>
<li><strong>All the tokens except for the name are case insensitive</strong>, so <code>"private a"</code> is the same as <code>"Private a"</code> is the same as <code>"PRIVATE a"</code>.</li>
<li>The name cannot be one of the reserved words "public", "private", "protected", "static", "final", and "const".</li>
</ul>
    </section>