/**! 
 * ers 
 * A central system for EEG data storage, management, and analysis under the auspices of USF  
 * 0.0.0 
 * build date: 2013-12-20 
 */
;window.ERS||(ERS = {});
(function($, NS, undefined) {
    
    var CONFIG = {
        dateFormat : "yyyy-MMM-dd"
    };

    /**
     * Returns the float representation of the first argument or the 
     * "defaultValue" if the float conversion is not possible.
     * @param {*} x The argument to convert
     * @param {*} defaultValue The fall-back return value. This is going to be 
     *                         converted to float too.
     * @return {Number} The resulting floating point number.
     */
    function floatVal( x, defaultValue ) {
        var out = parseFloat(x);
        if ( isNaN(out) || !isFinite(out) ) {
            out = defaultValue === undefined ? 0 : floatVal( defaultValue );
        }
        return out;
    }

    /**
     * Returns the int representation of the first argument or the 
     * "defaultValue" if the int conversion is not possible.
     * @param {*} x The argument to convert
     * @param {*} defaultValue The fall-back return value. This is going to be 
     *                         converted to integer too.
     * @return {Number} The resulting integer.
     */
    function intVal( x, defaultValue ) {
        var out = parseInt(x, 10);
        if ( isNaN(out) || !isFinite(out) ) {
            out = defaultValue === undefined ? 0 : intVal( defaultValue );
        }
        return out;
    }

    /**
     * Rounds the given number to configurable precision.
     * @param {numeric} n The argument to round.
     * @param {Number} precision The precision (number of digits after the 
     *                           decimal point) to use.
     * @return {Number} The resulting number.
     */
    function roundToPrecision(n, precision) {
        n = parseFloat(n);
        if ( isNaN(n) || !isFinite(n) ) {
            return NaN;
        }
        if ( !precision || isNaN(precision) || !isFinite(precision) || precision < 1 ) {
            return Math.round( n );
        }
        var q = Math.pow(10, precision);
        return Math.round( n * q ) / q;
    }

    function formatWeight(wKg, options) {
        var cfg = $.extend({
            system   : "metric", // metric|imperial
            precision: 2,
            split    : " "
        });
        
        var w = floatVal(wKg),
            u = cfg.system == "imperial" ? "lb" : "kg";
        
        if (cfg.system == "imperial") {
            w /= 0.453592;
        }
        
        w = roundToPrecision(w, cfg.precision);
        return w + cfg.split + u;
    }

    /**
     * Set (create or update) a cookie.
     * @param {String} name The name of the cookie
     * @param {*} value The value of the cookie
     * @param {Number} days (optional) The cookie lifetime in days. If omitted the 
     *                                 cookie is a session cookie.
     * @return {void}
     */
    function setCookie( name, value, days ) {
        if ( String(name).indexOf(";") > -1 ) {
            throw "The cookie name cannot contain ';'";
        }
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    /**
     * Reads a cookie identified by it's name.
     * @param {String} name The name of the cookie
     * @return {String|null} The value of the cookie or null on failure
     */
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') 
                c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) 
                return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    /**
     * Erases a cookie by setting it's expiration date in the past.
     * @param {String} name The name of the cookie
     * @return {void}
     */
    function deleteCookie(name) {
        setCookie(name, "", -1);
    }

    NS.Utils = {
        intVal           : intVal,
        floatVal         : floatVal,
        roundToPrecision : roundToPrecision,
        formatWeight     : formatWeight,
        setCookie        : setCookie,
        getCookie        : getCookie,
        deleteCookie     : deleteCookie
    };
    


    var Site = new (Backbone.Router.extend({

        routes: {
            "(*path)" : "loadPage"
        },
        
        loadPage : function(path) {
            openPage({ 
                path : path,
                title: "Register at ERS"
            });
        }

    }))();
    ////////////////////////////////////////////////////////////////////////////////

    function openPage(cfg) {
        if ( !cfg.path ) {
            return;
        }
        
        var pageContainer = $(".page-container");
        var pageDiv = pageContainer.find('[data-page="' + cfg.path + '"]');
        
        if ( !pageDiv.length ) {
            pageDiv = $('<div data-page="' + cfg.path + '"/>').appendTo(pageContainer);
        }
        
        pageContainer.find('[data-page].active').removeClass("active");
        
        $("html").addClass("loading-page");
        
        clearPageloadErrors();
        
        $.get("pages/" + cfg.path.replace(/(\.html)?$/, ".html"))
        .done(function( data, textStatus, jqXHR ) {
            pageDiv.addClass("active").data("page-options", cfg).html(jqXHR.responseText);
            prettyPrint();
            document.title = cfg.title || "EEG Research System";
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            showPageloadError(errorThrown || textStatus);
        })
        .always(function() {
            $("html").removeClass("loading-page");
        });
    }

    function showPageloadError(message) {
        $('<div class="alert alert-danger alert-dismissable">' + 
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + 
            '<strong>Error!</strong> ' + (message || "") + 
        '</div>').appendTo(".errors");
    }

    function clearPageloadErrors() {
        $(".errors").empty();
    }

    Backbone.history.on("route", function(router, route, params) {
        var hash = String(location.hash).replace(/^#/, "") || "";
        $(".side-nav li > a").each(function() {
            $(this).parent().toggleClass("active", this.getAttribute("href") == "#" + hash);
        });
    });

    Backbone.history.start({ 
        pushState : false,
        silent    : false,
        hashChange: true,
        root      : "/"
    });

    $(function() {
        prettyPrint();
    });
    
})(jQuery, ERS);
