(function(NS, undefined) {
var reservedWords = {
    "public"    : 1,
    "private"   : 1,
    "protected" : 1,
    "const"     : 1,
    "static"    : 1,
    "final"     : 1,
    "abstract"  : 1
};

var _classes = {};

var vjs = function(declaration, properties) {
    return new _class(declaration, properties).constructor;
};

vjs.getClass = function( name ) 
{
    return _classes[ name ];
};

vjs.createInstance = function( name ) 
{
    var _class = _classes[ name ];
    if (!_class) {
        throw "No such class '" + name + "'";
    }
    return _class.constructor.apply({}, Array.prototype.slice.call(arguments, 1));
};

function trim(str) {
    return str.trim ? str.trim() : str.replace(/^\s*|\s*$/, "");
}

/**
 * Creates a property object from the given declaration and value
 * @param {String} declaration The property declaration
 * @param {*} value The property value
 * @return {Object} The property object
 * @throws {Error} Upon validation failure
 */
function property( declaration, value ) 
{
    var arr = String(declaration || "").replace(/^\s*|\s*$/, "").split(/\s+/),
        out = {
            "access"   : "public",
            "const"    : false,
            "static"   : false,
            "final"    : false,
            "abstract" : false,
            "value"    : value
        },
        len = arr.length,
        decl = [],
        mod, modLower;
    
    if (!len) {
        throw new Error("Invalid (empty) property declaration.");
    }
    
    out.name = arr[--len];
    if (out.name in reservedWords) {
        throw new Error("Invalid property name '" + out.name + "'.");
    }
    
    while (len) {
        mod = arr[--len];
        modLower = mod.toLowerCase();
        switch (modLower) {
            case "public":
            case "private":
            case "protected":
                out.access = modLower;
            break;
            case "const":
            case "static":
            case "final":
            case "abstract":
                out[modLower] = true;
            break;
            default:
                throw new Error("Invalid property declaration. Unrecognised token '" + mod + "'.");
        }
    }
    
    if (!out.name) {
        throw new Error("Invalid property declaration. Missing name.")
    }
    
    // make sure that the properties have their declaration in common format so 
    // that they can be compared between each other. Although the modifiers 
    // order doesn't matter, the declaration property will always be in the 
    // format: "[abstract] [final] [const] access [static] name"
    if (out["abstract"]) {
        decl.push("abstract");
    }
    if (out["final"]) {
        decl.push("final");
    }
    if (out["const"]) {
        decl.push("const");
    }
    decl.push(out.access);
    if (out["static"]) {
        decl.push("static");
    }
    decl.push(out.name);
    out.declaration = decl.join(" ");
    
    return out;
}

function createProperty(obj, prop, state, _super) 
{
    /**
     * Creates a method of the object "obj". The purpose of that is to make 
     * every method entering the private scope at the beginning and leaving 
     * it at the end.
     * @param {Function} fn The actual function to be bound
     * @return {Function} The created method
     */
    function createMethod(fn, name) 
    {
        return function() {
            state._inPrivateScope++;
            var tmp = obj._super;
            obj._super = _super && _super[name] ? _super[name].value : function() {};
            var out = fn.apply(obj, arguments);
            obj._super = tmp;
            state._inPrivateScope--;
            return out;
        };
    }
    
    // Set the initial value of the property
    state.props[prop.name] = typeof prop.value == "function" ? 
        createMethod(prop.value, prop.name) : 
        prop.value;
    
    // There is no need for getters and setters for the simple properties
    if (prop.access == "public" && prop.const == false) {
        obj[prop.name] = state.props[prop.name];
        return;
    }
    
    // Use Object.defineProperty to do the access magic...
    Object.defineProperty(
        obj, 
        prop.name, 
        {
            /**
             * The getter for the property. Will deny access for 
             * non-public properties and methods from the outer scope.
             * @return {*} The property value if accessible.
             * @throws {Error} If the property is not accessible.
             */
            get : function() { 
                if ( prop.access != "public" && !state._inPrivateScope ) {
                    throw new Error(
                        "Cannot access the property '" + 
                        prop.name + 
                        "' from out of scope"
                    );
                }
                return state.props[prop.name];
            },
            
            /**
             * The setter for the property. Will deny access for 
             * non-public properties and methods from the outer scope. 
             * It will also deny the change for "const" properties and 
             * methods, unless they are undefined.
             * @return void
             * @throws {Error} If the property is not writeable.
             */
            set : function( value ) {
                if ( prop.access != "public" && !state._inPrivateScope ) {
                    throw new Error(
                        "Cannot access the property '" + 
                        prop.name + 
                        "' from out of scope"
                    );
                }
                if ( prop["const"] && prop.value !== undefined ) {
                    throw new Error(
                        "Cannot change the value of the constant '" + 
                        prop.name + 
                        "' property"
                    );
                }
                state.props[prop.name] = typeof value == "function" ? 
                    createMethod(value) : 
                    value;
            },
            
            // Non-public members are NOT enumerable
            enumerable : prop.access == "public",
            
            // Non-constant members are configurable 
            // (so that they can be deleted)
            configurable : !prop["const"]
        }
    );
}

function _class(declaration, properties) 
{
    /**
     * The original (unparsed) declaration string.
     */
    this.declaration = declaration;
    
    /**
     * The identifier (class name) pointing to the superclass that 
     * this class should inherit from.
     */
    this["extends"] = null;
    
    /**
     * An array of zero or more interface identifiers pointing to the 
     * interface(s) that this class should implement.
     */
    this["implements"] = [];
    
    /**
     * A map of name:property that will contain all the property objects 
     * as returned from the property() function.
     */
    this.instanceProperties = {};
    
    /**
     * A map of name:property that will contain all the property objects 
     * as returned from the property() function. Same as the instanceProperties 
     * above, but for static properties and methods
     */
    this.classProperties = {};
    
    ////////////////////////////////////////////////////////////////////////////
    var _thisClass = this,
        _staticState = { props : {}, _inPrivateScope : 0 },
        _compiledInstanceProps = {},
        _compiledStaticProps = {},
        _super,
        _prop,
        _x;
    
    this.parseClassDeclaration(declaration);
    
    if ( this["extends"] ) {
        _super = _classes[this["extends"]];
        if (!_super) {
            throw "Cannot inherit from undefined class '" + this["extends"] + "'";
        }
    }
    
    this.constructor = function Class() {
        
        if ( _thisClass.isAbstract ) {
            throw new Error(
                'Cannot create an instance of the abstract class "' + 
                _thisClass.name + '"'
            );
        }
        
        var _state = { props : {}, _inPrivateScope : 0 }, name;
        for ( name in _thisClass.instanceProperties ) {
            createProperty( 
                this, 
                _thisClass.instanceProperties[ name ], 
                _state,
                _super ? _super.instanceProperties : null
            );
        }
        
        if ( typeof this.init == "function" ) {
            //_state._inPrivateScope = true;
            
            this.init.apply( this, arguments );
            //_state._inPrivateScope = false;
        }
    }
    
    
    
    // Compile properties ======================================================
    if ( properties ) { // the second argument is optional
        
        // the second argument can be function returning the properties
        if ( typeof properties == "function" ) {
            properties = properties();
        }
        
        if ( !properties || typeof properties !== "object" ) {
            throw new Error( "Invalid class properties declaration" );
        }
        
        for ( _x in properties ) {
            _prop = property(_x, properties[_x]);
            
            // Check for abstract methods
            if ( _prop["abstract"] ) {
                
                if ( _prop.access == "private" ) {
                    throw new Error(
                        'The abstract property or method "' +
                        _prop.name + 
                        '" cannot be private'
                    );
                }
                
                if ( _prop["final"] ) {
                    throw new Error(
                        'The abstract property or method "' +
                        _prop.name + 
                        '" cannot be final'
                    );
                }
                
                if ( !_thisClass.isAbstract ) {
                    throw new Error(
                        'The property "' + _prop.name + '" is abstract, therefore ' +
                        'the class "' + _thisClass.name + '" must also be defined ' +
                        'abstract'
                    );
                }
            }
            
            if ( _prop["static"] ) {
                _compiledStaticProps[_prop.name] = _prop;
            } else {
                _compiledInstanceProps[_prop.name] = _prop;
            }
        }
    }
    
    // Inheritance =============================================================
    if ( _super ) {
        
        // Keep the prototype chain
        _thisClass.constructor.prototype = Object.create(_super.constructor.prototype);
        _thisClass.constructor.prototype.constructor = _thisClass.constructor;
        
        // inherit instance properties
        for ( _x in _super.instanceProperties ) {
            _prop = _super.instanceProperties[_x];
            
            if ( _prop.name in _compiledInstanceProps ) {
                
                // Don't override finals
                if ( _prop["final"] ) {
                    throw new Error(
                        "The final property or method '" + 
                        _prop.name + 
                        "' cannot be overridden"
                    );
                }
            } else {
                if ( _prop["abstract"] ) {
                    throw new Error(
                        'The class "' + _thisClass.name + '" must implement ' + 
                        'the "' + _prop.name + '" property or method'
                    );
                }
            }
            
            if ( _prop.access != "private" ) {
                this.instanceProperties[_prop.name] = _prop;
            }
        }
        
        // inherit static properties
        for ( _x in _super.classProperties ) {
            _prop = _super.classProperties[_x];
            
            if ( _prop["final"] && _prop.name in _compiledStaticProps ) {
                throw new Error(
                    "The final property or method '" + 
                    _prop.name + 
                    "' cannot be overridden"
                );
            }
            
            if ( _prop.access != "private" ) {
                this.classProperties[_prop.name] = _prop;
                createProperty(this.constructor, _prop, _staticState);
            }
        }
    }
    
    // Instance properties =====================================================
    for ( _x in _compiledInstanceProps ) {
        _prop = _compiledInstanceProps[_x];
        this.instanceProperties[_prop.name] = _prop;
    }
    
    // Static properties =======================================================
    for ( _x in _compiledStaticProps ) {
        _prop = _compiledStaticProps[_x];
        this.classProperties[_prop.name] = _prop;
        createProperty(this.constructor, _prop, _staticState);
    }
}

_class.prototype = {
    name       : "",
    isAbstract : false,
    isFinal    : false,
    constructor: null,
    
    parseClassDeclaration : function(declaration) {
        var tokens     = trim(String(declaration || "")).split(/\bclass\b/);
        var mods       = trim(tokens[0]).split(/\s+/);
        var right      = trim(tokens[1]);
        var name       = right.replace(/(\s.*|$)/, "");
        var ext        = right.match(/\bextends\s+(.+?)(?:\s+implements|$)/);
        var impl       = right.match(/\bimplements\s+(.+?)(?:\s+extends|$)/);
        var modsLength = mods.length, mod;
        
        while (modsLength) {
            mod = mods[--modsLength];
            if (!mod) { // can be ""
                continue;
            }
            switch (mod) {
                case "abstract":
                    this.isAbstract = true;
                break;
                case "final":
                    this.isFinal = true;
                break;
                default:
                    throw new Error("Unrecognised class modifier '" + mod + "'");
            }
        }
        
        if (ext) {
            this["extends"] = trim(ext[1]);
        }
        
        if (impl) {
            this["implements"] = impl[1].split(/\s*,\s*/);
        }
        
        this.name = name;
        
        _classes[name] = this;
    }
};

Class = function(classOptions, props) {
    return new _class(classOptions, props).constructor;
};

NS.property = property;
NS._class = _class;
NS.vjs = vjs;

})(this);