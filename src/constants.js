var reSpaces       = /\s+/,
    reClass        = /\bclass\b/,
    reExtends      = /\bextends\s+(.+?)(?:\s+implements|$)/,
    reImplements   = /\bimplements\s+(.+?)(?:\s+extends|$)/,
    reWsOrEof      = /(\s.*|$)/,
    reCommaSplit   = /\s*,\s*/,
    reSpaceTrim    = /^\s*|\s*$/,
    reJsPath       = /\[['"]?([^\]]+)['"]?\]/g,
    rePrintfTokens = /%(\.(\d))?([a-zA-Z%])/g,
    defineProperty = Object.defineProperty,
    toString       = Object.prototype.toString,
    reservedWords  = {
        "public"    : 1,
        "private"   : 1,
        "protected" : 1,
        "const"     : 1,
        "static"    : 1,
        "final"     : 1,
        "abstract"  : 1
    },
    errors = {
        // General message for unknown error
        0   : "Unknown VJS error",

        // 100-199 - Parse errors
        100 : "Invalid property declaration.",
        101 : "Invalid (empty) property declaration.",
        102 : "Invalid property name '%s' (reserved word).",
        103 : "Invalid property declaration. Unrecognised token '%s'.",
        104 : "Invalid property declaration. Missing name.",
        105 : "Unrecognised class modifier '%s'",
        106 : "Invalid class properties declaration",
        107 : "Invalid class declaration '%s'. Missing class name or path.",

        // 200-299 Runtime errors
        201 : "Cannot access the property '%s' from out of scope",
        202 : "No such class '%s'.",
        203 : "Cannot change the value of the constant '%s' property of class '%s'.",
        204 : "Cannot inherit from undefined class '%s'.",
        205 : "Cannot inherit from class '%s'",
        206 : "Cannot inherit from final class '%s'",
        207 : "Cannot create an instance of the abstract class '%s'.",
        208 : "The abstract property or method '%s' cannot be private",
        209 : "The abstract property or method '%s' cannot be final",
        210 : "The property '%s' is abstract, therefore the class '%s' must " +
              "also be defined as abstract",
        211 : "The final property or method '%s' cannot be overridden",
        212 : "The class '%s' must implement the '%s' property or method"

    };
    
