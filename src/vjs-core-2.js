// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

/**
 * The internal storage for classes
 * @type Object
 * @private
 */
var _classes = {};

/**
 * The vjs function - the main object that gets exported.
 * @param {String} declaration
 * @param {Object|Function} properties
 * @return {Function} The constructor of the created class
 */
function vjs(declaration, properties) {
    return (new _class(declaration, properties)).Constructor;
}

/**
 * The globally available static configuration object. Can be modified to
 * change the vjs behavior...
 * @type Object
 */
vjs.config = {
    namespace : NS
};

/**
 * This can be used to obtain a reference to a class object by name (normally,
 * the clients will only work with class constructors and are not going to
 * need this)
 * @param {String} name The name of the needed class
 */
vjs.getClass = function( name ) {
    return _classes[ name ];
};

vjs.getClassOf = function( inst ) {
    for ( var c in _classes ) {
        if ( _classes[c].Constructor === inst.constructor ) {
            return _classes[c];
        }
    }
};

/**
 * Utility function that can create new instances of the given class name.
 * The first argument is required and must be the class name and any additional
 * arguments will be passed to the class constructor.
 * @param {String} name The class name
 */
vjs.createInstance = function( name ) {
    var _class = _classes[ name ], inst;
    if (!_class) {
        throw new VJSError(202, name);
    }
    inst = Object.create(_class.Constructor.prototype);
    _class.Constructor.apply(inst, Array.prototype.slice.call(arguments, 1));
    return inst;
};

/**
 * Parses the class declaration string @declaration and sets some properties on
 * the instance @inst. This might be a typical task for some instance method,
 * but it is moved here (to local private function) to make it a little bit
 * faster and because such things should remain private and invisible to the
 * outer scope.
 * @param {String} declaration The declaration string
 * @param {_class} inst The instance to augment
 * @throws VJSError If the declaration is empty or invalid
 * @return {void}
 */
function parseClassDeclaration(declaration, inst) {
    var tokens     = trim(declaration || "").split(reClass),
        mods       = trim(tokens[0]).split(reSpaces),
        right      = tokens.length > 1 ? trim(tokens[1]) : "",
        path       = right.replace(reWsOrEof, ""),
        ext        = right.match(reExtends),
        impl       = right.match(reImplements),
        modsLength = mods.length,
        name, mod;

    if (!path) {
        throw new VJSError(107, declaration);
    }

    while (modsLength) {
        mod = mods[--modsLength];
        switch (mod) {
            case "":
                break;
            case "abstract":
                inst.isAbstract = true;
                break;
            case "final":
                inst.isFinal = true;
                break;
            default:
                throw new VJSError(105, mod);
        }
    }

    /**
     * The identifier (class path) pointing to the superclass that
     * this class should inherit from.
     */
    inst["extends"] = ext ? ext[1] : null;

    /**
     * An array of zero or more interface identifiers pointing to the
     * interface(s) that this class should implement.
     */
    inst["implements"] = impl ? impl[1].split(reCommaSplit) : [];

    inst.path = path;

    inst.name = path.replace(reJsPath, ".$1").split(".").pop();

    _classes[path] = inst;
}

/**
 *
 */
function InstanceState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.instanceProperties : {};
}

/**
 *
 */
function StaticState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.classProperties : {};
}

/**
 * Lookup the class at @path and verify that it can be used as base class
 * @param {String} path The class name or path
 * @throws VJSError
 * @return {_class} The base class
 */
function findSuper(path) {
    var _super = _classes[path];

    // Call the custom class-loader function if defined
    if (!_super && is(vjs.config.loadClass, "function") ) {
        _super = vjs.config.loadClass(path);
    }

    // Cannot inherit from undefined class
    // TODO: Better error message (like "failed to evaluate class path")
    if (!_super) {
        throw new VJSError(204, path);
    }

    // Cannot inherit from non-class
    if (!(_super instanceof _class)) {
        throw new VJSError(205, path);
    }

    // Cannot inherit from final class
    if (_super.isFinal) {
        throw new VJSError(206, path);
    }

    return _super;
}

function createInstance(Class, instance, args) {
    var privateState = {
        props : {},
        _inPrivateScope : 0
    }, prop, inSuperChain;
    for (var x in Class.instanceProperties) {
        prop = Class.instanceProperties[x];
        createProperty(prop, Class, instance, privateState);
    }

    defineProperty(instance, "__super__", {
        get : function() {
            var name = privateState._inPrivateScope, prt, out;
            if (!name) {
                throw new VJSError(201, "__super__");
            }

            prt = Class.superClass ? 
                Class.superClass.instanceProperties :
                Object.prototype;

            out = prt[name];

            return prt.hasOwnProperty(name) ? 
                typeof out.value == "function" ? 
                    inSuperChain ?
                        noop :
                        (function() {
                            inSuperChain = true;
                            var result = out.value.apply(this, arguments);
                            inSuperChain = false;
                            return result;
                        }).bind(instance) :
                    out.value : 
                undefined;
        }
    });

    if ( typeof instance.init == "function" ) {
        instance.init.apply( instance, args );
    }
    return instance;
}

function createClassConstructor(Class) {
    if ( Class.isAbstract ) {
        return Function(
            "return function " + Class.name + "() {\n" +
            "   throw new VJSError(207, '" + Class.name + "');\n" + 
            "};"
        )();
    }
        
    return Function(
        "Class, createInstance", 
        "return function " + Class.name + "() {\n" +
        "   createInstance(Class, this, arguments);\n" + 
        "};"
    )(Class, createInstance);
}

/**
 *
 */
function _class(declaration, properties)
{
    var prop, x, parentProp, thisClass = this;

    /**
     * The original (unparsed) declaration string.
     */
    this.declaration = declaration;

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function.
     */
    this.instanceProperties = {};

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function. Same as the instanceProperties
     * above, but for static properties and methods
     */
    this.classProperties = {};

    /**
     * The super class defaults to Object (if nothing else is specified in the
     * "extends" keyword).
     * @type Function
     */
    this.superClass = null;

    this.privateStaticState = {
        props : {},
        _inPrivateScope : 0
    };

    // Starts the class initialization
    // =========================================================================

    // This will set some of the instance properties like "name", "path",
    // "final", "abstract", "extends" and ""implements"."
    // -------------------------------------------------------------------------
    parseClassDeclaration(declaration, this);

    // If it is specified that this class should inherit from another, try to
    // load it and check if such inheritance is possible and valid
    // -------------------------------------------------------------------------
    if ( this["extends"] ) {
        this.superClass = findSuper(this["extends"]);
    }

    // Create the constructor
    // Trying to create an instance of abstract class must fail
    // -------------------------------------------------------------------------
    this.Constructor = createClassConstructor(this);
    

    // Inheritance
    // -------------------------------------------------------------------------
    this.Constructor.prototype = Object.create(
        this.superClass ? 
            this.superClass.Constructor.prototype : 
            Object.prototype
        );

    // Loop super props. Note that the for/in loop will skip the private 
    // properties because they ate not enumerable.
    if (this.superClass) {
        for ( x in this.superClass.instanceProperties ) {
            parentProp = this.superClass.instanceProperties[x];
            if (parentProp && parentProp.access != "private") {
                this.instanceProperties[x] = parentProp;
        //        defineProperty(this.constructor.prototype, x, {
        //            get : function() {
        //                throw new VJSError(201, x);
        //            },
        //            set : function( value ) {
        //                throw new VJSError(201, x);
        //            }
        //        });
            }
        //    this.Constructor.prototype[x] = this.superClass.Constructor.prototype[x];
        }
        //this.Constructor.prototype.__super__ = this.superClass.Constructor.prototype;


        //console.log("TEST: ", this.superClass.constructor.prototype.isPrototypeOf(this.constructor.prototype))
    }

    this.Constructor.prototype.constructor = this.Constructor;
    this.Constructor.__class__ = this;

    // Compile properties
    // (the second argument for "_class" is optional)
    // -------------------------------------------------------------------------
    if ( !properties ) {
        properties = {};
    }
    
    // "properties" can be a function
    if ( typeof properties == "function" ) {
        properties = properties(
            this.superClass ? this.superClass.Constructor.prototype : {},
            this.Constructor
        );
    }

    // Invalid class properties declaration
    if ( !properties || typeof properties !== "object" ) {
        throw new VJSError(106);
    }

    // Attach the properties to the proptptype (or to the constructor if they 
    // are static).
    // -------------------------------------------------------------------------
    for ( x in properties ) {
        prop = property(x, properties[x]);
        if ( prop["static"] ) {
            createProperty(prop, this, this.Constructor, this.privateStaticState);
        } else {
            parentProp = this.superClass ? 
                this.superClass.instanceProperties[prop.name] :
                null;
            
            // Don't override finals
            if (parentProp && parentProp["final"] ) {
                throw new VJSError(211, prop.name);
            }

            // Check for abstract methods (Must use  abstract class)
            if ( prop["abstract"] && !this.isAbstract ) {
                throw new VJSError(210, prop.name, this.name);
            }

            this.instanceProperties[prop.name] = prop;
        }
    }

    // If this class inherits from an abstract one make sure that the abstract
    // methods were implemented
    // -------------------------------------------------------------------------
    if ( this.superClass ) {
        for ( x in this.superClass.instanceProperties ) {
            parentProp = this.superClass.instanceProperties[x];
            prop = this.instanceProperties[parentProp.name];
            if (parentProp["abstract"] && prop === parentProp) {
                throw new VJSError(212, this.name, parentProp.name);
            }
        }
    }

    // Write the constructor to vjs.config.namespace if needed
    // -------------------------------------------------------------------------
    if (vjs.config.namespace) {
        jPath(vjs.config.namespace, this.path, this.Constructor);
    }

    this.toString = function() {
        return this.declaration;
    };

    return this;
}

NS.vjs = vjs;
