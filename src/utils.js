// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

/**
 * Just an empty function for whatever we might need it.
 * @return void
 */
function noop() {}

/**
 * Trims the white space on the both sides of a string. Delegates
 * to the native trim if it exists.
 * @param {String} str The string to trim
 * @return String
 */
function trim(str) {
    return str.trim ?
        str.trim() :
        String(str).replace(reSpaceTrim, "");
}

/**
 * Checks if @x is of type @type. Typically it will use the
 * Object.ptptotype.toString approach so that we can work with objects from
 * different origins (frames). For custom objects wit will use the "instanceof"
 * operator and for numbers it should return false if the @x argument is NaN
 * or Infinity.
 * @param x The "thing" to check the type of
 * @param {String|Function} type Can be a case-insensitive string type or a
 * reference to the desired constructor function.
 * @returns {Boolean}
 */
function is(x, type) {

    var _type = String(type).toLowerCase(),
        xType = toString.call(x).toLowerCase();

    // Built-in objects (except for Number)
    if (_type === "object"   ||
        _type === "function" ||
        _type === "array"    ||
        _type === "string"   ||
        _type === "boolean"  ||
        _type === "date"     ||
        _type === "regexp")
    {
        return xType === "[object " + type + "]";
    }

    // Strict number (return false for NaN and Infinty)
    if (_type === "number") {
       return xType === "[object " + type + "]" && !isNaN(x) && isFinite(x);
    }

    // Custom objects - use instanceof
    if (xType === "[object object]" &&
        toString.call(type) === "[object Function]")
    {
        return (x instanceof type);
    }

    // Everything else - just use simple strict comparison
    return x === type;
}

/**
 * Navigates to the @path inside @obj and returns the value or updates it if
 * called with 3 arguments.
 * @param {Object} obj The object to use
 * @param {String} path
 * @param {*} value (optional)
 */
function jPath(obj, path, value) {
    var segments = path.replace(reJsPath, ".$1").split("."),
        l        = segments.length,
        curPath  = [],
        modeGET  = 2,
        modeSET  = 3,
        modeDEL  = 4,
        mode     = arguments.length === 3 ?
            value === undefined ?
                modeDEL :
                modeSET :
            modeGET,
        cur      = obj,
        isArray,
        name,
        next,
        i;

    for ( i = 0; i < l; i++ ) {
        curPath[i] = name = segments[i];
        isArray = is(cur, "array");

        if (isArray) {
            name = parseInt(name, 10);
        }

        if ( i === l - 1 ) { // last
            if ( mode === modeDEL ) {
                if ( isArray ) {
                    cur.splice(name, 1);
                }
                else if ( cur.hasOwnProperty(name) ) {
                    delete cur[name];
                }
            }
            else if ( mode === modeSET ) {
                cur[name] = value;
            }
            return cur[name];
        }

        if (!cur.hasOwnProperty(name)) {

            // Called to read, but an intermediate path segment was not
            // found - return undefined
            if ( mode === modeGET ) {
                return undefined;
            }

            // Called to write, but an intermediate path segment was not
            // found - create it and continue
            next = segments[ i + 1 ];
            cur[name] = isNaN( parseFloat(next) ) ||
                String(parseFloat(next)) !== String(next) ? {} : [];
        }

        cur = cur[name];
    }
}

/**
 * Creates a property object from the given declaration and value
 * @param {String} declaration The property declaration
 * @param {*} value The property value
 * @return {Object} The property object
 * @throws {Error} Upon validation failure
 */
function property( declaration, value ) {
    var arr = trim(String(declaration || "")).split(reSpaces),
        out = {
            "access"   : "public",
            "const"    : false,
            "static"   : false,
            "final"    : false,
            "abstract" : false,
            "value"    : value
        },
        len = arr.length,
        decl = [],
        name,
        mod,
        modLower;

    // Empty declaration
    if (!arr[0]) {
        throw new VJSError(101);
    }

    name = arr[--len];

    // missing name
    if (!name) {
        throw new VJSError(104);
    }

    // name is a reserved word
    if (name in reservedWords) {
        throw new VJSError(102, name);
    }

    out.name = name;

    while ( len ) {
        mod = arr[--len];
        modLower = mod.toLowerCase();
        switch (modLower) {
            case "public":
            case "private":
            case "protected":
                out.access = modLower;
            break;
            case "const":
            case "static":
            case "final":
            case "abstract":
                out[modLower] = true;
            break;
            default:
                throw new VJSError(103, mod);
        }
    }

    // Check for invalid combinations
    if ( out["abstract"] ) {

        // Can't have abstract + private
        if ( out.access == "private" ) {
            throw new VJSError(208, name);
        }

        // Can't have abstract + final
        if ( out["final"] ) {
            throw new VJSError(209, name);
        }
    }

    // make sure that the properties have their declaration in common format so
    // that they can be compared between each other. Although the modifiers
    // order doesn't matter, the declaration property will always be in the
    // format: "[abstract] [final] [const] access [static] name"
    if (out["abstract"]) {
        decl.push("abstract");
    }
    if (out["final"]) {
        decl.push("final");
    }
    if (out["const"]) {
        decl.push("const");
    }
    decl.push(out.access);
    if (out["static"]) {
        decl.push("static");
    }
    decl.push(out.name);
    out.declaration = decl.join(" ");

    return out;
}

/**
 * Creates and returns a wrapped method. The purpose of that is to make
 * every method entering the private scope at the beginning and leaving
 * it at the end. Variables like @state and @staticState are objects that
 * are provided here and then "remembered" as local variables from the
 * closure. The actual function @fn cannot access them but the returned
 * functions will...
 * @param {Function} fn The actual function to be bound
 * @return {Function} The created method
 * TODO remove the name arg and fix the description!
 */
function createMethod(scope, fn, name, state, staticState) {

    function proxy() {
        var old = state._inPrivateScope,
            out;

        state._inPrivateScope = name;
        if (staticState) staticState._inPrivateScope++;

        //scope.__super__ = (state.superState[name] || {}).value || noop;
        //if (typeof scope.__super__ == "function") {
        //    scope.__super__ = scope.__super__.bind(scope);
        //}
        var o = this instanceof proxy ? Object.create(fn.prototype) : this || scope;
        //old = o.__super__;
        //o.__super__ = state.__super__(name);
        out = fn.apply(o, arguments);
        //o.__super__ = old;

        if (staticState) staticState._inPrivateScope--;
        state._inPrivateScope = old;

        return this instanceof proxy ? o : out;
    }

    proxy.toString = function() {
        return fn.toString();
    };

    return proxy;
}

/**
 * Creates a propperty attached to the object @obj but also maintains a link
 * to the private local objects @scope and @staticScope where the actual
 * values are stored. Unles the property should be "public" it can be
 * considered as a virtual property, meaning that it might have getters and
 * setters on the @obj but the actual value is stored at @state or
 * @staticState (depending on if the property is static or not)
 * @param {Object} The object to augment
 * @param {Object} property The property object created with the property
 *  function
 * @param {Object} state The actual property-value storage object
 * @param {Object} staticState The storage for the static properties. This
 *  might be the same object as @state if the property is static. Otherwise
 *  it is different. It is not used directly by this function but it gets
 *  passed to the createMethod function so that instance methods can also
 *  "unlock" the private and protected static scope...
 * TODO staticState needs a better description or just some better solution
 */
function createProperty(prop, Class, obj, state) {

    var staticState = Class.privateStaticState;

    // Set the initial value of the property
    var _prop = state.props[prop.name] = typeof prop.value == "function" ?
        createMethod(obj, prop.value, prop.name, state, staticState) :
        prop.value;

    // There is no need for getters and setters for the simple properties
    //if ( !prop["const"] && prop.access == "public" ) {
    //    obj[prop.name] = _prop;
    //    return;
    //}

    function isAllowed(prop, that) {console.log(prop, that, Class, obj);
        if (prop.access == "public") {
            return true;
        }
        if (state._inPrivateScope) {
            return true;
        }
        /*if (prop["static"]) {
            return staticState._inPrivateScope || 
                Class.constructor.prototype.isPrototypeOf(that.prototype);
        } else {
            if (that instanceof Class.constructor)
                return true;    
            return Class.constructor.prototype.isPrototypeOf(that);
        }*/
        return true;
    }

    // Use Object.defineProperty to do the access magic...
    defineProperty(
        obj,
        prop.name,
        {
            /**
             * The getter for the property. Will deny access for
             * non-public properties and methods from the outer scope.
             * @return {*} The property value if accessible.
             * @throws {Error} If the property is not accessible.
             */
            get : function() {//console.log(Class, this === obj);
                if (state._inPrivateScope || prop.access == "public") {
                    return _prop;    
                }
                //if (typeof this == "function" && Class.constructor.prototype.isPrototypeOf(this.prototype)) {
                //    return prop.value;
                //}
                //if (Class.constructor.prototype.isPrototypeOf(this)) {
                //    var c = vjs.getClassOf(this);//console.log("getClassOf:", c)
                //    if (!c || !state._inPrivateScope)
                        throw new VJSError(201, prop.name);
                //    return _prop;
                //}
                //throw new VJSError(201, prop.name);
            },

            /**
             * The setter for the property. Will deny access for
             * non-public properties and methods from the outer scope.
             * It will also deny the change for "const" properties and
             * methods, unless they are undefined.
             * @return void
             * @throws {Error} If the property is not writeable.
             */
            set : function( value ) {
                if ( !state._inPrivateScope && prop.access != "public" ) {
                    throw new VJSError(201, prop.name);
                }
                if ( prop["const"] && _prop !== undefined ) {
                    throw new VJSError(203, prop.name, Class.name);
                }
                _prop = state.props[prop.name] = typeof value == "function" ?
                    createMethod(obj, value, prop.name, state, staticState) :
                    value;
            },

            // Non-public members are NOT enumerable
            enumerable : prop.access != "private",

            // Non-constant members are configurable
            // (so that they can be deleted)
            configurable : !prop["const"]
        }
    );
}

/*
 * JavaScriipt implementation of the most useful parts of the C printf function
 */
function printf() {
    var args = Array.prototype.slice.call(arguments),
        len  = args.length,
        tpl;

    if ( !len ) {
        return "";
    }

    tpl = args.shift();

    return tpl.replace(
        rePrintfTokens,
        function(match, precision, precisionDigit, specifier, pos, input) {
            var replacement = args.shift();

            if (replacement === undefined) {
                return "";
            }

            switch ( specifier ) {

                // Signed decimal integer
                case "d":
                case "i":
                    return parseInt(replacement, 10);

                // Unsigned decimal integer
                case "u":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement);
                    }
                    return replacement;

                // Unsigned hexadecimal integer 7fa
                case "x":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16);
                    }
                    return replacement;

                // Unsigned hexadecimal integer (uppercase) 7FA
                case "X":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16).toUpperCase();
                    }
                    return replacement;

                // Decimal floating point, lowercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                case "f":
                    return parseFloat(replacement).toFixed(
                        parseInt(precisionDigit, 10)
                    );

                //Decimal floating point, uppercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                case "F":
                    return parseFloat(replacement).toFixed(
                        parseInt(precisionDigit, 10)
                    ).toUpperCase();

                case "c":
                case "s":
                    return String(replacement);

                case "%":
                    return "%";

                default:
                    return "%" + specifier;
            }
        }
    );
}

/**
 * Class VJSError extends Error. This is used to create Error object
 * by providing an error code to the constructor and other variables
 * that are substituted into the message using printf. This class
 * exists for several reasons:
 * 1. To make it possible to store all the error messages in one place
 * 2. To make it easier to implement some kind of message translations
 *    later
 * 3. To allow for test to compare errors by their code instead of by
 *    the message that can vary
 * @param {Number|String} code The error code as number or as numeric
 *    string
 * @param {*} any Zero or more additional arguments to be substituted into
 *    the message
 * @return {VJSError}
 */
function VJSError( code ) {
    var params = Array.prototype.slice.call(arguments, 1);
    this.code = code || 0;
    params.unshift(errors[this.code]);
    this.message = printf.apply({}, params);
}

VJSError.prototype = Object.create(Error.prototype);

