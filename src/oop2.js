(function(NS, undefined) {
    
    var reservedWords = {
        "public"    : 1,
        "private"   : 1,
        "protected" : 1,
        "const"     : 1,
        "static"    : 1,
        "final"     : 1,
        "abstract"  : 1
    };

    var reSpaces       = /\s+/,
        reClass        = /\bclass\b/,
        reExtends      = /\bextends\s+(.+?)(?:\s+implements|$)/,
        reImplements   = /\bimplements\s+(.+?)(?:\s+extends|$)/,
        reWsOrEof      = /(\s.*|$)/,
        reCommaSplit   = /\s*,\s*/,
        reSpaceTrim    = /^\s*|\s*$/,
        reJsPath       = /\[['"]?([^\]]+)['"]?\]/g,
        defineProperty = Object.defineProperty;
    
    var _classes = {};
    
    function jPath(path, value) {
        var segments = path.replace(reJsPath, ".$1").split("."),
            l        = segments.length,
            curPath  = [],
            mode     = arguments.length === 1 ? "get" : "set",
            cur      = vjs.config.namespace,
            name, 
            next,
            i;
            
        for ( i = 0; i < l; i++ ) {
            curPath[i] = name = segments[i];
            
            if (is(cur, "array")) {
                name = parseInt(name, 10);
            }
            
            if ( i == l - 1 ) { // last
                if ( mode == "set" ) {
                    cur[name] = value;
                }
                return cur[name];
            }
            
            if (!cur.hasOwnProperty(name)) {
            
                // Called to read, but an intermediate path segment was not
                // found - return undefined
                if ( mode == "get" ) {
                    return undefined;
                }
                
                // Called to write, but an intermediate path segment was not
                // found - create it and continue
                next = segments[ i + 1 ];
                cur[name] = isNaN( parseFloat(next) ) || 
                    String(parseFloat(next)) !== String(next) ? {} : [];
            }
            
            cur = cur[name];
        }
    }
    
    /**
     * Checks if @x is of type @type.
     * @param x The "thing" to check the type of
     * @param {String} type
     * @returns {Boolean}
     */
    function is(x, type) {
        
        var _type    = String(type).toLowerCase(),
            toString = Object.prototype.toString,
            xType    = toString.call(x).toLowerCase();
        
        // Built-in objects
        if (_type == "object" || 
            _type == "function" ||
            _type == "array" ||
            _type == "string" ||
            _type == "boolean" ||
            _type == "date" || 
            _type == "regexp") {
            return xType == "[object " + type + "]";
        }
        
        // Strict number
        if (_type == "number") {
            return xType == "[object " + type + "]" && !isNaN(x) && isFinite(x);
        }
        
        // Custom objects - use instanceof
        if (xType == "[object object]" && toString.call(type) == "[object Function]") {
            return (x instanceof type);
        }
        
        // Everything else - just use simple strict comparison
        return x === type;
    }
    
    function noop() {};

    function vjs(declaration, properties) {
        return new _class(declaration, properties).constructor;
    }
    
    vjs.config = {
        namespace : NS
    };
    
    vjs.getClass = function( name ) {
        return _classes[ name ];
    };

    vjs.createInstance = function( name ) {
        var _class = _classes[ name ];
        if (!_class) {
            throw "No such class '" + name + "'";
        }
        return _class.constructor.apply(
            Object.create(_class.constructor.prototype), 
            Array.prototype.slice.call(arguments, 1)
        );
    };

    function trim(str) {
        return str.trim ? str.trim() : str.replace(reSpaceTrim, "");
    }

    /**
     * Creates a property object from the given declaration and value
     * @param {String} declaration The property declaration
     * @param {*} value The property value
     * @return {Object} The property object
     * @throws {Error} Upon validation failure
     */
    function property( declaration, value ) 
    {
        var arr = trim(declaration).split(reSpaces),
            out = {
                "access"   : "public",
                "const"    : false,
                "static"   : false,
                "final"    : false,
                "abstract" : false,
                "value"    : value
            },
            len = arr.length,
            decl = [],
            mod, modLower;
        
        if (!len) {
            throw new Error("Invalid (empty) property declaration.");
        }
        
        out.name = arr[--len];
        if (out.name in reservedWords) {
            throw new Error("Invalid property name '" + out.name + "'.");
        }
        
        while (len) {
            mod = arr[--len];
            modLower = mod.toLowerCase();
            switch (modLower) {
                case "public":
                case "private":
                case "protected":
                    out.access = modLower;
                break;
                case "const":
                case "static":
                case "final":
                case "abstract":
                    out[modLower] = true;
                break;
                default:
                    throw new Error("Invalid property declaration. Unrecognised token '" + mod + "'.");
            }
        }
        
        if (!out.name) {
            throw new Error("Invalid property declaration. Missing name.")
        }
        
        // make sure that the properties have their declaration in common format so 
        // that they can be compared between each other. Although the modifiers 
        // order doesn't matter, the declaration property will always be in the 
        // format: "[abstract] [final] [const] access [static] name"
        if (out["abstract"]) {
            decl.push("abstract");
        }
        if (out["final"]) {
            decl.push("final");
        }
        if (out["const"]) {
            decl.push("const");
        }
        decl.push(out.access);
        if (out["static"]) {
            decl.push("static");
        }
        decl.push(out.name);
        out.declaration = decl.join(" ");
        
        return out;
    }

    /**
     * Creates a method of the object "obj". The purpose of that is to make 
     * every method entering the private scope at the beginning and leaving 
     * it at the end.
     * @param {Function} fn The actual function to be bound
     * @return {Function} The created method
     */
    function createMethod(fn, name, state, staticState) 
    {
        return staticState ? 
            function() {
                state._inPrivateScope++;
                staticState._inPrivateScope++;
                var out = fn.apply(this, arguments);
                staticState._inPrivateScope--;
                state._inPrivateScope--;
                return out;
            } :
            function() {
                state._inPrivateScope++;
                var out = fn.apply(this, arguments);
                state._inPrivateScope--;
                return out;
            };
    }

    function createProperty(obj, prop, state, staticState) 
    {
        // Set the initial value of the property
        var _prop = state.props[prop.name] = typeof prop.value == "function" ? 
            createMethod(prop.value, prop.name, state, staticState) : 
            prop.value;
        
        // There is no need for getters and setters for the simple properties
        if ( !prop["const"] && prop.access == "public" ) {
            obj[prop.name] = _prop;
            return;
        }
        
        // Use Object.defineProperty to do the access magic...
        defineProperty(
            obj, 
            prop.name, 
            {
                /**
                 * The getter for the property. Will deny access for 
                 * non-public properties and methods from the outer scope.
                 * @return {*} The property value if accessible.
                 * @throws {Error} If the property is not accessible.
                 */
                get : function() { 
                    if ( !state._inPrivateScope && prop.access != "public" ) {
                        throw new Error(
                            "Cannot access the property '" + 
                            prop.name + 
                            "' from out of scope"
                        );
                    }
                    return _prop;
                },
                
                /**
                 * The setter for the property. Will deny access for 
                 * non-public properties and methods from the outer scope. 
                 * It will also deny the change for "const" properties and 
                 * methods, unless they are undefined.
                 * @return void
                 * @throws {Error} If the property is not writeable.
                 */
                set : function( value ) {
                    if ( !state._inPrivateScope && prop.access != "public" ) {
                        throw new Error(
                            "Cannot access the property '" + 
                            prop.name + 
                            "' from out of scope"
                        );
                    }
                    if ( prop["const"] && _prop !== undefined ) {
                        throw new Error(
                            "Cannot change the value of the constant '" + 
                            prop.name + 
                            "' property"
                        );
                    }
                    _prop = state.props[prop.name] = typeof value == "function" ? 
                        createMethod(value, prop.name, state, staticState) : 
                        value;
                },
                
                // Non-public members are NOT enumerable
                enumerable : prop.access == "public",
                
                // Non-constant members are configurable 
                // (so that they can be deleted)
                configurable : !prop["const"]
            }
        );
    }
    
    function parseClassDeclaration(declaration, inst) {
        var tokens     = trim(declaration || "").split(reClass),
            mods       = trim(tokens[0]).split(reSpaces),
            right      = trim(tokens[1]),
            path       = right.replace(reWsOrEof, ""),
            ext        = right.match(reExtends),
            impl       = right.match(reImplements),
            modsLength = mods.length, 
            name, mod;
        
        while (modsLength) {
            mod = mods[--modsLength];
            switch (mod) {
                case "":
                    break;
                case "abstract":
                    inst.isAbstract = true;
                    break;
                case "final":
                    inst.isFinal = true;
                    break;
                default:
                    throw new Error("Unrecognised class modifier '" + mod + "'");
            }
        }
        
        /**
         * The identifier (class path) pointing to the superclass that 
         * this class should inherit from.
         */
        inst["extends"] = ext ? ext[1] : null;
        
        /**
         * An array of zero or more interface identifiers pointing to the 
         * interface(s) that this class should implement.
         */
        inst["implements"] = impl ? impl[1].split(reCommaSplit) : [];
        
        inst.path = path;
        
        inst.name = path.replace(reJsPath, ".$1").split(".").pop();
        
        _classes[path] = inst;
    }

    function InstanceState( superClass ) {
        this.props = {};
        this._inPrivateScope = 0;
        this.superState = superClass ? superClass.instanceProperties : null;
    }

    function StaticState( superClass ) {
        this.props = {};
        this._inPrivateScope = 0;
        this.superState = superClass ? superClass.classProperties : null;
    }

    function _class(declaration, properties) 
    {
        /**
         * The original (unparsed) declaration string.
         */
        this.declaration = declaration;
        
        /**
         * A map of name:property that will contain all the property objects 
         * as returned from the property() function.
         */
        this.instanceProperties = {};
        
        /**
         * A map of name:property that will contain all the property objects 
         * as returned from the property() function. Same as the instanceProperties 
         * above, but for static properties and methods
         */
        this.classProperties = {};
        
        ////////////////////////////////////////////////////////////////////////////
        var _thisClass = this,
            _staticState,
            _compiledInstanceProps = {},
            _compiledStaticProps = {},
            _super,
            _prop,
            _x;
        
        parseClassDeclaration(declaration, this);
        
        if ( this["extends"] ) {
            _super = _classes[this["extends"]];
            if (!_super) {
                if ( is(vjs.config.loadClass, "function") ) {
                    _super = vjs.config.loadClass(this["extends"]);
                }
                if (!_super) {
                    throw new Error("Cannot inherit from undefined class '" + this["extends"] + "'");
                }
                if (!(_super instanceof _class)) {
                    throw new Error("Cannot inherit from class '" + this["extends"] + "'");
                }
            }
            if (_super.isFinal) {
                throw new Error("Cannot inherit from final class '" + this["extends"] + "'");
            }
        }
        
        _staticState = new StaticState(_super);
        
        if ( _thisClass.isAbstract ) {
            this.constructor = function Class() {
                throw new Error(
                    'Cannot create an instance of the abstract class "' + 
                    _thisClass.name + '"'
                );
            };
        } else {
            this.constructor = function Class() {
                var _state = new InstanceState(_super), name;
                for ( name in _thisClass.instanceProperties ) {
                    createProperty( 
                        this, 
                        _thisClass.instanceProperties[ name ], 
                        _state,
                        _staticState
                    );
                }
                
                if ( typeof this.init == "function" ) {
                    this.init.apply( this, arguments );
                }
            };
        }
        
        // Compile properties ======================================================
        if ( properties ) { // the second argument is optional
            
            // the second argument can be function returning the properties
            if ( typeof properties == "function" ) {
                properties = properties(
                    function(instance, name) {  
                        return _super &&
                            _super.instanceProperties && 
                            _super.instanceProperties[name] && 
                            typeof _super.instanceProperties[name].value == "function" ?
                            _super.instanceProperties[name].value.apply(
                                instance, 
                                Array.prototype.slice.call(arguments, 2)
                            ) :
                            undefined;
                    },
                    this.constructor
                );
            }
            
            if ( !properties || typeof properties !== "object" ) {
                throw new Error( "Invalid class properties declaration" );
            }
            
            for ( _x in properties ) {
                _prop = property(_x, properties[_x]);
                
                // Check for abstract methods
                if ( _prop["abstract"] ) {
                    
                    if ( _prop.access == "private" ) {
                        throw new Error(
                            'The abstract property or method "' +
                            _prop.name + 
                            '" cannot be private'
                        );
                    }
                    
                    if ( _prop["final"] ) {
                        throw new Error(
                            'The abstract property or method "' +
                            _prop.name + 
                            '" cannot be final'
                        );
                    }
                    
                    if ( !_thisClass.isAbstract ) {
                        throw new Error(
                            'The property "' + _prop.name + '" is abstract, therefore ' +
                            'the class "' + _thisClass.name + '" must also be defined ' +
                            'abstract'
                        );
                    }
                }
                
                if ( _prop["static"] ) {
                    _compiledStaticProps[_prop.name] = _prop;
                } else {
                    _compiledInstanceProps[_prop.name] = _prop;
                }
            }
        }
        
        // Inheritance =============================================================
        if ( _super ) {
            
            // Keep the prototype chain
            _thisClass.constructor.prototype = Object.create(_super.constructor.prototype);
            _thisClass.constructor.prototype.constructor = _thisClass.constructor;
            
            
            // inherit instance properties
            for ( _x in _super.instanceProperties ) {
                _prop = _super.instanceProperties[_x];
                
                if ( _prop.name in _compiledInstanceProps ) {
                    
                    // Don't override finals
                    if ( _prop["final"] ) {
                        throw new Error(
                            "The final property or method '" + 
                            _prop.name + 
                            "' cannot be overridden"
                        );
                    }
                } else {
                    if ( _prop["abstract"] ) {
                        throw new Error(
                            'The class "' + _thisClass.name + '" must implement ' + 
                            'the "' + _prop.name + '" property or method'
                        );
                    }
                }
                
                if ( _prop.access != "private" ) {
                    this.instanceProperties[_prop.name] = _prop;
                    //_thisClass.constructor.prototype._super[_prop.name] = _prop.value;
                }
            }
            
            // inherit static properties
            for ( _x in _super.classProperties ) {
                _prop = _super.classProperties[_x];
                
                if ( _prop["final"] && _prop.name in _compiledStaticProps ) {
                    throw new Error(
                        "The final property or method '" + 
                        _prop.name + 
                        "' cannot be overridden"
                    );
                }
                
                if ( _prop.access != "private" ) {
                    this.classProperties[_prop.name] = _prop;
                    createProperty(this.constructor, _prop, _staticState);
                }
            }
        }
        
        // Instance properties =====================================================
        for ( _x in _compiledInstanceProps ) {
            _prop = _compiledInstanceProps[_x];
            this.instanceProperties[_prop.name] = _prop;
        }
        
        // Static properties =======================================================
        for ( _x in _compiledStaticProps ) {
            _prop = _compiledStaticProps[_x];
            this.classProperties[_prop.name] = _prop;
            createProperty(this.constructor, _prop, _staticState);
        }
        
        if (vjs.config.namespace) {
            jPath(this.path, this.constructor);
        }
    }



    NS.property = property;
    NS._class = _class;
    NS.vjs = vjs;

})(this);