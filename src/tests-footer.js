NS.trim     = trim;
NS.is       = is;
NS.jPath    = jPath;
NS.VJSError = VJSError;
NS.property = property;
NS.printf   = printf;
NS._class   = _class;

// Export predefined error codes and messages so that the tests
// can compare the codes
NS.errors = errors;

/**
 * The nodeunit tests can use this function to test if some code
 * throws a VJSError having the specified code.
 * @param {NodeUnit.test} test The test
 * @param {Function} fn The function that will be executed and is expected
 * to throw VJSError
 * @param {Number} code The expected error code
 * @param {String} message Optional error message
 */
NS.throwsVJSErrorCode = function(test, fn, code, message) {
     var _code;
    try {
        fn();
    } catch (e) {
        _code = e.code;
    }
    test.equal(_code, code, message);
};

