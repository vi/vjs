(function(NS, undefined) {

var reSpaces       = /\s+/,
    reSpacesLeft   = /^\s+/,
    reSpacesRight  = /\s+$/,
    reClass        = /\bclass\b/,
    reExtends      = /\bextends\s+(.+?)(?:\s+implements|$)/,
    reImplements   = /\bimplements\s+(.+?)(?:\s+extends|$)/,
    reWsOrEof      = /(\s.*|$)/,
    reCommaSplit   = /\s*,\s*/,
    reSpaceTrim    = /^\s*|\s*$/,
    reJsPath       = /\[['"]?([^\]]+)['"]?\]/g,
    rePrintfTokens = /%(\.(\d))?([a-zA-Z%])/g,
    defineProperty = Object.defineProperty,
    toString       = Object.prototype.toString,
    hasOwn         = Object.hasOwnProperty,
    slice          = Array.prototype.slice,
    reservedWords  = {
        "public"    : 1,
        "private"   : 1,
        "protected" : 1,
        "const"     : 1,
        "static"    : 1,
        "final"     : 1,
        "abstract"  : 1
    },
    errors = {
        // General message for unknown error
        0   : "Unknown VJS error",

        // 100-199 - Parse errors
        100 : "Invalid property declaration.",
        101 : "Invalid (empty) property declaration.",
        102 : "Invalid property name '%s' (reserved word).",
        103 : "Invalid property declaration. Unrecognised token '%s'.",
        104 : "Invalid property declaration. Missing name.",
        105 : "Unrecognised class modifier '%s'",
        106 : "Invalid class properties declaration",
        107 : "Invalid class declaration '%s'. Missing class name or path.",

        // 200-299 Runtime errors
        201 : "Cannot access the property '%s' from out of scope",
        202 : "No such class '%s'.",
        203 : "Cannot change the value of the constant '%s' property of class '%s'.",
        204 : "Cannot inherit from undefined class '%s'.",
        205 : "Cannot inherit from class '%s'",
        206 : "Cannot inherit from final class '%s'",
        207 : "Cannot create an instance of the abstract class '%s'.",
        208 : "The abstract property or method '%s' cannot be private",
        209 : "The abstract property or method '%s' cannot be final",
        210 : "The property '%s' is abstract, therefore the class '%s' must " +
              "also be defined as abstract",
        211 : "The final property or method '%s' cannot be overridden",
        212 : "The class '%s' must implement the '%s' property or method",
        213 : "The method '%s' of class '%s' does not have a super method"
    };

/**
 * The internal storage for classes. Contains all the classes that have been 
 * created stored by their full path as a key.
 * @type Object
 * @private
 */
var _classes = {};

/**
 * Just an empty function for whatever we might need it.
 * @return void
 */
function noop() {}

/**
 * Trims the white space on the both sides of a string. Delegates
 * to the native trim if it exists.
 * @param {String} str The string to trim
 * @return String
 */
function trim(str) {
    return str.trim ?
        str.trim() :
        String(str).replace(reSpaceTrim, "");
}

function rtrim(str) {
    return str.replace(reSpacesRight, "");
}

function ltrim(str) {
    return str.replace(reSpacesLeft, "");
}

function isArray(x) {
    return toString.call(x) == "[object Array]";
}

function isFunction(x) {
    return toString.call(x) == "[object Function]";
}

function isWindow(x) {
    return x && x === x.window;
}

function isPlainObject(x) {
    // Not plain objects:
    // - Any object or value whose internal [[Class]] property is not "[object Object]"
    // - DOM nodes
    // - window
    if (toString.call(x) != "[object Object]" || x.nodeType || isWindow(x)) {
        return false;
    }

    if (x.constructor && !hasOwn.call(x.constructor.prototype, "isPrototypeOf")) {
        return false;
    }

    // If the function hasn't returned already, we're confident that
    // |obj| is a plain object, created by {} or constructed with new Object
    return true;
}

function mixin(a, b) {
    var key, src, tgt;
    for ( key in b ) {
        src = b[key];
        tgt = a[key];

        if (src === tgt)
            continue;

        if (src && isArray(src)) {
            a[key] = mixin(tgt && isArray(tgt) ? tgt : [], src);
        } else if (src && isPlainObject(src)) {
            a[key] = mixin(tgt && isPlainObject(tgt) ? tgt : [], src);
        } else {
            a[key] = src;
        }
    }
    return a;
}

function clone(x) {
    return x && isArray(x) ? 
        mixin([], x) :
        x && isPlainObject(x) ?
            mixin({}, x) : 
            x;
}

var uid = (function() {
    var n = 0, add = "", max = 100000000000000000000;
    return function() {
        if (++n > max) {
            add += n + "";
            n = 0;
        }
        return "uid_" + add + n;
    };
})();

/**
 * Navigates to the @path inside @obj and returns the value or updates it if
 * called with 3 arguments.
 * @param {Object} obj The object to use
 * @param {String} path
 * @param {*} value (optional)
 */
function jPath(obj, path, value) {
    var segments = path./*replace(reJsPath, ".$1").*/split("."),
        l        = segments.length,
        curPath  = [],
        modeGET  = 2,
        modeSET  = 3,
        modeDEL  = 4,
        mode     = arguments.length === 3 ?
            value === undefined ?
                modeDEL :
                modeSET :
            modeGET,
        cur      = obj,
        inArray,
        name,
        next,
        i;

    for ( i = 0; i < l; i++ ) {
        curPath[i] = name = segments[i];
        inArray = isArray(cur);

        if (inArray) {
            name = parseInt(name, 10);
        }

        if ( i === l - 1 ) { // last
            if ( mode === modeDEL ) {
                if ( inArray ) {
                    cur.splice(name, 1);
                }
                else if ( cur.hasOwnProperty(name) ) {
                    delete cur[name];
                }
            }
            else if ( mode === modeSET ) {
                cur[name] = value;
            }
            return cur[name];
        }

        if (!cur.hasOwnProperty(name)) {

            // Called to read, but an intermediate path segment was not
            // found - return undefined
            if ( mode === modeGET ) {
                return undefined;
            }

            // Called to write, but an intermediate path segment was not
            // found - create it and continue
            next = segments[ i + 1 ];
            cur[name] = isNaN( parseFloat(next) ) ||
                String(parseFloat(next)) !== String(next) ? {} : [];
        }

        cur = cur[name];
    }
}

var PropertyCache = {};

/**
 * Creates a property object from the given declaration and value
 * @param {String} declaration The property declaration
 * @param {*} value The property value
 * @return {Object} The property object
 * @throws {Error} Upon validation failure
 */
function Property(declaration, value, owner) {

    //if (declaration in PropertyCache) {
    //    return PropertyCache[declaration].clone(value, owner);
    //}


    var arr  = trim(String(declaration || "")).split(reSpaces),
        len  = arr.length,
        name;

    // Empty declaration
    //if (!arr[0]) {
    //    throw new VJSError(101);
    //}

    name = arr[--len];

    // missing name
    if (!name) {
        throw new VJSError(104);
    }

    // name is a reserved word
    if (reservedWords.hasOwnProperty(name)) {
        throw new VJSError(102, name);
    }

    this.name = name;

    while ( len ) {
        switch (arr[--len]) {
            case "public":
                this.isPrivate   = false;
                this.isProtected = false;
                this.isPublic    = true ;
            break;
            case "private":
                this.isPrivate   = true ;
                this.isProtected = false;
                this.isPublic    = false;
            break;
            case "protected":
                this.isPrivate   = false;
                this.isProtected = true ;
                this.isPublic    = false;
            break;
            case "const":
                this.isConst = true;
            break;
            case "static":
                this.isStatic = true;
            break;
            case "final":
                this.isFinal = true;
            break;
            case "abstract":
                this.isAbstract = true;
            break;
            default:
                throw new VJSError(103, arr[len]);
        }
    }

    // Check for invalid combinations
    if ( this.isAbstract ) {
        if ( this.isPrivate ) { // Can't have abstract + private
            throw new VJSError(208, name);
        }
        if ( this.isFinal ) { // Can't have abstract + final
            throw new VJSError(209, name);
        }
    }

    this.owner = owner;
    this.value = value;

    //PropertyCache[declaration] = this;
}

Property.prototype = {
    isConst     : false,
    isStatic    : false,
    isFinal     : false,
    isAbstract  : false,
    value       : undefined,
    isPublic    : true,
    isProtected : false,
    isPrivate   : false,
    owner       : undefined,

    // make sure that the properties have their declaration in common format so
    // that they can be compared between each other. Although the modifiers
    // order doesn't matter, the declaration property will always be in the
    // format: "[abstract] [final] [const] access [static] name"
    getDeclaration : function() {
        var decl = [];

        if (this.isAbstract) {
            decl.push("abstract");
        }
        if (this.isFinal) {
            decl.push("final");
        }
        if (this.isConst) {
            decl.push("const");
        }
        
        if (this.isPublic) {
            decl.push("public");
        } else if (this.isPrivate) {
            decl.push("private");
        } else if (this.isProtected) {
            decl.push("protected");
        }

        if (this.isStatic) {
            decl.push("static");
        }
        
        decl.push(this.name);
        
        return decl.join(" ");
    },

    clone : function(newValue, newOwner) {
        return {
            isConst        : this.isConst,
            isStatic       : this.isStatic,
            isFinal        : this.isFinal,
            isAbstract     : this.isAbstract,
            value          : newValue,
            isPublic       : this.isPublic,
            isProtected    : this.isProtected,
            isPrivate      : this.isPrivate,
            owner          : newOwner,
            name           : this.name,
            getDeclaration : this.getDeclaration,
            clone          : this.clone
        };
    }
};



function createPropertyGetter(prop, state, stack) {
    return function() {
        if (!prop.isPublic && !stack[0]) {
            throw new VJSError(201, prop.name);
        }
        return state[prop.name];
    };
}

function createPropertySetter(prop, state, stack, staticStack) {
    return function(value) {
        if (!prop.isPublic && !stack[0]) {
            throw new VJSError(201, prop.name);
        }
        if ( prop.isConst && state[prop.name] !== undefined ) {
            throw new VJSError(203, prop.name, prop.owner.name);
        }
        state[prop.name] = typeof value == "function" ? 
            createMethod(value, stack, prop.name, staticStack) :
            value;
    };
}

function createMethod(fn, stack, name, staticStack) {
    

    var str = fn.toString();
    var body= str.replace(/.*?\{\s*/, "").replace(/\s*\};?\s*$/, "");
    //var args = str.substr(0, str.indexOf(")")).replace(/^.*?\(/, "");
    // console.log(str);
    // console.log(
    //     "(function tmp() {" +
    //     "\n\treturn function " + name + "(" + args + ") {" +
    //     "\n\t\tstack.push(name);"+
    //     "\n\t\tif (staticStack) staticStack.push(name);" +
    //     "\n\t\t" + body +
    //     "\n\t\tif (staticStack) staticStack.pop();" +
    //     "\n\t\tstack.pop();" +
    //     "\n\t};})"
    // );
    if (!body) {
        console.log(
            fn.toString()
        );
        return fn;
    }
    if (staticStack) {
        return function method() {
            stack.push(name);
            staticStack.push(name);
            var out = fn.apply(this, arguments);
            staticStack.pop();
            stack.pop();
            return out;
        };
    }
    // var mtd = Function(
    //     "stack, staticStack, name",
    //     "return function " + name + "(" + args + ") {" +
    //     "\n\t\tvar __out__;"+
    //     "\n\t\tstack.push(name);"+
    //     "\n\t\tif (staticStack) staticStack.push(name);" +
    //     "\n\t\t" + body.replace(/\breturn?\s*(.*)?[;$]/, "__out__ = $1;") + 
    //     "\n\t\tif (staticStack) staticStack.pop();" +
    //     "\n\t\tstack.pop();" +
    //     "\n\t\treturn __out__;" +
    //     "\n};"
    // )(stack, staticStack, name);
    
    
    // //mtd = mtd(stack, staticStack, name);
    // console.log(
    //     mtd.toString()
    // );
    // return mtd;
    return function method() {
        // var isNew = this.constructor === method,
        //     o     = isNew ? Object.create(fn.prototype) : this,
        //     out;
        
        // stack.push(name);
        // if (staticStack) {
        //     staticStack.push(name);
        // }
        
        // out = fn.apply(o, arguments);
        
        // if (staticStack) {
        //     staticStack.pop();
        // }
        // stack.pop();
        
        // return isNew ? out ||o : out;

        stack.push(name);
        var out = fn.apply(this, arguments);
        stack.pop();
        return out;
    };
}

/**
 * Creates a propperty attached to the object @obj but also maintains a link
 * to the private local objects @scope and @staticScope where the actual
 * values are stored. Unles the property should be "public" it can be
 * considered as a virtual property, meaning that it might have getters and
 * setters on the @obj but the actual value is stored at @state or
 * @staticState (depending on if the property is static or not)
 * @param {Object} The object to augment
 * @param {Object} property The property object created with the property
 *  function
 * @param {Object} state The actual property-value storage object
 * @param {Object} staticState The storage for the static properties. This
 *  might be the same object as @state if the property is static. Otherwise
 *  it is different. It is not used directly by this function but it gets
 *  passed to the createMethod function so that instance methods can also
 *  "unlock" the private and protected static scope...
 * TODO staticState needs a better description or just some better solution
 */
function createProperty(prop, obj, state, stack, staticStack) {
    var name  = prop.name,
        value = clone(prop.value);
    
    if (prop.isPublic && !prop.isConst) {
        if (typeof value == "function") {
            obj[name] = createMethod(value, stack, name, staticStack);
        } else {
            obj[name] = value;
        }
    } else {
        state[name] = typeof value == "function" ? 
            createMethod(value, stack, name, staticStack) :
            value;

        defineProperty(obj, name, {
            get          : createPropertyGetter(prop, state, stack),
            set          : createPropertySetter(prop, state, stack, staticStack),
            enumerable   : !prop.isPrivate,
            configurable : !prop.isConst
        });
    }
}

function createObject(Class, inst) {
    var x, 
        template    = Class.instanceProperties,
        staticStack = Class.privateStaticStack,
        state       = {},
        stack       = [],
        scope       = Class;

    inst.__super__ = function() {
        var len = stack.length,
            name,
            prop,
            owner,
            superProp,
            out,
            old;
        
        if (len) {
            name = stack[len - 1];
            prop = scope.instanceProperties[name];
            if (prop) {
                owner     = prop.owner;
                superProp = prop._super;
                if (superProp) {
                    old   = scope;
                    scope = superProp.owner;
                    out   = superProp.value.apply(inst, arguments);
                    scope = old;
                    return out;
                }
            }
            throw new VJSError(213, name, Class.name);
        }
    };

    for (x in template) {
        createProperty(template[x], inst, state, stack, staticStack);
    }
}

/*
 * JavaScriipt implementation of the most useful parts of the C printf function
 */
function printf() {
    var args = slice.call(arguments),
        len  = args.length,
        tpl;

    if ( !len ) {
        return "";
    }

    tpl = args.shift();

    return tpl.replace(
        rePrintfTokens,
        function(match, precision, precisionDigit, specifier, pos, input) {
            var replacement = args.shift();

            if (replacement === undefined) {
                return "";
            }

            switch ( specifier ) {

                // Signed decimal integer
                case "d":
                case "i":
                    return parseInt(replacement, 10);

                // Unsigned decimal integer
                case "u":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement);
                    }
                    return replacement;

                // Unsigned hexadecimal integer 7fa
                case "x":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16);
                    }
                    return replacement;

                // Unsigned hexadecimal integer (uppercase) 7FA
                case "X":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16).toUpperCase();
                    }
                    return replacement;

                // Decimal floating point, lowercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                //case "f":
                //    return parseFloat(replacement).toFixed(
                //        parseInt(precisionDigit, 10)
                //    );

                //Decimal floating point, uppercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                //case "F":
                //    return parseFloat(replacement).toFixed(
                //        parseInt(precisionDigit, 10)
                //    ).toUpperCase();

                case "c":
                case "s":
                    return String(replacement);

                case "%":
                    return "%";

                default:
                    return "%" + specifier;
            }
        }
    );
}

/**
 * Class VJSError extends Error. This is used to create Error object
 * by providing an error code to the constructor and other variables
 * that are substituted into the message using printf. This class
 * exists for several reasons:
 * 1. To make it possible to store all the error messages in one place
 * 2. To make it easier to implement some kind of message translations
 *    later
 * 3. To allow for test to compare errors by their code instead of by
 *    the message that can vary
 * @param {Number|String} code The error code as number or as numeric
 *    string
 * @param {*} any Zero or more additional arguments to be substituted into
 *    the message
 * @return {VJSError}
 */
function VJSError( code ) {
    var params = slice.call(arguments, 1);
    this.code = code || 0;
    params.unshift(errors[this.code]);
    this.message = printf.apply({}, params);
}

VJSError.prototype = Object.create(Error.prototype);

/**
 * The vjs function - the main object that gets exported.
 * @param {String} declaration
 * @param {Object|Function} properties
 * @return {Function} The constructor of the created class
 */
function vjs(declaration, properties) {
    return (new Class(declaration, properties)).Constructor;
}

/**
 * The globally available static configuration object. Can be modified to
 * change the vjs behavior...
 * @type Object
 */
vjs.config = {
    globalClasses : false
};

/**
 * This can be used to obtain a reference to a class object by name (normally,
 * the clients will only work with class constructors and are not going to
 * need this)
 * @param {String} name The name of the needed class
 */
vjs.getClass = function( path ) {
    return _classes[ path ];
};

//vjs.getClassOf = function( inst ) {
//    for ( var c in _classes ) {
//        if ( _classes[c].Constructor === inst.constructor ) {
//            return _classes[c];
//        }
//    }
//};

/**
 * Utility function that can create new instances of the given class name.
 * The first argument is required and must be the class name and any additional
 * arguments will be passed to the class constructor.
 * @param {String} name The class name
 */
vjs.createInstance = function( path ) {
    var _class = _classes[ path ], inst;
    if (!_class) {
        throw new VJSError(202, path);
    }
    inst = Object.create(_class.Constructor.prototype);
    _class.Constructor.apply(inst, slice.call(arguments, 1));
    return inst;
};

var classDeclarationCache = {};

/**
 * Parses the class declaration string @declaration and sets some properties on
 * the instance @inst. This might be a typical task for some instance method,
 * but it is moved here (to local private function) to make it a little bit
 * faster and because such things should remain private and invisible to the
 * outer scope.
 * @param {String} declaration The declaration string
 * @param {Class} inst The instance to augment
 * @throws VJSError If the declaration is empty or invalid
 * @return {void}
 */
function parseClassDeclaration(declaration, inst) {
    var map = classDeclarationCache[declaration];
    if (!map) {
        map = {};

        var tokens     = trim(declaration || "").split(reClass),
            mods       = rtrim(tokens[0]).split(reSpaces),
            right      = tokens.length > 1 ? ltrim(tokens[1]) : "",
            path       = right.replace(reWsOrEof, ""),
            ext        = right.match(reExtends),
            impl       = right.match(reImplements),
            modsLength = mods.length,
            name, mod;

        if (!path) {
            throw new VJSError(107, declaration);
        }

        while (modsLength) {
            mod = mods[--modsLength];
            switch (mod) {
                case "abstract":
                    map.isAbstract = true;
                    break;
                case "final":
                    map.isFinal = true;
                    break;
                case "":
                    break;
                default:
                    throw new VJSError(105, mod);
            }
        }

        /**
         * The identifier (class path) pointing to the superclass that
         * this class should inherit from.
         */
        map["extends"] = ext ? ext[1] : null;

        /**
         * An array of zero or more interface identifiers pointing to the
         * interface(s) that this class should implement.
         */
        map["implements"] = impl ? impl[1].split(reCommaSplit) : [];

        map.path = path;

        map.name = path.replace(reJsPath, ".$1").split(".").pop();

        classDeclarationCache[declaration] = map;
    }

    inst.isAbstract    = !!map.isAbstract;
    inst.isFinal       = !!map.isFinal;
    inst["extends"]    = map["extends"];
    inst["implements"] = map["implements"];
    inst.path          = map.path;
    inst.name          = map.name;

    _classes[inst.path] = inst;
}

/**
 * Lookup the class at @path and verify that it can be used as base class
 * @param {String} path The class name or path
 * @throws VJSError
 * @return {Class} The base class
 */
function findSuper(path) {
    var _super = _classes[path];

    // Call the custom class-loader function if defined
    if (!_super && isFunction(vjs.config.loadClass) ) {
        _super = vjs.config.loadClass(path);
    }

    // Cannot inherit from undefined class
    // TODO: Better error message (like "failed to evaluate class path")
    if (!_super) {
        throw new VJSError(204, path);
    }

    // Cannot inherit from non-class
    if (!(_super instanceof Class)) {
        throw new VJSError(205, path);
    }

    // Cannot inherit from final class
    if (_super.isFinal) {
        throw new VJSError(206, path);
    }

    return _super;
}

function createClassConstructor(Class) {
    return Class.isAbstract ?
        function() {
            throw new VJSError(207, Class.name);
        } :
        function() {
            createObject(Class, this);
            //if ( typeof this.init == "function" ) {
            this.init.apply( this, arguments );
            //}
        };
}

Class.prototype = {
    isAbstract : false,
    isFinal : false,

    /**
     * The super class defaults to Object (if nothing else is specified in the
     * "extends" keyword).
     * @type Function
     */
    superClass   : null
};

function Class(declaration, properties)
{
    var prop, x, parentProp, 
        thisClass = this,
        globalClasses = vjs.config.globalClasses;

    /**
     * The original (unparsed) declaration string.
     */
    this.declaration = declaration;

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the Property() function.
     */
    this.instanceProperties = Object.create(null);

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the Property() function. Same as the instanceProperties
     * above, but for static properties and methods
     */
    this.classProperties = {};

    this.privateStaticState = {};
    this.privateStaticStack = [];

    parseClassDeclaration(declaration, this);

    if ( this["extends"] ) {
        this.superClass = findSuper(this["extends"]);
    }

    this.Constructor = createClassConstructor(this);

    // Compile properties
    // (the second argument for "Class" is optional)
    // -------------------------------------------------------------------------
    if ( !properties ) {
        properties = {};
    }
    
    // "properties" can be a function
    else if ( typeof properties == "function" ) {
        properties = properties(
            this.superClass ? this.superClass.Constructor.prototype : {},
            this.Constructor
        );
    }

    // Invalid class properties declaration
    if ( !properties || typeof properties != "object" ) {
        throw new VJSError(106);
    }

    // Attach the properties to the proptptype (or to the constructor if they 
    // are static).
    // -------------------------------------------------------------------------
    for ( x in properties ) {
        prop = new Property(x, properties[x], this);
        if ( prop.isStatic ) {
            //createProperty(prop, this, this.Constructor, this.privateStaticState);
            this.classProperties[prop.name] = prop;
            createProperty(
                prop, 
                this.Constructor, 
                this.privateStaticState, 
                this.privateStaticStack
            );
        } else {
            // Check for abstract methods (Must use  abstract class)
            if ( prop.isAbstract && !this.isAbstract ) {
                throw new VJSError(210, prop.name, this.name);
            }

            parentProp = this.superClass ? 
                this.superClass.instanceProperties[prop.name] :
                null;
            
            // Don't override finals
            if (parentProp && parentProp.isFinal ) {
                throw new VJSError(211, prop.name);
            }

            this.instanceProperties[prop.name] = prop;
        }
    }

    // Inheritance
    // -------------------------------------------------------------------------
    if (this.superClass) {
        this.Constructor.prototype = Object.create(
            this.superClass.Constructor.prototype
        );
        for ( x in this.superClass.instanceProperties ) {
            parentProp = this.superClass.instanceProperties[x];
            if (parentProp && !parentProp.isPrivate) {
                if (this.instanceProperties[x]) {
                    this.instanceProperties[x]._super = parentProp;
                } else {
                    if (parentProp.isAbstract) {
                        throw new VJSError(212, this.name, parentProp.name);
                    }
                    this.instanceProperties[x] = parentProp;
                }
            }
        }
    } else {
        this.Constructor.prototype = Object.create({
            init : function() {}
        });
    }

    this.Constructor.prototype.constructor = this.Constructor;

    // Expose the constructor to global using the path if needed
    // -------------------------------------------------------------------------
    if (globalClasses) {
        jPath(NS, this.path, this.Constructor);
    }
}

// exports
// -----------------------------------------------------------------------------
// NS.createClassConstructor
// NS.createInstance
// NS.findSuper
// NS.parseClassDeclaration
NS.vjs_edge = vjs;
// NS.VJSError
// NS.printf
// NS.createObject
// NS.trim
// NS.is
// NS.jPath
// NS.createPropertyGetter
// NS.createPropertySetter
// NS.createMethod
// NS.createProperty
// 
if (typeof VJS_TESTING_NAMESPACE == "object") {
    VJS_TESTING_NAMESPACE.Class    = Class;
    VJS_TESTING_NAMESPACE.Property = Property;
}

})(this);
