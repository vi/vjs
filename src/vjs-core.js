// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

/**
 * The internal storage for classes
 * @type Object
 * @private
 */
var _classes = {};

/**
 * The vjs function - the main object that gets exported.
 * @param {String} declaration
 * @param {Object|Function} properties
 * @return {Function} The constructor of the created class
 */
function vjs(declaration, properties) {
    return new _class(declaration, properties).constructor;
}

/**
 * The globally available static configuration object. Can be modified to
 * change the vjs behavior...
 * @type Object
 */
vjs.config = {
    namespace : NS
};

/**
 * This can be used to obtain a reference to a class object by name (normally,
 * the clients will only work with class constructors and are not going to
 * need this)
 * @param {String} name The name of the needed class
 */
vjs.getClass = function( name ) {
    return _classes[ name ];
};

/**
 * Utility function that can create new instances of the given class name.
 * The first argument is required and must be the class name and any additional
 * arguments will be passed to the class constructor.
 * @param {String} name The class name
 */
vjs.createInstance = function( name ) {
    var _class = _classes[ name ];
    if (!_class) {
        throw new VJSError(202, name);
    }
    return _class.constructor.apply(
        Object.create(_class.constructor.prototype),
        Array.prototype.slice.call(arguments, 1)
    );
};

/**
 * Parses the class declaration string @declaration and sets some properties on
 * the instance @inst. This might be a typical task for some instance method,
 * but it is moved here (to local private function) to make it a little bit
 * faster and because such things should remain private and invisible to the
 * outer scope.
 * @param {String} declaration The declaration string
 * @param {_class} inst The instance to augment
 * @throws VJSError If the declaration is empty or invalid
 * @return {void}
 */
function parseClassDeclaration(declaration, inst) {
    var tokens     = trim(declaration || "").split(reClass),
        mods       = trim(tokens[0]).split(reSpaces),
        right      = trim(tokens[1]),
        path       = right.replace(reWsOrEof, ""),
        ext        = right.match(reExtends),
        impl       = right.match(reImplements),
        modsLength = mods.length,
        name, mod;

    while (modsLength) {
        mod = mods[--modsLength];
        switch (mod) {
            case "":
                break;
            case "abstract":
                inst.isAbstract = true;
                break;
            case "final":
                inst.isFinal = true;
                break;
            default:
                throw new VJSError(105, mod);
        }
    }

    /**
     * The identifier (class path) pointing to the superclass that
     * this class should inherit from.
     */
    inst["extends"] = ext ? ext[1] : null;

    /**
     * An array of zero or more interface identifiers pointing to the
     * interface(s) that this class should implement.
     */
    inst["implements"] = impl ? impl[1].split(reCommaSplit) : [];

    inst.path = path;

    inst.name = path.replace(reJsPath, ".$1").split(".").pop();

    _classes[path] = inst;
}

/**
 *
 */
function InstanceState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.instanceProperties : {};
}

/**
 *
 */
function StaticState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.classProperties : {};
}

/**
 * Lookup the class at @path and verify that it can be used as base class
 * @param {String} path The class name or path
 * @throws VJSError
 * @return {_class} The base class
 */
function findSuper(path) {
    var _super = _classes[path];

    // Call the custom class-loader function if defined
    if (!_super && is(vjs.config.loadClass, "function") ) {
        _super = vjs.config.loadClass(path);
    }

    // Cannot inherit from undefined class
    // TODO: Better error message (like "failed to evaluate class path")
    if (!_super) {
        throw new VJSError(204, path);
    }

    // Cannot inherit from non-class
    if (!(_super instanceof _class)) {
        throw new VJSError(205, path);
    }

    // Cannot inherit from final class
    if (_super.isFinal) {
        throw new VJSError(206, path);
    }

    return _super;
}

/**
 *
 */
function _class(declaration, properties)
{
    /**
     * The original (unparsed) declaration string.
     */
    this.declaration = declaration;

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function.
     */
    this.instanceProperties = {};

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function. Same as the instanceProperties
     * above, but for static properties and methods
     */
    this.classProperties = {};

    ////////////////////////////////////////////////////////////////////////////
    var _thisClass = this,
        _staticState,
        _compiledInstanceProps = {},
        _compiledStaticProps = {},
        _super,
        _prop,
        _x;

    // This will set some of the instance properties like "name", "path","final",
    // "abstract", "extends" and ""implements"."
    parseClassDeclaration(declaration, this);

    // If it is specified that this class should inherit from another, try to
    // load it and check if such inheritance is possible and valid
    if ( this["extends"] ) {
        _super = findSuper(this["extends"]);
    }

    _staticState = new StaticState(_super);

    // Create the constructor
    // -------------------------------------------------------------------------
    if ( _thisClass.isAbstract ) {

        // Trying to create an instance of abstract class must fail
        this.constructor = function Class() {
            throw new VJSError(207, _thisClass.name);
        };

    } else {
        this.constructor = function Class() {
            //var _state = new InstanceState(_super), name;
            //for ( name in _thisClass.instanceProperties ) {
            //    createProperty(
            //        this,
            //        _thisClass.instanceProperties[ name ],
            //        _state,
            //        _staticState
            //    );
            //}

            if ( typeof this.init == "function" ) {
                this.init.apply( this, arguments );
            }
        };
    }

    // -------------------------------------------------------------------------
    // loop super props
        // skip private and static
            // check final
            // check abstract
            // Attach to instanceProps

    // loop props
        // if static
            // Attach to constructor
        // else
            // check final
            // check abstract
            // Attach to instanceProps
    // -------------------------------------------------------------------------

    // Inheritance 
    // -------------------------------------------------------------------------
    // Keep the prototype chain
    this.constructor.prototype = Object.create(
        _super ? _super.constructor.prototype : {}
    );
    this.constructor.__super__ = this.constructor.prototype.constructor;
    this.constructor.prototype.constructor = this.constructor;
    this.constructor.prototype.__super__   = _super ? _super.constructor : Object;
    this.constructor.prototype.__state__   = new InstanceState(_super);


    // Compile properties
    // ------------------------------------------------------------------------
    if ( properties ) { // the second argument is optional

        // the second argument can be function returning the properties
        if ( typeof properties == "function" ) {
            properties = properties(
                function(instance, name) {
                    return _super &&
                        _super.instanceProperties &&
                        _super.instanceProperties[name] &&
                        typeof _super.instanceProperties[name].value == "function" ?
                        _super.instanceProperties[name].value.apply(
                            instance,
                            Array.prototype.slice.call(arguments, 2)
                        ) :
                        undefined;
                },
                this.constructor,
                function Super() {
                    var name = "";
                    if (_super && 
                        _super.instanceProperties && 
                        _super.instanceProperties[name] &&
                        typeof _super.instanceProperties[name].value == "function")
                    {
                        return _super.instanceProperties[name].value.call(
                            instance
                        );
                    }
                },
                function superApply() {

                }
            );
        }

        // Invalid class properties declaration
        if ( !properties || typeof properties !== "object" ) {
            throw new VJSError(106);
        }

        for ( _x in properties ) {
            _prop = property(_x, properties[_x]);

            // Check for abstract methods (Must use  abstract class)
            if ( _prop["abstract"] && !_thisClass.isAbstract ) {
                throw new VJSError(210, _prop.name, _thisClass.name);
            }

            if ( _prop["static"] ) {
                _compiledStaticProps[_prop.name] = _prop;
            } else {
                //_compiledInstanceProps[_prop.name] = _prop;
                //_thisClass.constructor.prototype[_prop.name] = _prop.value;
                createProperty(
                    _thisClass.constructor.prototype, 
                    _prop, 
                    _thisClass.constructor.prototype.__state__,
                    _staticState
                );
            }
        }
    }

    // Inheritance =============================================================
    if ( _super ) {
        /*
        // Keep the prototype chain
        _thisClass.constructor.prototype = Object.create(
            _super.constructor.prototype
        );
        _thisClass.constructor.prototype.constructor = _thisClass.constructor;
        //_thisClass.constructor.__super__ = _super.classProperties;
        _thisClass.constructor.prototype.__super__ = {};

        // inherit instance properties
        for ( _x in _super.instanceProperties ) {
            _prop = _super.instanceProperties[_x];

            if ( _prop.name in _compiledInstanceProps ) {

                // Don't override finals
                if ( _prop["final"] ) {
                    throw new VJSError(211, _prop.name);
                }
            } else {

                // must redefine abstracts
                if ( _prop["abstract"] ) {
                    throw new VJSError(212, _thisClass.name, _prop.name);
                }
            }

            if ( _prop.access != "private" ) {
                this.instanceProperties[_prop.name] = _prop;
                _thisClass.constructor.prototype.__super__[_prop.name] = 
                    typeof _prop.value == "function" ? 
                    _prop.value.bind(_super.instanceProperties):
                    _prop.value;
            }
        }*/

        // inherit static properties
        for ( _x in _super.classProperties ) {
            _prop = _super.classProperties[_x];

            if ( _prop["final"] && _prop.name in _compiledStaticProps ) {
                throw new VJSError(211, _prop.name);
            }

            if ( _prop.access != "private" ) {
                this.classProperties[_prop.name] = _prop;
                createProperty(this.constructor, _prop, _staticState);
            }
        }
    }

    // Instance properties =====================================================
    for ( _x in _compiledInstanceProps ) {
        _prop = _compiledInstanceProps[_x];
        this.instanceProperties[_prop.name] = _prop;
    }

    // Static properties =======================================================
    for ( _x in _compiledStaticProps ) {
        _prop = _compiledStaticProps[_x];
        this.classProperties[_prop.name] = _prop;
        createProperty(this.constructor, _prop, _staticState);
    }

    if (vjs.config.namespace) {
        jPath(vjs.config.namespace, this.path, this.constructor);
    }
}

NS.property = property;
NS._class = _class;
NS.vjs = vjs;
