function Test(options) {
    var noop       = function() {};
    var worker     = options.worker || noop;
    var title      = options.title  || "Untitled Test";
    var iterations = 0;
    var oncomplete = options.oncomplete || noop;
    var onstart    = options.onstart    || noop;
    var duration   = options.duration   || 2000;
    
    return {
        start : function() {
            var ended = false;
            
            function doTest() {
                if (ended) {
                    oncomplete(iterations);
                } else {
                    try {
                        worker();
                    } catch (ex) {
                        return oncomplete(iterations, ex);
                    }
                    iterations++;
                    
                    if (iterations % 1234 === 0)
                        setTimeout(doTest, 0);
                    else 
                        doTest();
                }
            }
            
            onstart();
            doTest();
            setTimeout(function() {
                ended = true;
            }, duration);
            
            
        }
    };
}