module("Utils");

test( "Testing the '_class' constructor - declaration parsing and validation", function() {
    new _class("class A");
    new _class("class B");
    new _class("class C");
    new _class("class D");
    
    strictEqual(!!(new _class("class A")).isAbstract, false);
    strictEqual(!!(new _class("abstract class A")).isAbstract, true);
    strictEqual(!!(new _class("abstract final class A")).isAbstract, true);
    strictEqual(!!(new _class("final abstract class A")).isAbstract, true);
    
    strictEqual(!!(new _class("class A")).isFinal, false);
    strictEqual(!!(new _class("final class A")).isFinal, true);
    strictEqual(!!(new _class("abstract final class A")).isFinal, true);
    strictEqual(!!(new _class("final abstract class A")).isFinal, true);
    
    strictEqual(new _class("final abstract class A").name, "A");
    strictEqual(new _class("final abstract class A").declaration, "final abstract class A");
    deepEqual(new _class("final abstract class A").extends, null);
    deepEqual(new _class("final abstract class A").implements, []);
    
    deepEqual(new _class("final abstract class A extends B").extends, "B");
    deepEqual(new _class("final abstract class A implements B, C   , D").implements, ["B", "C", "D"]);
    
    throws(
        function() {
            new _class("final abstract something class A implements B, C   , D");
        },
        /something/
    );
    
    throws(
        function() {
            new _class();
        },
        Error
    );
});

test( "Testing the 'instaceof' at single level", function() {
    var A = new _class("class A");
    var B = vjs("class B");
    var instA = new A.constructor();
    var instB = new B();
    
    ok(instA instanceof A.constructor);
    ok(instA instanceof vjs.getClass("A").constructor);
    ok(instB instanceof B);
    ok(instB instanceof vjs.getClass("B").constructor);
});

//test( "Testing the '_class' constructor - properties compilation", function() {
    
    //var C = vjs("class C", {
    //    "public    a" : 1,
    //    "protected b" : 2,
    //    "private   c" : 3,
    //    "static    d" : 4
    //});
    
    //ok(!vjs.getClass("C").instanceProperties.hasOwnProperty("d"));
    //ok(!vjs.getClass("C").classProperties.hasOwnProperty("a"));
    //console.dir(vjs.getClass("C"));
    //strictEqual(vjs.getClass("C").instanceProperties.a.value, 1);
    //strictEqual(vjs.getClass("C").instanceProperties.b.value, 2);
    //strictEqual(vjs.getClass("C").instanceProperties.c.value, 3);
    //strictEqual(vjs.getClass("C").classProperties   .d.value, 4);
//});

////////////////////////////////////////////////////////////////////////////////

test( "Testing the 'property' function - typical use case", function() {
    strictEqual( property("a").value, undefined, "The value defaults to undefined" );
    strictEqual( property("a").name, "a", "The name gets recognized");
    strictEqual( property("a").access, "public", "The value defaults to public" );
    strictEqual( property("a").final, false, "final defaults to false");
    strictEqual( property("a").static, false, "static defaults to false");
    strictEqual( property("a").const, false, "const defaults to false");
    strictEqual( property("a", 5).value, 5, "The value gets recognized");
});

test( "Testing the 'property' function - arguments validation", function() {
    throws(function() { property(); }, "Invalid (empty) property declaration.", "Throws an error if the declaration is empty");
    throws(function() { property("static"); }, /Invalid property name/, "Throws an error if the name is missing");
    throws(function() { property("something static big"); }, /Unrecognised token/, "Throws exceptions on unrecognised declarations");
});

test( "Testing the 'property' function - 'static'", function() {
    strictEqual( property("a").static, false, "static defaults to false");
    strictEqual( property("static a").static, true, "static can be set to true");
    strictEqual( property(" static a").static, true, "static can ignore the leading white space");
    strictEqual( property("private static a").static, true, "static does not have to be the first keyword");
});



test( "Testing the 'property' function - 'public'", function() {
    strictEqual( property("a").access, "public", "public defaults to true");
    strictEqual( property("public a").access, "public", "public can be set to false");
    strictEqual( property(" public a").access, "public", "public can ignore the leading white space");
    strictEqual( property("const public  a").access, "public", "public does not have to be the first keyword");
});

test( "Testing the 'property' function - 'protected'", function() {
    strictEqual( property("a").access, "public", "protected defaults to false");
    strictEqual( property("protected a").access, "protected", "protected can be set to true");
    strictEqual( property(" protected a").access, "protected", "protected can ignore the leading white space");
    strictEqual( property("const protected  a").access, "protected", "protected does not have to be the first keyword");
});

test( "Testing the 'property' function - 'private'", function() {
    strictEqual( property("a").access, "public", "private defaults to false");
    strictEqual( property("private a").access, "private", "private can be set to true");
    strictEqual( property(" private a").access, "private", "private can ignore the leading white space");
    strictEqual( property("const private  a").access, "private", "private does not have to be the first keyword");
});

test( "Testing the 'property' function - 'declaration normalisation'", function() {
    // "[final] [const] access [static] name"
    strictEqual(property("a").declaration, "public a");
    strictEqual(property("public a").declaration, "public a");
    strictEqual(property("Public a").declaration, "public a");
    strictEqual(property("PUBLIC a").declaration, "public a");
    strictEqual(property("publiC staTic conST fInal a").declaration, "final const public static a");
    strictEqual(property("public private a").declaration, "public a");
    strictEqual(property("private public a").declaration, "private a");
    strictEqual(property("protected private public a").declaration, "protected a");
});
////////////////////////////////////////////////////////////////////////////////
module("Core");

test( "Testing the 'Class' function - typical use case", function() {
    var MyClass = vjs("class A", {
        a : 1,
        b : function() {
            return 2;
        },
        init : function( name ) {
            this.name = name;
        }
    });
    var myInstance = new MyClass("testName");
    
    strictEqual( myInstance.a, 1, "The properties are readable");
    strictEqual( myInstance.b(), 2, "The methods are callable");
    strictEqual( myInstance.name, "testName", "The constructor gets invoked");
});

test( "Testing the 'Class' function - static properties and methods", function() {
    var MyClass = vjs("class A", {
        "static c" : 3,
        "static d" : function() {
            return 4;
        }
    });
    strictEqual( MyClass.c, 3, "The static properties are readable");
    strictEqual( MyClass.d(), 4, "The static methods are callable");
});

test( "Testing the 'Class' function - private properties and methods", function() {
    var MyClass = vjs("class A", {
        "private a" : 1,
        "private b" : function() {
            return 2;
        },
        "private static c" : 3,
        "private static d" : function() {
            return 4;
        }
    });
    var myInstance = new MyClass();
    
    ok(myInstance.hasOwnProperty("a"), "private properties are attached to the instance");
    ok(myInstance.hasOwnProperty("b"), "private methods are attached to the instance");
    ok(MyClass.hasOwnProperty("c"), "private static properties are attached to class constructor");
    ok(MyClass.hasOwnProperty("d"), "private static methods are attached to the class constructor");
});

test( "Testing the 'Class' function - protected properties and methods", function() {
    var MyClass = vjs("class A", {
        "protected a" : 1,
        "protected b" : function() {
            return 2;
        },
        "protected static c" : 3,
        "protected static d" : function() {
            return 4;
        }
    });
    var myInstance = new MyClass();
    
    ok(myInstance.hasOwnProperty("a"), "protected properties are attached to the instance");
    ok(myInstance.hasOwnProperty("b"), "protected methods are attached to the instance");
    ok(MyClass.hasOwnProperty("c"), "protected static properties are attached to class constructor");
    ok(MyClass.hasOwnProperty("d"), "protected static methods are attached to the class constructor");
});


test( "Testing the 'Class' function - access restrictions", function() {
    var MyClass = vjs("class A", function(SUPER, SELF) {
        return {
            "private privateA" : 1,
            "protected protectedA" : 1,
            "private privateB" : function() {
                return 2;
            },
            "protected protectedB" : function() {
                return 2;
            },
            "private static privateStaticC" : 3,
            "protected static protectedStaticC" : 3,
            "private static privateStaticD" : function() {
                return 4;
            },
            "protected static protectedStaticD" : function() {
                return 4;
            },
            init : function( name ) {
                this.name = name;
            },
            dump : function() {
                for ( var x in this ) {
                    console.log( x, ": ", this[x] );
                }
            },
            access : function(name, value) {
                if (arguments.length > 1) {
                    this[name] = value;
                }
                return this[name];
            },
            "static access" : function(name, value) {
                if (arguments.length > 1) {
                    this[name] = value;
                }
                return this[name];
            },
            testStaticAccess : function() {
                return SELF.privateStaticD(SELF.privateStaticC);
            }
        };
    });
    var myInstance = new MyClass("testName");
    //myInstance.dump();
    
    throws(
        function() { myInstance.privateA; }, 
        /Cannot access/, 
        "Throws an error if trying to read private property"
    );
    throws(
        function() { myInstance.protectedA; }, 
        /Cannot access/, 
        "Throws an error if trying to read protected property"
    );
    throws(
        function() { myInstance.privateA = 2; }, 
        /Cannot access/, 
        "Throws an error if trying to write private property"
    );
    throws(
        function() { myInstance.protectedA = 2; }, 
        /Cannot access/, 
        "Throws an error if trying to write protected property"
    );
    throws(
        function() { myInstance.privateB(); }, 
        /Cannot access/, 
        "Throws an error if trying to call private method"
    );
    throws(
        function() { myInstance.protectedB(); }, 
        /Cannot access/, 
        "Throws an error if trying to call protected method"
    );
    
    // static ------------------------------------------------------------------
    throws(
        function() { MyClass.privateStaticC; }, 
        /Cannot access/, 
        "Throws an error if trying to read private static property"
    );
    throws(
        function() { MyClass.protectedStaticC; }, 
        /Cannot access/, 
        "Throws an error if trying to read protected static property"
    );
    throws(
        function() { MyClass.privateStaticC = 2; }, 
        /Cannot access/, 
        "Throws an error if trying to write private property"
    );
    throws(
        function() { MyClass.protectedStaticC = 2; }, 
        /Cannot access/, 
        "Throws an error if trying to write protected property"
    );
    throws(
        function() { MyClass.privateStaticD(); }, 
        /Cannot access/, 
        "Throws an error if trying to call private static method"
    );
    throws(
        function() { MyClass.protectedStaticD(); }, 
        /Cannot access/, 
        "Throws an error if trying to call protected static method"
    );
    
    ok(myInstance.access("privateA") === 1, "Private properties are readable through methods");
    ok(myInstance.access("protectedA") === 1, "Protected properties are readable through methods");
    ok(MyClass.access("privateStaticC") === 3, "Private static properties are readable through static methods");
    ok(MyClass.access("protectedStaticC") === 3, "Protected static properties are readable through static methods");
    
    ok(myInstance.access("privateA", 2) === 2, "Private properties are writeable through methods");
    ok(myInstance.access("protectedA", 2) === 2, "Protected properties are writeable through methods");
    ok(MyClass.access("privateStaticC", 4) === 4, "Private static properties are writeable through methods");
    ok(MyClass.access("protectedStaticC", 4) === 4, "Protected static properties are writeable through methods");
    
    ok(myInstance.access("privateB")() === 2, "Private methods are callable through other methods");
    ok(myInstance.access("protectedB")() === 2, "Protected methods are callable through other methods");
    ok(MyClass.access("privateStaticD")() === 4, "Private static methods are callable through other static methods");
    ok(MyClass.access("protectedStaticD")() === 4, "Protected static methods are callable through other static methods");
    
    ok(myInstance.testStaticAccess() === 4, "Protected static properties and methods are accessible via public instance methods");
});

// =============================================================================
// final
// =============================================================================
(function() {
    module("final");
    
    test("Testing the 'final' keyword parsing", function() {
        strictEqual( property("a").final, false, "final defaults to false");
        strictEqual( property("final a").final, true, "final can be set to true");
        strictEqual( property(" final a").final, true, "final can ignore the leading white space");
        strictEqual( property("private final a").final, true, "final does not have to be the first keyword");
    });
    
    test("Testing the 'final' keyword parsing for class declarations", function() {
        strictEqual( !(new _class("class A").isFinal), true, "Classes are not final by default");
        strictEqual( new _class("final class A").isFinal, true, "final can be set to true");
        strictEqual( new _class(" final class A").isFinal, true, "final can ignore the leading white space");
        strictEqual( new _class("abstract final class A").isFinal, true, "final does not have to be the first keyword");
    });
    
    var NormalBase = vjs("class NormalBase", {
        "final a" : 1,
        "final static b" : 2,
        "final c" : function() {},
        "final static d" : function() {}
    });
    
    var FinalBase = vjs("final class FinalBase", {});
    
    test("The final instance properties cannot be overridden", function() {
        throws(
            function() {
                vjs("class B extends NormalBase", {
                    "a" : 2
                });
            },
            Error
        );
    });
    
    test("The final static properties cannot be overridden", function() {
        throws(
            function() {
                vjs("class B extends NormalBase", {
                    "static b" : 3
                });
            },
            Error
        );
    });
    
    test("The final instance methods cannot be overridden", function() {
        throws(
            function() {
                vjs("class B extends NormalBase", {
                    "c" : function() {}
                });
            },
            Error
        );
    });
    
    test("The final static methods cannot be overridden", function() {
        throws(
            function() {
                vjs("class B extends NormalBase", {
                    "static d" : function() {}
                });
            },
            Error
        );
    });
    
    test("The final classes cannot be extended", function() {
        throws(
            function() {
                vjs("class B extends FinalBase", {});
            },
            Error
        ); 
    });
    
})();

// =============================================================================
// const
// =============================================================================
(function() {
    module("const");

    test( "Testing the 'const' keyword parsing", function() {
        strictEqual( property("a").const, false, "const defaults to false");
        strictEqual( property("const a").const, true, "const can be set to true");
        strictEqual( property(" const a").const, true, "const can ignore the leading white space");
        strictEqual( property("private const a").const, true, "const does not have to be the first keyword");
    });

    var MyClass = vjs("class A", function(__self__) {
        return {
            "const a" : 1,
            "const b" : function() {
                return 2;
            },
            "const static c" : 3,
            "const static d" : function() {
                return 4;
            },
            "const static e" : 5,
            "const f" : 5,
            "const g" : undefined
        };
    });
    var myInstance = new MyClass();
    
    test("const properties are attached to the instance", function() {
        ok(myInstance.hasOwnProperty("a"));
    });
    test("const methods are attached to the instance", function() {
        ok(myInstance.hasOwnProperty("b"));
    });
    test("const static properties are attached to class constructor", function() {
        ok(MyClass.hasOwnProperty("c"));
    });
    test("const static methods are attached to the class constructor", function() {
        ok(MyClass.hasOwnProperty("d"));
    });
    test("const properties cannot be changed", function() {
        throws(
            function() {
                myInstance.a = 2;
            },
            /Cannot change the value of the constant/
        );
    });
    test("const methods cannot be changed", function() {
        throws(
            function() {
                myInstance.b = 2;
            },
            /Cannot change the value of the constant/
        );
    });
    test("static const properties cannot be changed", function() {
        throws(
            function() {
                MyClass.c = 2;
            },
            /Cannot change the value of the constant/
        );
    });
    test("static const methods cannot be changed", function() {
        throws(
            function() {
                MyClass.d = 2;
            },
            /Cannot change the value of the constant/
        );
    });
    test("const properties can be changed only once if initialised as undefined", function() {
        ok(myInstance.g === undefined);
        ok((myInstance.g = 2));
        ok(myInstance.g === 2);
        throws(
            function() {
                myInstance.g = 3;
            },
            /Cannot change the value of the constant/
        );
        ok(myInstance.g === 2);
    });
})();


////////////////////////////////////////////////////////////////////////////////
module("Inheritance");

test( "Testing the simple inheritance", function() {
    var Class1 = vjs("class A", { a : 1, b : 2 });
    var Class2 = vjs("class B extends A", { b : 3, c : 4 });
    var inst1 = new Class1();
    var inst2 = new Class2();
    ok(inst1.a === 1);
    ok(inst1.b === 2);
    ok(inst2.a === 1);
    ok(inst2.b === 3);
    ok(inst2.c === 4);
});

test( "Testing the public/protected/private inheritance", function() {
    var Class1 = vjs("class A", { 
        "public a" : 1, 
        "protected b" : 2,
        "private c" : 3, 
        "protected func" : function() { return true; },
        access : function(name, value) {
            if (arguments.length > 1) {
                this[name] = value;
            }
            return this[name];
        },
        "static access" : function(name, value) {
            if (arguments.length > 1) {
                this[name] = value;
            }
            return this[name];
        }
    });
    var Class2 = vjs("class B extends A", {
        callProtected : function() {
            return this.func();
        }
    });
    var inst1 = new Class1();
    var inst2 = new Class2();
    
    ok(inst2.access("a") === 1, "The public properties must be inherited");
    ok(inst2.access("b") === 2, "The protected properties must be inherited");
    ok(inst2.access("c") === undefined, "The private properties must NOT be inherited");
    ok(inst2.callProtected(), "Protected methods must be callable from the subclass");
    throws(
        function() {
            inst2.trigger("e");
        },
        Error,
        "The protected methods are inaccessible again, after they have been called"
    );
});



test( "Testing the abstract properties validation", function() {
    throws(
        function() {
            vjs("class A", {
                "abstract a" : 2
            });
        },
        Error,
        "An abstract property must require it's class to be declared as abstract too"
    );
    throws(
        function() {
            vjs("class A", {
                "abstract a" : function() { return 2; }
            });
        },
        Error,
        "An abstract method must require it's class to be declared as abstract too"
    );
    throws(
        function() {
            var AbstractClass = vjs("abstract class A", {
                "a" : 2
            });
            new AbstractClass();
        },
        Error,
        "Trying to create an instance of abstract class must throw an error"
    );
    throws(
        function() {
            vjs("abstract class A", {
                "abstract private a" : 2
            });
        },
        Error,
        "The abstract properties and methods cannot be private"
    );
    throws(
        function() {
            vjs("abstract class A", {
                "abstract final a" : 2
            });
        },
        Error,
        "The abstract properties and methods cannot be final"
    );
    throws(
        function() {
            var AbstractClass = vjs("abstract class A", {
                "abstract a" : 2
            });
            var Subclass = vjs("class B extends A", {
                b : 3
            });
        },
        Error,
        "The subclasses must be forced to implement all the abstract properties " +
        "and methods from their superclass"
    );
    ok(
        (function() {
            var AbstractClass = vjs("abstract class A", {
                a : 2
            });
            var Subclass = vjs("class B extends A", {
                b : 3
            });
            var inst = new Subclass();
            return inst.a == 2 && inst.b == 3;
        })(),
        "The subclasses can inherit from abstract classes without abstract properties"
    );
});

////////////////////////////////////////////////////////////////////////////////
test( "BIG REAL LIFE EXAMPLE", function() {
    
    /**
     * Class Event - the base class for custom events
     */
    var Event = vjs("class Event", {
        
        /**
         * The event type is public so anyone can read it but you don't have to 
         * worry. It is a "const" (read only) property, so once initialized by 
         * the constructor, no one can modify it.
         */
        "const type" : undefined,

        /**
         * Private flag used internally to store the "PropagationStopped" state 
         * of the event.
         */
        "private _isPropagationStopped" : false,

        /**
         * That is how the outer world can check if the event propagation has 
         * been stopped
         */
        "public isPropagationStopped" : function() {
            return this._isPropagationStopped;
        },

        /**
         * Just sets the private "_isPropagationStopped" flag to true.
         */
        "public stopPropagation" : function() {
            this._isPropagationStopped= true;
        },

        /**
         * The constructor just sets the event type. Note that once the type 
         * property is set (to something other than undefined), it gets locked 
         * and nothing can change it and introduce weird bugs that are very 
         * difficult to find...
         */
        "init" : function(type) {
            this.type = type;
        }
    });
    
    var Observable = vjs("abstract class Observable", {
        
        /**
         * Contains all the listeners grouped by the evntType that they are 
         * attached to.
         */
        "protected listeners" : {},
        
        /**
         * The method used to attach event listeners.
         * @param {String} eventType The type of the event to listen to.
         * @param {Function} callback The event handler function
         * @return {Observable} Return this instance for chaining
         */
        "public bind" : function( eventType, callback ) {
            var set = this.listeners[ eventType ];
            if ( !set ) {
                this.listeners[ eventType ] = [ callback ];
            } else {
                this.listeners[ eventType ].push( callback );
            }
            return this;
        },
        
        /**
         * Unbinds event listener(s).
         * If no arguments are provided, all of the event listeners are removed 
         * from the target (the observable instance). If only the eventType is 
         * provided, than all the listeners for that event are removed. 
         * If both arguments are provided, than ONLY that listener for that 
         * eventType will be removed.
         * @param {String} eventType The type of the event to listen to.
         * @param {Function} callback The event handler function
         */
        "public unbind" : function( eventType, callback ) {
            if (!eventType) {
                this.listeners = {};
            } else if (!callback) {
                this.listeners[eventType] = [];
            } else {
                var set = this.listeners[ eventType ];
                if (set) {
                    for ( var i = 0, l = set.length; i < l; i++ ) {
                        if (set[i] === callback) {
                            set.splice(i--, 1);
                        }
                    }
                }
            }
            return this;
        },
        
        /**
         * Dispatches an event on the target.
         * @param {String} eventType The type of the event to be dispatched. 
         *                 Note that the eventType argument will be used to 
         *                 create new Event instance that will be passed to 
         *                 the listeners as their first argument.
         * @return {Boolean} True if the event has not been cancelled (i.e. the 
         *                   stopPropagation method has not been called from 
         *                   any of the listeners), false otherwise.
         */
        "protected trigger" : function( eventType ) {
            var event = new Event(eventType),
                set = this.listeners[ eventType ];
            if ( set ) {
                for ( var i = 0, l = set.length; i < l; i++ ) {
                    set[i].call(this, event);
                    if (event.isPropagationStopped()) {
                        break;
                    }
                }
            }
            return !event.isPropagationStopped();
        }
    });
    
    var Widget = vjs("class Widget extends Observable", {
        "public clicks" : 0,
        "public init" : function() {
            //console.log(this.trigger);
            //debugger;
            
            this.bind("click", function( event ) {
                //console.log( event );
                this.clicks++;
            });
            
            this.trigger("click");
        }
    });
    
    /**
     * Class ButtonBase - the abstract base class for all the buttons
     */
    vjs("abstract class ButtonBase", {
        
        /**
         * The hash of attributes that are to be applied on the button.
         * @type {Object}
         */
        "protected attributes" : {},
        
        /**
         * Force the child classes to implement the getContents method that 
         * will be used to return the content of the buttons. Not that this 
         * will also force the method to be implemented as public or protected.
         */
        "abstract protected getContents" : function() {},
        
        /**
         * Provide the getHTML method to all the subclasses
         */
        "public getHTML" : function() {
            var html = '<button', attrName;
            for (attrName in this.attributes) {
                html += ' ' + attrName + '="' + this.attributes[attrName] + '"';
            }
            html += '>' + this.getContents() + '</button>';
            return html;
        }
    });
    
    /**
     * Class Button
     */
    var Button = vjs("class Button extends ButtonBase", {
        "protected text" : "",
        getText : function() {
            return this.text;
        },
        setText : function(text) {
            this.text = String(text || "");
        },
        init : function(text) {
            if (text) {
                this.setText(text);
            }
        },
        getContents : function() {
            return this.getText();
        }
    });
    
    /**
     * Class ImageButton
     */
    var ImageButton = vjs(
        "class ImageButton extends Button", 
        function(_super) {
            return {
                "protected imageUrl" : "",
                getImageUrl : function() {
                    return this.imageUrl;
                },
                setImageUrl : function(url) {
                    this.imageUrl = String(url || "");
                },
                init : function(text, url) {
                    //console.log("_super: ", this._super);
                    _super(this, "init", text);
                    if (url) {
                        this.setImageUrl(url);
                    }
                },
                getContents : function() {
                    return '<img src="' + this.getImageUrl() + '"> ' + _super(this, "getContents");
                }
            };
        }
    );
    
    var my = new ImageButton("Click Me!", "image.png");
    //console.log("my.getHTML(): ", my.getHTML());
    var checkbox = new Widget();
    ok(checkbox instanceof Widget);
    ok(checkbox instanceof Observable);
    ok(checkbox.clicks === 1);
});

// =============================================================================
// Widget Factory
// =============================================================================
test( "Widget Factory", function() {
    var Widget = vjs("abstract class Widget", {});
    
    var Checkbox = vjs(
        "class Input extends Widget", 
        function(_super, SELF) { 
            return {
                "static const STATE_UNDETERMINED" : 0,
                "static const STATE_UNCHECKED"    : 1,
                "static const STATE_CHECKED"      : 2,
                
                "protected static cssClassMap" : [
                    "undetermined",
                    "unchecked",
                    "checked"
                ],
                
                "public element" : {
                    toggleClass : function() {}
                },
                
                "public setState" : function( state ) {
                    switch (state) {
                        case SELF.STATE_CHECKED:
                        case SELF.STATE_UNCHECKED:
                        case SELF.STATE_UNDETERMINED:
                            SELF.cssClassMap.forEach(function(c, i) {
                                this.element.toggleClass(c, i === state);
                            }, this);
                            break;
                        default:
                            throw "Invalid argument";
                    }
                }
            };
        }
    );
    
    var Input = vjs(
        "class Input extends Widget", 
        function(_super) { 
            return {
                "protected input" : null,
                
                "public init" : function(input) {
                    this.input = input;
                },
                
                "public setValue" : function(v) {
                    this.input.value = this.format(v);
                },
                
                "public getValue" : function(v) {
                    return this.input.value;
                },
                
                "public format" : function(v) {
                    return v;
                }
            }; 
        }
    );
    
    var NumericInput = vjs(
        "class NumericInput extends Input", 
        function(_super) { 
            return {
                "protected valueAsNumber" : 0,
                "protected min"           : -Infinity,
                "protected max"           :  Infinity,
                
                "public init" : function(input) {
                    console.log("NumericInput.init");
                    _super(this, "init", input);
                },
                
                "public setValueAsNumber" : function(n) {
                    n = parseFloat(n);
                    if ( !isNaN(n) && isFinite(n) ) {
                        this.valueAsNumber = n;
                        this.setValue(n);
                    }
                    return this;
                },
                
                "public setValueAsString" : function(n) {
                    this.setValueAsNumber(n);
                    return this;
                },
                
                "public getValueAsNumber" : function() {
                    return this.valueAsNumber;
                }
            };
        }
    );
    
    var StepInput = vjs(
        "class StepInput extends NumericInput", 
        function(_super) {
            return {
                "protected step" : 1,
                
                "public init" : function(input) {
                    console.log("StepInput.init");
                    _super(this, "init", input);
                },
                
                "public increase" : function( steps ) {
                    this.setValueAsNumber(
                        this.getValueAsNumber() + this.step * (steps || 1)
                    );
                },
                
                "public decrease" : function( steps ) {
                    this.setValueAsNumber(
                        this.getValueAsNumber() - this.step * (steps || 1)
                    );
                },
                
                "public getInput" : function() { return this.input; }
            };
        }
    );
    
    var input = document.createElement("input");
    input.type = "text";
    var stepInpput1 = new StepInput(input);
    
    stepInpput1.format = function(v) {
        return v + "cm";
    };
    
    // Tests
    // -------------------------------------------------------------------------
    ok(
        stepInpput1 instanceof Widget, 
        "'instanceof' returns true for the base class"
    );
    
    strictEqual(
        stepInpput1.getInput(), 
        input, 
        "'_super' can walk up the inheritance chain"
    );
    
    stepInpput1.setValueAsNumber(43.5);
    strictEqual(
        stepInpput1.getValueAsNumber(),
        43.5,
        "Inherited methods (setValueAsNumber and getValueAsNumber) are working correctly"
    );
    
    stepInpput1.increase();
    strictEqual(
        stepInpput1.getValueAsNumber(),
        44.5,
        "The 'increase' method is working correctly"
    );
    
    stepInpput1.increase(3);
    strictEqual(
        stepInpput1.getValueAsNumber(),
        47.5,
        "The 'increase' method is working correctly with argument"
    );
    
    stepInpput1.decrease();
    strictEqual(
        stepInpput1.getValueAsNumber(),
        46.5,
        "The 'decrease' method is working correctly"
    );
    
    stepInpput1.decrease(3);
    strictEqual(
        stepInpput1.getValueAsNumber(),
        43.5,
        "The 'decrease' method is working correctly with argument"
    );
    
    strictEqual(
        stepInpput1.getValue(),
        "43.5cm",
        "Replaced public method works correctly"
    );
    
    var cb = new Checkbox;
    cb.setState(Checkbox.STATE_CHECKED);
    //alert(Widget.TYPE_FORM_CONTROL);
});

// Namespaces
// =============================================================================
module("namespaces");
var global = window;

test("Class in the global namespace", function() {
    var path = "GlobalClass",
        c    = vjs("class " + path),
        cInt = vjs.getClass(path);
    
    strictEqual(global[path], c, "The class is stored in {global}." + path);
    ok(!!cInt, "The class object is stored internally");
    strictEqual(cInt.path, path, "The class path is recognised correctly.");
    strictEqual(cInt.name, path, "The class name is recognised correctly.");
});

test("Namespaced class (1 level)", function() {
    var name = "Class1",
        ns   = "MyNS",
        path = ns + "." + name,
        c    = vjs("class " + path),
        cInt = vjs.getClass(path);
        
    strictEqual(global[ns][name], c, "The class is stored in {global}." + path);
    ok(!!cInt, "The class object is stored internally");
    strictEqual(cInt.path, path, "The class path is recognised correctly.");
    strictEqual(cInt.name, name, "The class name is recognised correctly.");
});

test("Namespaced class (2 levels)", function() {
    var name = "Class1",
        ns1  = "MyNS2",
        ns2  = "MySubNS",
        path = ns1 + "." + ns2 + "." + name,
        c    = vjs("class " + path),
        cInt = vjs.getClass(path);
        
    strictEqual(global[ns1][ns2][name], c, "The class is stored in {global}." + path);
    ok(!!cInt, "The class object is stored internally");
    strictEqual(cInt.path, path, "The class path is recognised correctly.");
    strictEqual(cInt.name, name, "The class name is recognised correctly.");
});

test("Inherit from namespaced class", function() {
    expect(1);
    try {
        vjs("class MyNS2.MySubNS.Class1");
        vjs("class TestClass extends MyNS2.MySubNS.Class1");
    } catch( ex ) {
        console.error(ex.message);
        return;
    }
    ok(vjs.getClass("TestClass"), "The parent class at MyNS2.MySubNS.Class1 was found and used");
});

test("Paths in case of custom namespace root", function() {
    var sandbox = {};
    vjs.config.namespace = sandbox;
    
    var Observable = vjs("class MyUniqueLib.Observable", {});
    var Widget     = vjs("class MyUniqueLib.Widget extends MyUniqueLib.Observable", {});
    
    ok(
        Observable === sandbox.MyUniqueLib.Observable && Widget === sandbox.MyUniqueLib.Widget,
        "The classes are created inside the customized NS root"
    );
    
    ok(
        !global.MyUniqueLib || (!MyUniqueLib.Observable && !MyUniqueLib.Widget),
        "The classes are NOT created inside the global object"
    );
});



