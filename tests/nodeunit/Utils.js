// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

var lib = require("../../build/vjs-lib.test.js");

/**
 * Tests for the "trim" function
 */
exports.trim = function( test ) {

    // Make sure it works on both sides
    test.equal("abc", lib.trim(" abc  "), "Typical usecase");

    // If the string is made of white spaces only, it should result in an empty string
    test.strictEqual(
        "",
        lib.trim("    "),
        "A string containing only \\s and \\t " +
        "gets trimmed to an empty string"
    );

    // The rsult of trimming a numeric string must also be a string (and not a number)
    test.strictEqual(
        "56",
        lib.trim(56),
        "Converts number arg to string"
    );

    test.done();
};

/**
 * Tests for the "is" function
 */
exports.is = function(test) {
    var is = lib.is;

    // functions
    test.ok( is(function(){}, "function"), "function" );
    test.ok( is(new Function(), "function"), "Function object" );

    // strings
    test.ok( is("", "string"), "empty string" );
    test.ok( is("test", "string"), "not-empty string" );
    test.ok( is(new String("test"), "string"), "string object" );

    // arrays
    test.ok( is([], "array"), "Empty array literal" );
    test.ok( is(new Array(), "array"), "Empty array object" );
    test.ok( !is(arguments, "array"), "Array-like object" );

    // numbers
    test.ok( is(0, "number"), "number literal" );
    test.ok( is(new Number(0), "number"), "number object" );
    test.ok( is(0x01, "number"), "number as hex literal" );
    test.ok( is(0123, "number"), "octal number" );
    test.ok( is(Number.MAX_VALUE, "number"), "Number.MAX_VALUE" );
    test.ok( is(Number.MIN_VALUE, "number"), "Number.MIN_VALUE" );
    test.ok( !is(Number.NaN, "number"), "Number.NaN is NOT considered a number");
    test.ok( !is(Infinity, "number"), "Infinity is NOT considered a number" );
    test.ok( !is(-Infinity, "number"), "-Infinity is NOT considered a number" );

    // Date
    test.ok( is(new Date(), "date"), "date" );

    // RegExp
    test.ok( is(/\s/, "regexp"), "regexp literal" );
    test.ok( is(new RegExp("\\s"), "regexp"), "regexp object" );

    // booleans
    test.ok( is(true, "boolean"), "boolean true" );
    test.ok( is(false, "boolean"), "boolean false" );
    test.ok( is(new Boolean(1), "boolean"), "boolean instance (true)" );
    test.ok( is(new Boolean(0), "boolean"), "boolean instance (false)" );

    // Object
    test.ok( is({}, "object"), "object literal" );
    test.ok( is(new Object(), "object"), "object instance" );
    test.ok( !is(null, "object"), "null is NOT an object here" );

    // custom objects
    function Car() {};
    test.ok( is(new Car(), "object"), "custom object is object" );
    test.ok( is(new Car(), Car), "custom object is instance of it's constructor" );
    test.ok( is(new Car(), Object), "custom object is instance of Object" );

    test.done();
};

/**
 * Tests for the "jPath" function in GET (read) mode
 */
exports.jPath_GET = function(test) {
    var o = {
        a : 1,
        b : 3,
        d : { e : { f : 5, a : [1,2,3,4] } }
    }, jPath = lib.jPath;

    test.equal(jPath(o, "a"       ), 1, "Failed to read from  first level");
    test.equal(jPath(o, "d.e.f"   ), 5, "Failed to read deeper level");
    test.equal(jPath(o, "d.e.a[2]"), 3, "Failed to read array by index");
    test.equal(jPath(o, "d.e.a.2" ), 3, "Failed to read array by index");
    test.equal(jPath(o, "c"       ), undefined);
    test.done();
};

/**
 * Tests for the "jPath" function in SET (write) mode
 */
exports.jPath_SET = function( test ) {
    var o = {
        a : 1,
        b : 3,
        d : { e : { f : 5, a : [1,2,3,4] } }
    }, jPath = lib.jPath;

    jPath(o, "a", 4);
    jPath(o, "c", 6);
    jPath(o, "d.e.f", 6);
    jPath(o, "d.e.a[0]", "a");
    jPath(o, "d.e.a.1", "b");
    test.equal(o.a, 4, "Update field");
    test.equal(o.c, 6, "Add field");
    test.equal(o.d.e.f, 6, "Update deeper field");
    test.equal(o.d.e.a[0], "a", "Update array by index");
    test.equal(o.d.e.a[1], "b", "Update array by index");
    test.done();
};

/**
 * Tests for the "jPath" function in DELETE mode
 */
exports.jPath_DELETE = function( test ) {
    var o = {
        a : 1,
        b : {
            c : 6
        },
        d : [1,2,3]
    }, jPath = lib.jPath;

    jPath(o, "a"  , undefined);
    jPath(o, "b.c", undefined);
    jPath(o, "d.1", undefined);

    test.ok( !o  .hasOwnProperty("a"), "Failed to delete on first level");
    test.ok( !o.b.hasOwnProperty("c"), "Failed to delete on deeper level");
    test.deepEqual(o.d, [1,3], "Failed to delete array item by index");
    test.done();
};

/**
 * Tests for the "jPath" - intermediate paths creation. This means that when
 * used in write mode, it should create the intermediate path segments in case
 * they are missing ...
 */
exports.jPath_intermediate_path_creation = function( test ) {
    var o = { a : {} }, jPath = lib.jPath;

    jPath(o, "a.b.c", 5);
    test.deepEqual(o, {a:{b:{c:5}}}, "Failed to create intermediate objects");

    jPath(o, "a.d.3", 5);
    test.deepEqual(
    o,
    {
        a : {
            b : {
                c : 5
            },
            d : [, , , 5]
        }
    }, "Failes to create intermediate arrays");
    test.done();
};

/**
 * Tests for the "property" function
 */
exports.property = function ( test ) {

    var property = lib.property;

    // typical use cases
    // ------------------------------------------------------------------------
    test.strictEqual( property("a").value, undefined, "The value defaults to undefined" );
    test.strictEqual( property("a").name, "a", "The name gets recognized");
    test.strictEqual( property("a").access, "public", "The value defaults to public" );
    test.strictEqual( property("a").final, false, "final defaults to false");
    test.strictEqual( property("a").static, false, "static defaults to false");
    test.strictEqual( property("a").const, false, "const defaults to false");
    test.strictEqual( property("a", 5).value, 5, "The value gets recognized");

    // arguments validation
    // ------------------------------------------------------------------------
    lib.throwsVJSErrorCode(
        test,
        function() { property(); },
        101,
        "Doesn't throw an error if the declaration is empty"
    );

    lib.throwsVJSErrorCode(
        test,
        function() { property("static"); },
        102,
        "Doesn't throw an error if the name is a resrved word"
    );

    lib.throwsVJSErrorCode(
        test,
        function() { property("something static big"); },
        103,
        "Doesn't throw exceptions on unrecognised declarations"
    );


    // static
    // ------------------------------------------------------------------------
    test.strictEqual( property("a").static, false, "static defaults to false");
    test.strictEqual( property("static a").static, true, "static can be set to true");
    test.strictEqual( property(" static a").static, true, "static can ignore the leading white space");
    test.strictEqual( property("private static a").static, true, "static does not have to be the first keyword");

    // public
    // ------------------------------------------------------------------------
    test.strictEqual( property("a").access, "public", "public defaults to true");
    test.strictEqual( property("public a").access, "public", "public can be set to false");
    test.strictEqual( property(" public a").access, "public", "public can ignore the leading white space");
    test.strictEqual( property("const public  a").access, "public", "public does not have to be the first keyword");

    // protected
    // ------------------------------------------------------------------------
    test.strictEqual( property("a").access, "public", "protected defaults to false");
    test.strictEqual( property("protected a").access, "protected", "protected can be set to true");
    test.strictEqual( property(" protected a").access, "protected", "protected can ignore the leading white space");
    test.strictEqual( property("const protected  a").access, "protected", "protected does not have to be the first keyword");

    // private
    // ------------------------------------------------------------------------
    test.strictEqual( property("a").access, "public", "private defaults to false");
    test.strictEqual( property("private a").access, "private", "private can be set to true");
    test.strictEqual( property(" private a").access, "private", "private can ignore the leading white space");
    test.strictEqual( property("const private  a").access, "private", "private does not have to be the first keyword");

    // declaration normalisation
    // ------------------------------------------------------------------------
    // "[final] [const] access [static] name"
    test.strictEqual(property("a").declaration, "public a");
    test.strictEqual(property("public a").declaration, "public a");
    test.strictEqual(property("Public a").declaration, "public a");
    test.strictEqual(property("PUBLIC a").declaration, "public a");
    test.strictEqual(property("publiC staTic conST fInal a").declaration, "final const public static a");
    test.strictEqual(property("public private a").declaration, "public a");
    test.strictEqual(property("private public a").declaration, "private a");
    test.strictEqual(property("protected private public a").declaration, "protected a");


    test.done();
};

/**
 * Tests for the "printf" function
 */
exports.printf = function( test ) {
    test.equal(lib.printf("a%db", 5.4 ), "a5b"  , "Failed to handle %d");
    test.equal(lib.printf("a%ib", 5.4 ), "a5b"  , "Failed to handle %i");
    test.equal(lib.printf("a%xb", -5.4), "a5b"  , "Failed to handle %x");
    test.equal(lib.printf("a%Xb", -15 ), "aFb"  , "Failed to handle %X");
    //test.equal(lib.printf("a%4.2fb", Math.PI ), "a3.14b", "Failed to handle %f");
    //test.equal(lib.printf("a%4.2Fb", Math.PI ), "a3.14b", "Failed to handle %F");
    test.equal(lib.printf("a%cb", 5.4 ), "a5.4b", "Failed to handle %c");
    test.equal(lib.printf("a%sb", 5.4 ), "a5.4b", "Failed to handle %s");
    test.equal(lib.printf("a%%b", 5.4 ), "a%b"  , "Failed to handle %%");
    test.equal(lib.printf("a%Nb", 5   ), "a%Nb" , "Failed to handle %char");
    test.done();
};

// TODO: Tests for the "createMethod" function
// TODO: Tests for the "createProperty" function

