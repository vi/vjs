var lib = require("../../build/vjs-lib.test.js");

// XXX: Those are specific tests! If the error messages or error codes are
// changed in constants.js, then some of those test sould might have to be
// modified!
exports.VJSError = function( test ) {
    var VJSError = lib.VJSError,
        errors   = lib.errors;

    test.strictEqual(
        errors[0],
        (new VJSError()).message,
        "The error message does not fail back to the default one if the " +
        "code is not provided"
    );

    test.strictEqual(
        0,
        (new VJSError()).code,
        "The error code must default to 0"
    );

    test.strictEqual(
        errors[100],
        (new VJSError(100)).message,
        "Failed to pick the message by code"
    );

    test.equal(
        "No such class 'test'.",
        (new VJSError(202, "test")).message,
        "Failed to substitute parameter in message"
    );

    test.equal(
        "The property 'A' is abstract, therefore the class 'B' must " +
        "also be defined as abstract",
        (new VJSError(210, "A", "B")).message,
        "Failed to substitute parameters in message"
    );

    test.done();
};
