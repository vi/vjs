// Source: src/header.js
(function( NS, undefined ) {
    "use strict";


// Source: src/constants.js
var reSpaces       = /\s+/,
    reClass        = /\bclass\b/,
    reExtends      = /\bextends\s+(.+?)(?:\s+implements|$)/,
    reImplements   = /\bimplements\s+(.+?)(?:\s+extends|$)/,
    reWsOrEof      = /(\s.*|$)/,
    reCommaSplit   = /\s*,\s*/,
    reSpaceTrim    = /^\s*|\s*$/,
    reJsPath       = /\[['"]?([^\]]+)['"]?\]/g,
    rePrintfTokens = /%(\.(\d))?([a-zA-Z%])/g,
    defineProperty = Object.defineProperty,
    toString       = Object.prototype.toString,
    reservedWords  = {
        "public"    : 1,
        "private"   : 1,
        "protected" : 1,
        "const"     : 1,
        "static"    : 1,
        "final"     : 1,
        "abstract"  : 1
    },
    errors = {
        // General message for unknown error
        0   : "Unknown VJS error",

        // 100-199 - Parse errors
        100 : "Invalid property declaration.",
        101 : "Invalid (empty) property declaration.",
        102 : "Invalid property name '%s' (reserved word).",
        103 : "Invalid property declaration. Unrecognised token '%s'.",
        104 : "Invalid property declaration. Missing name.",
        105 : "Unrecognised class modifier '%s'",
        106 : "Invalid class properties declaration",
        107 : "Invalid class declaration '%s'. Missing class name or path.",

        // 200-299 Runtime errors
        201 : "Cannot access the property '%s' from out of scope",
        202 : "No such class '%s'.",
        203 : "Cannot change the value of the constant '%s' property of class '%s'.",
        204 : "Cannot inherit from undefined class '%s'.",
        205 : "Cannot inherit from class '%s'",
        206 : "Cannot inherit from final class '%s'",
        207 : "Cannot create an instance of the abstract class '%s'.",
        208 : "The abstract property or method '%s' cannot be private",
        209 : "The abstract property or method '%s' cannot be final",
        210 : "The property '%s' is abstract, therefore the class '%s' must " +
              "also be defined as abstract",
        211 : "The final property or method '%s' cannot be overridden",
        212 : "The class '%s' must implement the '%s' property or method"

    };
    

// Source: src/utils.js
// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

/**
 * Just an empty function for whatever we might need it.
 * @return void
 */
function noop() {}

/**
 * Trims the white space on the both sides of a string. Delegates
 * to the native trim if it exists.
 * @param {String} str The string to trim
 * @return String
 */
function trim(str) {
    return str.trim ?
        str.trim() :
        String(str).replace(reSpaceTrim, "");
}

/**
 * Checks if @x is of type @type. Typically it will use the
 * Object.ptptotype.toString approach so that we can work with objects from
 * different origins (frames). For custom objects wit will use the "instanceof"
 * operator and for numbers it should return false if the @x argument is NaN
 * or Infinity.
 * @param x The "thing" to check the type of
 * @param {String|Function} type Can be a case-insensitive string type or a
 * reference to the desired constructor function.
 * @returns {Boolean}
 */
function is(x, type) {

    var _type = String(type).toLowerCase(),
        xType = toString.call(x).toLowerCase();

    // Built-in objects (except for Number)
    if (_type === "object"   ||
        _type === "function" ||
        _type === "array"    ||
        _type === "string"   ||
        _type === "boolean"  ||
        _type === "date"     ||
        _type === "regexp")
    {
        return xType === "[object " + type + "]";
    }

    // Strict number (return false for NaN and Infinty)
    if (_type === "number") {
       return xType === "[object " + type + "]" && !isNaN(x) && isFinite(x);
    }

    // Custom objects - use instanceof
    if (xType === "[object object]" &&
        toString.call(type) === "[object Function]")
    {
        return (x instanceof type);
    }

    // Everything else - just use simple strict comparison
    return x === type;
}

/**
 * Navigates to the @path inside @obj and returns the value or updates it if
 * called with 3 arguments.
 * @param {Object} obj The object to use
 * @param {String} path
 * @param {*} value (optional)
 */
function jPath(obj, path, value) {
    var segments = path.replace(reJsPath, ".$1").split("."),
        l        = segments.length,
        curPath  = [],
        modeGET  = 2,
        modeSET  = 3,
        modeDEL  = 4,
        mode     = arguments.length === 3 ?
            value === undefined ?
                modeDEL :
                modeSET :
            modeGET,
        cur      = obj,
        isArray,
        name,
        next,
        i;

    for ( i = 0; i < l; i++ ) {
        curPath[i] = name = segments[i];
        isArray = is(cur, "array");

        if (isArray) {
            name = parseInt(name, 10);
        }

        if ( i === l - 1 ) { // last
            if ( mode === modeDEL ) {
                if ( isArray ) {
                    cur.splice(name, 1);
                }
                else if ( cur.hasOwnProperty(name) ) {
                    delete cur[name];
                }
            }
            else if ( mode === modeSET ) {
                cur[name] = value;
            }
            return cur[name];
        }

        if (!cur.hasOwnProperty(name)) {

            // Called to read, but an intermediate path segment was not
            // found - return undefined
            if ( mode === modeGET ) {
                return undefined;
            }

            // Called to write, but an intermediate path segment was not
            // found - create it and continue
            next = segments[ i + 1 ];
            cur[name] = isNaN( parseFloat(next) ) ||
                String(parseFloat(next)) !== String(next) ? {} : [];
        }

        cur = cur[name];
    }
}

/**
 * Creates a property object from the given declaration and value
 * @param {String} declaration The property declaration
 * @param {*} value The property value
 * @return {Object} The property object
 * @throws {Error} Upon validation failure
 */
function property( declaration, value ) {
    var arr = trim(String(declaration || "")).split(reSpaces),
        out = {
            "access"   : "public",
            "const"    : false,
            "static"   : false,
            "final"    : false,
            "abstract" : false,
            "value"    : value
        },
        len = arr.length,
        decl = [],
        name,
        mod,
        modLower;

    // Empty declaration
    if (!arr[0]) {
        throw new VJSError(101);
    }

    name = arr[--len];

    // missing name
    if (!name) {
        throw new VJSError(104);
    }

    // name is a reserved word
    if (name in reservedWords) {
        throw new VJSError(102, name);
    }

    out.name = name;

    while ( len ) {
        mod = arr[--len];
        modLower = mod.toLowerCase();
        switch (modLower) {
            case "public":
            case "private":
            case "protected":
                out.access = modLower;
            break;
            case "const":
            case "static":
            case "final":
            case "abstract":
                out[modLower] = true;
            break;
            default:
                throw new VJSError(103, mod);
        }
    }

    // Check for invalid combinations
    if ( out["abstract"] ) {

        // Can't have abstract + private
        if ( out.access == "private" ) {
            throw new VJSError(208, name);
        }

        // Can't have abstract + final
        if ( out["final"] ) {
            throw new VJSError(209, name);
        }
    }

    // make sure that the properties have their declaration in common format so
    // that they can be compared between each other. Although the modifiers
    // order doesn't matter, the declaration property will always be in the
    // format: "[abstract] [final] [const] access [static] name"
    if (out["abstract"]) {
        decl.push("abstract");
    }
    if (out["final"]) {
        decl.push("final");
    }
    if (out["const"]) {
        decl.push("const");
    }
    decl.push(out.access);
    if (out["static"]) {
        decl.push("static");
    }
    decl.push(out.name);
    out.declaration = decl.join(" ");

    return out;
}

/**
 * Creates and returns a wrapped method. The purpose of that is to make
 * every method entering the private scope at the beginning and leaving
 * it at the end. Variables like @state and @staticState are objects that
 * are provided here and then "remembered" as local variables from the
 * closure. The actual function @fn cannot access them but the returned
 * functions will...
 * @param {Function} fn The actual function to be bound
 * @return {Function} The created method
 * TODO remove the name arg and fix the description!
 */
function createMethod(scope, fn, name, state, staticState) {

    function proxy() {
        var old = state._inPrivateScope,
            out;

        state._inPrivateScope = name;
        if (staticState) staticState._inPrivateScope++;

        //scope.__super__ = (state.superState[name] || {}).value || noop;
        //if (typeof scope.__super__ == "function") {
        //    scope.__super__ = scope.__super__.bind(scope);
        //}
        var o = this instanceof proxy ? Object.create(fn.prototype) : this || scope;
        //old = o.__super__;
        //o.__super__ = state.__super__(name);
        out = fn.apply(o, arguments);
        //o.__super__ = old;

        if (staticState) staticState._inPrivateScope--;
        state._inPrivateScope = old;

        return this instanceof proxy ? o : out;
    }

    proxy.toString = function() {
        return fn.toString();
    };

    return proxy;
}

/**
 * Creates a propperty attached to the object @obj but also maintains a link
 * to the private local objects @scope and @staticScope where the actual
 * values are stored. Unles the property should be "public" it can be
 * considered as a virtual property, meaning that it might have getters and
 * setters on the @obj but the actual value is stored at @state or
 * @staticState (depending on if the property is static or not)
 * @param {Object} The object to augment
 * @param {Object} property The property object created with the property
 *  function
 * @param {Object} state The actual property-value storage object
 * @param {Object} staticState The storage for the static properties. This
 *  might be the same object as @state if the property is static. Otherwise
 *  it is different. It is not used directly by this function but it gets
 *  passed to the createMethod function so that instance methods can also
 *  "unlock" the private and protected static scope...
 * TODO staticState needs a better description or just some better solution
 */
function createProperty(prop, Class, obj, state) {

    var staticState = Class.privateStaticState;

    // Set the initial value of the property
    var _prop = state.props[prop.name] = typeof prop.value == "function" ?
        createMethod(obj, prop.value, prop.name, state, staticState) :
        prop.value;

    // There is no need for getters and setters for the simple properties
    //if ( !prop["const"] && prop.access == "public" ) {
    //    obj[prop.name] = _prop;
    //    return;
    //}

    function isAllowed(prop, that) {console.log(prop, that, Class, obj);
        if (prop.access == "public") {
            return true;
        }
        if (state._inPrivateScope) {
            return true;
        }
        /*if (prop["static"]) {
            return staticState._inPrivateScope || 
                Class.constructor.prototype.isPrototypeOf(that.prototype);
        } else {
            if (that instanceof Class.constructor)
                return true;    
            return Class.constructor.prototype.isPrototypeOf(that);
        }*/
        return true;
    }

    // Use Object.defineProperty to do the access magic...
    defineProperty(
        obj,
        prop.name,
        {
            /**
             * The getter for the property. Will deny access for
             * non-public properties and methods from the outer scope.
             * @return {*} The property value if accessible.
             * @throws {Error} If the property is not accessible.
             */
            get : function() {//console.log(Class, this === obj);
                if (state._inPrivateScope || prop.access == "public") {
                    return _prop;    
                }
                //if (typeof this == "function" && Class.constructor.prototype.isPrototypeOf(this.prototype)) {
                //    return prop.value;
                //}
                //if (Class.constructor.prototype.isPrototypeOf(this)) {
                //    var c = vjs.getClassOf(this);//console.log("getClassOf:", c)
                //    if (!c || !state._inPrivateScope)
                        throw new VJSError(201, prop.name);
                //    return _prop;
                //}
                //throw new VJSError(201, prop.name);
            },

            /**
             * The setter for the property. Will deny access for
             * non-public properties and methods from the outer scope.
             * It will also deny the change for "const" properties and
             * methods, unless they are undefined.
             * @return void
             * @throws {Error} If the property is not writeable.
             */
            set : function( value ) {
                if ( !state._inPrivateScope && prop.access != "public" ) {
                    throw new VJSError(201, prop.name);
                }
                if ( prop["const"] && _prop !== undefined ) {
                    throw new VJSError(203, prop.name, Class.name);
                }
                _prop = state.props[prop.name] = typeof value == "function" ?
                    createMethod(obj, value, prop.name, state, staticState) :
                    value;
            },

            // Non-public members are NOT enumerable
            enumerable : prop.access != "private",

            // Non-constant members are configurable
            // (so that they can be deleted)
            configurable : !prop["const"]
        }
    );
}

/*
 * JavaScriipt implementation of the most useful parts of the C printf function
 */
function printf() {
    var args = Array.prototype.slice.call(arguments),
        len  = args.length,
        tpl;

    if ( !len ) {
        return "";
    }

    tpl = args.shift();

    return tpl.replace(
        rePrintfTokens,
        function(match, precision, precisionDigit, specifier, pos, input) {
            var replacement = args.shift();

            if (replacement === undefined) {
                return "";
            }

            switch ( specifier ) {

                // Signed decimal integer
                case "d":
                case "i":
                    return parseInt(replacement, 10);

                // Unsigned decimal integer
                case "u":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement);
                    }
                    return replacement;

                // Unsigned hexadecimal integer 7fa
                case "x":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16);
                    }
                    return replacement;

                // Unsigned hexadecimal integer (uppercase) 7FA
                case "X":
                    replacement = parseInt(replacement, 10);
                    if ( !isNaN(replacement) && isFinite(replacement) ) {
                        replacement = Math.abs(replacement).toString(16).toUpperCase();
                    }
                    return replacement;

                // Decimal floating point, lowercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                case "f":
                    return parseFloat(replacement).toFixed(
                        parseInt(precisionDigit, 10)
                    );

                //Decimal floating point, uppercase 392.65
                // TODO: This does not work as it should!!! (not used here)
                case "F":
                    return parseFloat(replacement).toFixed(
                        parseInt(precisionDigit, 10)
                    ).toUpperCase();

                case "c":
                case "s":
                    return String(replacement);

                case "%":
                    return "%";

                default:
                    return "%" + specifier;
            }
        }
    );
}

/**
 * Class VJSError extends Error. This is used to create Error object
 * by providing an error code to the constructor and other variables
 * that are substituted into the message using printf. This class
 * exists for several reasons:
 * 1. To make it possible to store all the error messages in one place
 * 2. To make it easier to implement some kind of message translations
 *    later
 * 3. To allow for test to compare errors by their code instead of by
 *    the message that can vary
 * @param {Number|String} code The error code as number or as numeric
 *    string
 * @param {*} any Zero or more additional arguments to be substituted into
 *    the message
 * @return {VJSError}
 */
function VJSError( code ) {
    var params = Array.prototype.slice.call(arguments, 1);
    this.code = code || 0;
    params.unshift(errors[this.code]);
    this.message = printf.apply({}, params);
}

VJSError.prototype = Object.create(Error.prototype);


// Source: src/vjs-core-2.js
// vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={,} foldlevel=10 foldmethod=manual spell:

/**
 * The internal storage for classes
 * @type Object
 * @private
 */
var _classes = {};

/**
 * The vjs function - the main object that gets exported.
 * @param {String} declaration
 * @param {Object|Function} properties
 * @return {Function} The constructor of the created class
 */
function vjs(declaration, properties) {
    return (new _class(declaration, properties)).Constructor;
}

/**
 * The globally available static configuration object. Can be modified to
 * change the vjs behavior...
 * @type Object
 */
vjs.config = {
    namespace : NS
};

/**
 * This can be used to obtain a reference to a class object by name (normally,
 * the clients will only work with class constructors and are not going to
 * need this)
 * @param {String} name The name of the needed class
 */
vjs.getClass = function( name ) {
    return _classes[ name ];
};

vjs.getClassOf = function( inst ) {
    for ( var c in _classes ) {
        if ( _classes[c].Constructor === inst.constructor ) {
            return _classes[c];
        }
    }
};

/**
 * Utility function that can create new instances of the given class name.
 * The first argument is required and must be the class name and any additional
 * arguments will be passed to the class constructor.
 * @param {String} name The class name
 */
vjs.createInstance = function( name ) {
    var _class = _classes[ name ], inst;
    if (!_class) {
        throw new VJSError(202, name);
    }
    inst = Object.create(_class.Constructor.prototype);
    _class.Constructor.apply(inst, Array.prototype.slice.call(arguments, 1));
    return inst;
};

/**
 * Parses the class declaration string @declaration and sets some properties on
 * the instance @inst. This might be a typical task for some instance method,
 * but it is moved here (to local private function) to make it a little bit
 * faster and because such things should remain private and invisible to the
 * outer scope.
 * @param {String} declaration The declaration string
 * @param {_class} inst The instance to augment
 * @throws VJSError If the declaration is empty or invalid
 * @return {void}
 */
function parseClassDeclaration(declaration, inst) {
    var tokens     = trim(declaration || "").split(reClass),
        mods       = trim(tokens[0]).split(reSpaces),
        right      = tokens.length > 1 ? trim(tokens[1]) : "",
        path       = right.replace(reWsOrEof, ""),
        ext        = right.match(reExtends),
        impl       = right.match(reImplements),
        modsLength = mods.length,
        name, mod;

    if (!path) {
        throw new VJSError(107, declaration);
    }

    while (modsLength) {
        mod = mods[--modsLength];
        switch (mod) {
            case "":
                break;
            case "abstract":
                inst.isAbstract = true;
                break;
            case "final":
                inst.isFinal = true;
                break;
            default:
                throw new VJSError(105, mod);
        }
    }

    /**
     * The identifier (class path) pointing to the superclass that
     * this class should inherit from.
     */
    inst["extends"] = ext ? ext[1] : null;

    /**
     * An array of zero or more interface identifiers pointing to the
     * interface(s) that this class should implement.
     */
    inst["implements"] = impl ? impl[1].split(reCommaSplit) : [];

    inst.path = path;

    inst.name = path.replace(reJsPath, ".$1").split(".").pop();

    _classes[path] = inst;
}

/**
 *
 */
function InstanceState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.instanceProperties : {};
}

/**
 *
 */
function StaticState( superClass ) {
    this.props = {};
    this._inPrivateScope = 0;
    this.superState = superClass ? superClass.classProperties : {};
}

/**
 * Lookup the class at @path and verify that it can be used as base class
 * @param {String} path The class name or path
 * @throws VJSError
 * @return {_class} The base class
 */
function findSuper(path) {
    var _super = _classes[path];

    // Call the custom class-loader function if defined
    if (!_super && is(vjs.config.loadClass, "function") ) {
        _super = vjs.config.loadClass(path);
    }

    // Cannot inherit from undefined class
    // TODO: Better error message (like "failed to evaluate class path")
    if (!_super) {
        throw new VJSError(204, path);
    }

    // Cannot inherit from non-class
    if (!(_super instanceof _class)) {
        throw new VJSError(205, path);
    }

    // Cannot inherit from final class
    if (_super.isFinal) {
        throw new VJSError(206, path);
    }

    return _super;
}

function createInstance(Class, instance, args) {
    var privateState = {
        props : {},
        _inPrivateScope : 0
    }, prop, inSuperChain;
    for (var x in Class.instanceProperties) {
        prop = Class.instanceProperties[x];
        createProperty(prop, Class, instance, privateState);
    }

    defineProperty(instance, "__super__", {
        get : function() {
            var name = privateState._inPrivateScope, prt, out;
            if (!name) {
                throw new VJSError(201, "__super__");
            }

            prt = Class.superClass ? 
                Class.superClass.instanceProperties :
                Object.prototype;

            out = prt[name];

            return prt.hasOwnProperty(name) ? 
                typeof out.value == "function" ? 
                    inSuperChain ?
                        noop :
                        (function() {
                            inSuperChain = true;
                            var result = out.value.apply(this, arguments);
                            inSuperChain = false;
                            return result;
                        }).bind(instance) :
                    out.value : 
                undefined;
        }
    });

    if ( typeof instance.init == "function" ) {
        instance.init.apply( instance, args );
    }
    return instance;
}

function createClassConstructor(Class) {
    if ( Class.isAbstract ) {
        return Function(
            "return function " + Class.name + "() {\n" +
            "   throw new VJSError(207, '" + Class.name + "');\n" + 
            "};"
        )();
    }
        
    return Function(
        "Class, createInstance", 
        "return function " + Class.name + "() {\n" +
        "   createInstance(Class, this, arguments);\n" + 
        "};"
    )(Class, createInstance);
}

/**
 *
 */
function _class(declaration, properties)
{
    var prop, x, parentProp, thisClass = this;

    /**
     * The original (unparsed) declaration string.
     */
    this.declaration = declaration;

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function.
     */
    this.instanceProperties = {};

    /**
     * A map of name:property that will contain all the property objects
     * as returned from the property() function. Same as the instanceProperties
     * above, but for static properties and methods
     */
    this.classProperties = {};

    /**
     * The super class defaults to Object (if nothing else is specified in the
     * "extends" keyword).
     * @type Function
     */
    this.superClass = null;

    this.privateStaticState = {
        props : {},
        _inPrivateScope : 0
    };

    // Starts the class initialization
    // =========================================================================

    // This will set some of the instance properties like "name", "path",
    // "final", "abstract", "extends" and ""implements"."
    // -------------------------------------------------------------------------
    parseClassDeclaration(declaration, this);

    // If it is specified that this class should inherit from another, try to
    // load it and check if such inheritance is possible and valid
    // -------------------------------------------------------------------------
    if ( this["extends"] ) {
        this.superClass = findSuper(this["extends"]);
    }

    // Create the constructor
    // Trying to create an instance of abstract class must fail
    // -------------------------------------------------------------------------
    this.Constructor = createClassConstructor(this);
    

    // Inheritance
    // -------------------------------------------------------------------------
    this.Constructor.prototype = Object.create(
        this.superClass ? 
            this.superClass.Constructor.prototype : 
            Object.prototype
        );

    // Loop super props. Note that the for/in loop will skip the private 
    // properties because they ate not enumerable.
    if (this.superClass) {
        for ( x in this.superClass.instanceProperties ) {
            parentProp = this.superClass.instanceProperties[x];
            if (parentProp && parentProp.access != "private") {
                this.instanceProperties[x] = parentProp;
        //        defineProperty(this.constructor.prototype, x, {
        //            get : function() {
        //                throw new VJSError(201, x);
        //            },
        //            set : function( value ) {
        //                throw new VJSError(201, x);
        //            }
        //        });
            }
        //    this.Constructor.prototype[x] = this.superClass.Constructor.prototype[x];
        }
        //this.Constructor.prototype.__super__ = this.superClass.Constructor.prototype;


        //console.log("TEST: ", this.superClass.constructor.prototype.isPrototypeOf(this.constructor.prototype))
    }

    this.Constructor.prototype.constructor = this.Constructor;
    this.Constructor.__class__ = this;

    // Compile properties
    // (the second argument for "_class" is optional)
    // -------------------------------------------------------------------------
    if ( !properties ) {
        properties = {};
    }
    
    // "properties" can be a function
    if ( typeof properties == "function" ) {
        properties = properties(
            this.superClass ? this.superClass.Constructor.prototype : {},
            this.Constructor
        );
    }

    // Invalid class properties declaration
    if ( !properties || typeof properties !== "object" ) {
        throw new VJSError(106);
    }

    // Attach the properties to the proptptype (or to the constructor if they 
    // are static).
    // -------------------------------------------------------------------------
    for ( x in properties ) {
        prop = property(x, properties[x]);
        if ( prop["static"] ) {
            createProperty(prop, this, this.Constructor, this.privateStaticState);
        } else {
            parentProp = this.superClass ? 
                this.superClass.instanceProperties[prop.name] :
                null;
            
            // Don't override finals
            if (parentProp && parentProp["final"] ) {
                throw new VJSError(211, prop.name);
            }

            // Check for abstract methods (Must use  abstract class)
            if ( prop["abstract"] && !this.isAbstract ) {
                throw new VJSError(210, prop.name, this.name);
            }

            this.instanceProperties[prop.name] = prop;
        }
    }

    // If this class inherits from an abstract one make sure that the abstract
    // methods were implemented
    // -------------------------------------------------------------------------
    if ( this.superClass ) {
        for ( x in this.superClass.instanceProperties ) {
            parentProp = this.superClass.instanceProperties[x];
            prop = this.instanceProperties[parentProp.name];
            if (parentProp["abstract"] && prop === parentProp) {
                throw new VJSError(212, this.name, parentProp.name);
            }
        }
    }

    // Write the constructor to vjs.config.namespace if needed
    // -------------------------------------------------------------------------
    if (vjs.config.namespace) {
        jPath(vjs.config.namespace, this.path, this.Constructor);
    }

    this.toString = function() {
        return this.declaration;
    };

    return this;
}

NS.vjs = vjs;

// Source: src/footer.js

})( this );

